<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreateListaPrecioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lista_precio', function (Blueprint $table) {
            $table->increments('id'); 
            $table->integer('per_id_padre')->unsigned();
            $table->integer('producto_id')->unsigned();
            $table->decimal('precio',16,4)->unsigned();
            $table->float('porcentaje_ganancia',5,2)->unsigned();
            $table->decimal('precio_final',16,4)->unsigned();
            $table->string('glosa')->default('');            
            $table->smallInteger('estado')->unsigned()->default(1);

            $table->timestamps();

            $table->foreign('producto_id')
                            ->references('id')
                            ->on('producto');

            $table->index('per_id_padre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lista_precio');
    }
}
