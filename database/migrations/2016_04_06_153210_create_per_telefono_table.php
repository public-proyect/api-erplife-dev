<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreatePerTelefonoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('per_telefono', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('persona_id')->unsigned();
            $table->smallinteger('item')->default(1);
            $table->integer('tipo_telefono_id')->unsigned();
            $table->string('telefono',20);
            $table->smallInteger('estado')->default(1);

            $table->foreign('persona_id')
                    ->references('id')
                    ->on('persona');

            $table->foreign('tipo_telefono_id')
                    ->references('id')
                    ->on('tipo_telefono');

            $table->index('telefono');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('per_telefono');
    }
}
