<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreateSubfamiliaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_familia', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('per_id_padre')->unsigned();
            $table->integer('familia_id')->unsigned();
            $table->string('codigo',15);
            $table->string('descripcion',255);
            $table->string('glosa',255)->default('');
            $table->string('imagen',200)->default('');
            $table->smallInteger('estado')->default(1);

            $table->foreign('familia_id')
                    ->references('id')
                    ->on('familia');

            $table->index('per_id_padre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_familia');
    }
}
