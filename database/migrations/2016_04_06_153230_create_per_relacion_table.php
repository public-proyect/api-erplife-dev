<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreatePerRelacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('per_relacion', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('persona_id')->unsigned();
            $table->integer('tipo_relacion_id')->unsigned();
            $table->integer('per_id_padre')->unsigned();
            $table->string('referencia',100);
            $table->smallInteger('estado')->default(1) ;
            $table->timestamps();

            $table->foreign('persona_id')
                    ->references('id')
                    ->on('persona');

            $table->foreign('tipo_relacion_id')
                    ->references('id')
                    ->on('tipo_relacion');

            $table->index('per_id_padre');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('per_relacion');
    }
}
