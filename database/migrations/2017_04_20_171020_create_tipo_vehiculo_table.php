<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipoVehiculoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_vehiculo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('per_id_padre')->unsigned();
            $table->bigInteger('clase_vehiculo_id')->unsigned();
            $table->string("descripcion", 255);
            $table->string("glosa", 255)->default("");
            $table->integer("estado")->default(1);
            $table->timestamps();

            $table->foreign('clase_vehiculo_id')
                            ->references('id')
                            ->on('clase_vehiculo');  

            $table->index('per_id_padre');              
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tipo_vehiculo');
    }
}
