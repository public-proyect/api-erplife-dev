<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiculoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehiculo', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->bigInteger('per_id_padre')->unsigned();
            $table->bigInteger('tipo_vehiculo_id')->unsigned();
            $table->bigInteger('marca_vehiculo_id')->unsigned();
            $table->string("placa", 20);
            $table->string("tarjeta_propiedad", 50);
            $table->integer('anio_fabricacion');
            $table->float('largo',4,2);
            $table->float('ancho',4,2);
            $table->float('alto',4,2);
            $table->float('capacidad',4,2);
            $table->string("glosa", 255)->default("");
            $table->integer("estado")->default(1);
            $table->timestamps();

            $table->foreign('tipo_vehiculo_id')
                            ->references('id')
                            ->on('tipo_vehiculo');

            $table->foreign('marca_vehiculo_id')
                            ->references('id')
                            ->on('marca_vehiculo');
                            
            $table->index('per_id_padre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehiculo');
    }
}
