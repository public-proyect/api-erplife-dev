<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreateTiendaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('tienda', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('per_id_padre')->unsigned();
            $table->integer('sucursal_id')->unsigned();
            $table->integer('distrito_id')->unsigned();
            $table->string('codigo',10)->default('');
            $table->string('nombre',255);
            $table->string('descripcion',255)->default('');
            $table->string('direccion',255)->default('');
            $table->smallInteger('estado')->default(1);
            $table->timestamps();

            $table->foreign('sucursal_id')
                    ->references('id')
                    ->on('sucursal');

            // $table->index('sucursal_id');
            $table->index('per_id_padre');
            $table->index('distrito_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tienda');
    }
}
