<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreatePerJuridicaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('per_juridica', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('persona_id')->unsigned();
            $table->integer('rubro_id')->unsigned()->default(0);
            $table->string('ruc',15)->index();
            $table->string('razon_social',255);
            $table->string('nombre_comercial',255)->default('');
            $table->string('glosa',255)->default('');
            $table->smallInteger('estado')->default(1);

            $table->foreign('persona_id')
                    ->references('id')
                    ->on('persona');

            $table->index('rubro_id');

            /*$table->foreign('rubro_id')
                    ->references('id')
                    ->on('rubro');*/

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('per_juridica');
    }
}
