<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreateSucursalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sucursal', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('per_id_padre')->unsigned();
            $table->integer('item')->unsigened()->default(0);
            $table->integer('departamento_id')->unsigened()->default(0);
            $table->string('codigo',10)->default('');
            $table->string('nombre',255);
            $table->string('descripcion',255)->default('');
            $table->smallInteger('estado')->default(1);
            $table->timestamps();

            $table->foreign('per_id_padre')
                    ->references('id')
                    ->on('persona');

            $table->index('per_id_padre');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sucursal');
    }
}
