<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubAreaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_area', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('per_id_padre')->unsigned();
            $table->integer('area_id')->unsigned();
            $table->string('descripcion',150);
            $table->string('glosa',255)->default('');
            $table->smallInteger('estado')->default(1);

            $table->foreign('area_id')
                    ->references('id')
                    ->on('area');

            $table->index('per_id_padre');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_area');
    }
}
