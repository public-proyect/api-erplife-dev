<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreateUbigeoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ubigeo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo',10);
            $table->string('ubigeo',150);
            $table->string('descripcion',255)->nullable();
            $table->integer('ubigeo_id_padre')->unsigned()->default(0);
            $table->integer('pais_id')->unsigned()->default(1);
            $table->integer('tipo_ubigeo_id')->unsigned()->default(1); # 1= departamento ; 2=provincia: 3= distrito
            $table->smallInteger('estado')->default(1);

            $table->index('codigo');
            $table->index('tipo_ubigeo_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ubigeo');
    }
}
