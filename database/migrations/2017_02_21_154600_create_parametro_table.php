<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreateParametroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parametro', function (Blueprint $table) {
           $table->increments('id');
            $table->integer('per_id_padre')->unsigned();
            $table->integer('par_codigo')->unsigned();
            $table->integer('par_clase')->unsigned();
            $table->string('par_jerarquia',20)->default('');
            $table->string('par_descripcion',255);
            $table->string('par_glosa',255)->default('');
            $table->smallInteger('estado')->default(1);
            // $table->stimestamps();

            $table->index('per_id_padre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parametro');
    }
}
