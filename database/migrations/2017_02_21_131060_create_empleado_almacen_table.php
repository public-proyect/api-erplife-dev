<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadoAlmacenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleado_almacen', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empleado_id')->unsigned();
            $table->integer('almacen_id')->unsigned();
            $table->date('fecha_ini');
            $table->date('fecha_fin');
            $table->string('codigo_empleado',45);
            $table->string('nombres',45);
            $table->smallInteger('estado')->unsigned()->default(1) ;
            $table->timestamps();

            $table->foreign('empleado_id')
                    ->references('id')
                    ->on('empleado');

            $table->foreign('almacen_id')
                    ->references('id')
                    ->on('almacen');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('empleado_almacen');
    }
}
