<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreateDocPagoDetalleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doc_pago_detalle', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('doc_pago_id')->unsigned();
            $table->integer('tipo_comprobante_id')->unsigned();
            $table->integer('item')->unsigned();            
            $table->string('numero_comp',20);
            $table->decimal('importe',20,4); 
            $table->dateTime('fecha_pago');
            $table->decimal('tipo_cambio',16,4);//agregado
            $table->integer('doc_tipo_id')->unsigned();            
            $table->integer('par_medio_pago_id')->unsigned();//agregado
            $table->string('glosa',255)->default('');
            $table->smallInteger('estado')->default(1); 
            $table->timestamps();

            $table->foreign('doc_pago_id')
                    ->references('id')
                    ->on('doc_pago');
            $table->foreign('tipo_comprobante_id')
                    ->references('id')
                    ->on('tipo_comprobante');
            $table->foreign('doc_tipo_id')
                    ->references('id')
                    ->on('doc_tipo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doc_pago_detalle');
    }
}
