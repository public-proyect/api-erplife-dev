<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreatePerMailTable extends Migration
{
    /**
     * Run the migrations.
     *sudo apt install wine2.0

     * @return void
     */
    public function up()
    {
        Schema::create('per_mail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('persona_id')->unsigned();
            $table->smallinteger('item')->default(1)->unsigned();
            $table->string('mail',150);
            $table->smallInteger('estado')->default(1);

            $table->foreign('persona_id')
                    ->references('id')
                    ->on('persona');

            $table->index('mail');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('per_mail');
    }
}
