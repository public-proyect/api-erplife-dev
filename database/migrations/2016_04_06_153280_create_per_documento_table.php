<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreatePerDocumentoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('per_documento', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('persona_id')->unsigned();
            $table->integer('tipo_per_documento_id')->unsigned();
            $table->string('numero',20);
            $table->date('feha_emision')->nullable();
            $table->date('fecha_caducidad')->nullable();
            $table->string('clase_categoria',100);
            $table->string('imagen',255)->default('');
            $table->smallInteger('estado')->default(1);
            $table->timestamps();

            $table->foreign('persona_id')
                    ->references('id')
                    ->on('persona');

            $table->foreign('tipo_per_documento_id')
                    ->references('id')
                    ->on('tipo_per_documento');

            $table->index('numero');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('per_documento');
    }
}
