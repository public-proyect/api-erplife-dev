<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersPersonaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_persona', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("per_id_padre")->unsigned();
            $table->integer("user_id")->unsigned();
            // $table->integer("persona_id")->unsigned();
            $table->integer("rol_id")->unsigned();
            $table->smallInteger('estado')->default(1);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('per_id_padre')->references('id')->on('persona');
            $table->foreign('rol_id')->references('id')->on('rol');

            $table->index('per_id_padre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('users_persona');
    }
}
