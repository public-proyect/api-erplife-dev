<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreateUnidadMedidaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unidad_medida', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('per_id_padre')->unsigned();
            $table->integer('unidad_medida_id')->unsigned()->nullable();
            $table->string('abreviatura', 45);
            $table->string('descripcion', 100);
            $table->integer('equivalente')->unsigned()->nullable();
            $table->string('glosa')->default('');
            $table->smallInteger('estado')->unsigned()->default(1);


            $table->foreign('unidad_medida_id')
                ->references('id')
                ->on('unidad_medida');

            $table->index('per_id_padre');
        });





    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unidad_medida');
    }
}
