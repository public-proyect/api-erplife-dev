<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class CreateKardexDetalleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kardex_detalle', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('kardex_id')->unsigned() ;
            $table->integer('producto_id')->unsigned() ;
            $table->integer('per_id_padre')->unsigned() ;
            $table->integer('user_id')->unsigned() ;
            $table->integer('almacen_id_destino')->unsigned() ;
            $table->dateTime('fecha_mov') ;
            $table->integer('stock_actual') ;
            $table->integer('stock_anterior') ;
            $table->integer('cantidad') ;
            $table->decimal('precio_unitario',16,4) ;
            $table->decimal('precio_total',16,4) ;
            $table->string('glosa',255)->default('') ;
            $table->integer('estado')->unsigned()->default(1) ;
            $table->timestamps();

            $table->foreign('producto_id')
                    ->references('id')
                    ->on('producto');

            $table->foreign('kardex_id')
                    ->references('id')
                    ->on('kardex');

            $table->index('per_id_padre');
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kardex_detalle');
    }
}
