<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreateDocParametroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doc_parametro', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('documento_id')->unsigned();
            $table->integer('par_clase')->unsigned();
            $table->integer('par_codigo')->unsigned();
             $table->string('descripcion',255); 
            $table->string('glosa',255)->default('');
            $table->smallInteger('estado')->default(1); 
            

            $table->foreign('documento_id')
                    ->references('id')
                    ->on('documento');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doc_parametro');
    }
}
