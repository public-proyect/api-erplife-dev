<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
*/

class CreateAccesoEntidadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acceso_entidad', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_persona_id')->unsigned();
            $table->integer('entidad_id')->unsigned();
            $table->integer('objeto_entidad_id')->unsigned();
            $table->smallInteger('estado')->default(1);
            $table->timestamps();

            $table->foreign('user_persona_id')
                    ->references('id')
                    ->on('users_persona');

            $table->foreign('entidad_id')
                    ->references('id')
                    ->on('entidad');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acceso_entidad');
    }
}
