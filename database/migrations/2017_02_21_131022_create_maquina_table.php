<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaquinaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maquina', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("tienda_id")->unsigned();
            $table->string("codigo_maquina",45);
            $table->string("modelo_maquina",45);
            $table->string("serie_maquina",45);
            $table->string("descripcion",45);
            $table->smallInteger('estado')->default(1);
            $table->timestamps();

            $table->foreign('tienda_id')
            ->references('id')
            ->on('tienda');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('maquina');
    }
}
