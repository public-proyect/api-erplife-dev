<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreateEmpleadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleado', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('persona_id')->unsigned();
            $table->integer('per_id_padre')->unsigned();
            $table->string('codigo',15)->default('');
            $table->smallInteger('estado')->unsigned()->default(1);
            $table->timestamps();

            $table->foreign('persona_id')
                    ->references('id')
                    ->on('persona');

            $table->index('per_id_padre');
            $table->index('codigo');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('empleado');
    }
}
