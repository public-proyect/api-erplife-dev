<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */
class CreatePersonaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

         Schema::create('persona', function (Blueprint $table) {
            $table->increments('id');
            $table->string("per_nombre", 255);
            $table->string("per_apellidos", 255);
            $table->date("per_fecha_nac")->nullable();
            $table->integer("per_tipo")->default(1);
            $table->integer("per_id_padre")->unsigned();
            $table->integer("empresa_id")->unsigned()->default(0);
            $table->smallInteger('estado')->default(1);
            $table->timestamps();

            /*$table->foreign('empresa_id')
                    ->references('id')
                    ->on('empresa');*/

            $table->index('empresa_id');
            $table->index('per_id_padre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('persona');
    }
}
