<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreatePerDocIdentidadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('per_doc_identidad', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('persona_id')->unsigned();
            $table->integer('tipo_per_doc_identidad_id')->unsigned();
            $table->integer('item')->unsigned()->default(1);
            $table->string('numero',25);
            $table->date('fecha_caducidad')->nullable();
            $table->date('fecha_emision')->nullable();
            $table->string('imagen',255)->default('');
            $table->smallInteger('estado')->default(1);

            $table->foreign('persona_id')
                    ->references('id')
                    ->on('persona');

            $table->foreign('tipo_per_doc_identidad_id')
                    ->references('id')
                    ->on('tipo_per_doc_identidad');


            $table->index('numero');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('per_doc_identidad');
    }
}
