<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreateDocPagoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doc_pago', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('documento_id')->unsigned();
            $table->integer('forma_pago_id')->unsigned();
            $table->integer('moneda_id')->unsigned();
            $table->integer('igv_id')->unsigned();
            $table->integer('items')->unsigned();
            $table->decimal('importe',20,4);//agregado
            $table->decimal('importe_igv',20,4);//agregado
            $table->decimal('importe_total',20,4);//agregado
            $table->decimal('saldo',20,4);
            $table->dateTime('fecha_vence');
            $table->smallInteger('estado')->default(1);
            $table->string('glosa',255)->default('');

            $table->foreign('documento_id')
                    ->references('id')
                    ->on('documento');

            $table->foreign('forma_pago_id')
                    ->references('id')
                    ->on('forma_pago');

            $table->foreign('moneda_id')
                    ->references('id')
                    ->on('moneda');

            $table->foreign('igv_id')
                    ->references('id')
                    ->on('igv');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doc_pago');
    }
}
