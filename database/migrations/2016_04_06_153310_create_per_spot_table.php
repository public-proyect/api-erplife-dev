<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerSpotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('per_spot', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('persona_id')->unsigned();
            $table->integer('spot_id')->unsigned();
            $table->string('glosa',100)->default('');
            $table->smallInteger('estado')->unsigned()->default(1);
            $table->timestamps();

            $table->foreign('persona_id')
                    ->references('id')
                    ->on('persona');

            $table->foreign('spot_id')
                    ->references('id')
                    ->on('spot');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('per_spot');
    }
}
