<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');

            // $table->integer('rol_id')->unsigned();
            $table->string('email',150)->unique();
            $table->string('password');
            $table->integer('empresa_id')->unsigned();
            $table->string('alias',100);
            $table->smallInteger('estado')->default(1);
            $table->rememberToken();
            $table->timestamps();

            // $table->foreign('rol_id') ->references('id') ->on('rol');

            $table->index('empresa_id');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
