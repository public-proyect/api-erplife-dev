<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserInfoTabe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->char('dni',8);
            $table->string('nombre',255);
            $table->string('apellidos',255);
            $table->string('avatar',255)->default('');
            $table->string('telefonos',255)->default('');
            $table->string('direccion',255)->default('');
            $table->smallInteger('estado')->default(1);

            $table->foreign('user_id')->references('id')->on('users');
            $table->index('dni');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_info');
    }
}
