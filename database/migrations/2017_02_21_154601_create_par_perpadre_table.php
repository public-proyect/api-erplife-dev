<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParPerpadreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('par_perpadre', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("per_id_padre")->unsigned();
            $table->integer("parametro_id")->unsigned();
            $table->integer("par_clase")->unsigned();
            $table->integer("par_codigo")->unsigned();
             $table->smallInteger('estado')->default(1);

            $table->foreign('parametro_id')
            ->references('id')
            ->on('parametro');
            
            $table->index('per_id_padre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('par_perpadre');
    }
}
