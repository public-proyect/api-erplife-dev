<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocEstadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('doc_estado', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger("documento_id")->unsigned();
            $table->string("par_codigo",45);
            $table->string("par_clase",45);
            $table->datetime("doc_est_fecha");//observado
            $table->string("glosa",255)->default('');


            $table->foreign('documento_id')
                    ->references('id')
                    ->on('documento');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doc_estado');
    }
}
