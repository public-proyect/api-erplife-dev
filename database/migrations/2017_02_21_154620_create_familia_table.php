<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreateFamiliaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('familia', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('per_id_padre')->unsigned();
            $table->integer('clase_id')->unsigned();
            $table->string('codigo',15);
            $table->string('descripcion',255);
            $table->string('glosa',255)->default('');
            $table->string('imagen',200)->default('');
            $table->smallInteger('estado')->default(1);

            $table->foreign('clase_id')
                    ->references('id')
                    ->on('clase');

            $table->index('per_id_padre');
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('familia');
    }
}
