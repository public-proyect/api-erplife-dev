<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreateAlmacenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('almacen', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('per_id_padre')->unsigned();
            $table->integer('tienda_id')->unsigned() ;
            $table->integer('tipo_almacen_id')->unsigned() ;
            $table->integer('distrito_id')->unsigned();
            $table->string('codigo',10) ;
            $table->string('descripcion',255) ;
            $table->string('direccion',255)->default('') ;
            $table->string('telefono',45) ;
            $table->string('glosa',255)->default('') ;
            $table->smallInteger('estado')->unsigned()->default(1);
            $table->timestamps();

            $table->foreign('tienda_id')
                    ->references('id')
                    ->on('tienda');

            $table->foreign('tipo_almacen_id')
                    ->references('id')
                    ->on('tipo_almacen');

            $table->index('per_id_padre');




        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('almacen');
    }
}
