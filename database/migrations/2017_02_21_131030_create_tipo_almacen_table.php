<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreateTipoAlmacenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_almacen', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('per_id_padre')->unsigned();
            $table->string('descripcion',255)->default('') ;
            $table->string('glosa',255)->default('');
            $table->smallInteger('estado')->default(1);

            $table->index('per_id_padre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tipo_almacen');
    }
}
