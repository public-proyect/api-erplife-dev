<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreateCargoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cargo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sub_area_id')->unsigned();
            $table->integer('per_id_padre')->unsigned();
            $table->string('descripcion',255);
            $table->string('glosa',255)->default('');
            $table->smallInteger('estado')->unsigned()->default(1);

            $table->foreign('sub_area_id')
                    ->references('id')
                    ->on('sub_area');

            $table->index('per_id_padre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cargo');
    }
}
