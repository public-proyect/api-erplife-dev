<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
*/

class CreatePerGrupoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('per_grupo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('persona_id')->unsigned();
            $table->integer('per_id_padre')->unsigned();
            $table->string('codigo',20)->default('');
            $table->integer('par_codigo')->unsigned();
            $table->integer('par_clase')->unsigned();
            $table->string('glosa',255)->default(''); 
            $table->integer('parametro_id')->unsigned()->default(0);
            $table->smallInteger('estado')->default(1);
            $table->timestamps();
            
            $table->foreign('persona_id')
                    ->references('id')
                    ->on('persona');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('per_grupo');
    }
}
