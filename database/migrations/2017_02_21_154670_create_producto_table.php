<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreateProductoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('per_id_padre')->unsigned();
            $table->string('codigo',20);
            $table->string('codigo_barra',30);
            $table->integer('unidad_medida_id')->unsigned();
            $table->integer('modelo_id')->unsigned()->nullable();
            $table->integer('sub_familia_id')->unsigned();            
            $table->integer('stock_min')->unsigned();
            $table->string('descripcion',255);
            $table->string('imagen',255);
            $table->smallInteger('percepcion');
            $table->string('glosa')->default('');
            $table->smallInteger('estado')->default(1);
            $table->timestamps();

            $table->foreign('sub_familia_id')
                    ->references('id')
                    ->on('sub_familia');

            $table->foreign('unidad_medida_id')
                    ->references('id')
                    ->on('unidad_medida');

            $table->foreign('modelo_id')
                    ->references('id')
                    ->on('modelo');

            $table->index('per_id_padre');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producto');
    }
}
