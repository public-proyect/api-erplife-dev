<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreateBitacoraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bitacora', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_persona_id')->unsigned();
            $table->string('action')->default('');
            $table->string('table_id',30);
            $table->string('table',100);
            $table->string('computer_ip',250)->default('');
            $table->text('new_value',45);
            $table->text('old_value',45);
            $table->timestamps();

            $table->foreign('user_persona_id')
                    ->references('id')
                    ->on('users_persona');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bitacora');
    }
}
