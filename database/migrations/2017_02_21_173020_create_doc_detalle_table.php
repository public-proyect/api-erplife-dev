<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreateDocDetalleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doc_detalle', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('documento_id')->unsigned(); 
            $table->integer('producto_id')->unsigned();
            $table->float('cantidad')->unsigned();
            $table->decimal('precio_unitario',20,4)->unsigned();
            $table->decimal('precio_total',20,4)->unsigned();
            $table->string('glosa',255)->default('');
            $table->smallInteger('estado')->default(1); 

            $table->foreign('documento_id')
                    ->references('id')
                    ->on('documento');
            $table->foreign('producto_id')
                    ->references('id')
                    ->on('producto');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doc_detalle');
    }
}
