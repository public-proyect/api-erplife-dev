<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormaPagoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forma_pago', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("tipo_pago_id")->unsigned();
            $table->string("descripcion",255);
             $table->smallInteger('estado')->default(1);

            $table->foreign('tipo_pago_id')->references('id')->on('tipo_pago');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('forma_pago');
    }
}
