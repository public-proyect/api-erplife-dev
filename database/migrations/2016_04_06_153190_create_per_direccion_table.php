<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreatePerDireccionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('per_direccion', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('persona_id')->unsigned();
            $table->integer('item')->unsigned()->default(1);
            $table->integer('tipo_direccion_id')->unsigned()->nullable();
            $table->integer('ubigeo_id')->unsigned()->nullable();
            $table->string('direccion',250);
            $table->string('referencia',250);
            $table->smallInteger('estado')->default(1);

            $table->foreign('persona_id')
                    ->references('id')
                    ->on('persona');

            $table->foreign('tipo_direccion_id')
                    ->references('id')
                    ->on('tipo_direccion');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('per_direccion');
    }
}
