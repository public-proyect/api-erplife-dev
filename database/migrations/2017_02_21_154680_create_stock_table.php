<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreateStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock', function (Blueprint $table) {     
            $table->increments('id');
            $table->integer('per_id_padre')->unsigned();
            $table->integer('producto_id')->unsigned();
            $table->integer('almacen_id')->unsigned();        
            $table->integer('stock_actual')->unsigned();
            $table->string('glosa',100)->default('');
            $table->smallInteger('estado')->unsigned()->default(1);
            $table->timestamps();

            $table->foreign('producto_id')
                            ->references('id')
                            ->on('producto');

            $table->foreign('almacen_id')
                            ->references('id')
                            ->on('almacen');


            $table->index('per_id_padre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock');
    }
}
