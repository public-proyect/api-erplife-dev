<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreateDocumentoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documento', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('per_id_padre')->unsigned();
            $table->integer('doc_tipo_id')->unsigned();          
            $table->integer('user_id')->unsigned();
            $table->string('codigo',20);
            $table->dateTime('doc_fecha',20);
            $table->string('doc_descripcion',255);
            $table->string('doc_glosa',255)->default('');
            $table->smallInteger('estado')->default(1);            
            $table->timestamps();

            $table->foreign('doc_tipo_id')
                    ->references('id')
                    ->on('doc_tipo');

            $table->index('per_id_padre');
            $table->index('user_id');
            $table->index('codigo');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documento');
    }
}
