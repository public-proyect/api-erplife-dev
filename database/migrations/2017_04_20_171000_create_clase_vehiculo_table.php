<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaseVehiculoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clase_vehiculo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('per_id_padre')->unsigned();
            $table->string("descripcion", 255);
            $table->string("glosa", 255)->default("");
            $table->integer("estado")->default(1);
            $table->timestamps();

            $table->index('per_id_padre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clase_vehiculo');
    }
}
