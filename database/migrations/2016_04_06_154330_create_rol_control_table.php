<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreateRolControlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rol_control', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rol_id')->unsigned();
            $table->integer('control_id')->unsigned();
            $table->string('referencia',50);
            $table->smallInteger('estado')->default(1);

            $table->foreign('rol_id')
                    ->references('id')
                    ->on('rol');

            $table->foreign('control_id')
                    ->references('id')
                    ->on('control');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rol_control');
    }
}
