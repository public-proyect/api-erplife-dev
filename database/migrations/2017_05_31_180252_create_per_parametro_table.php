<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerParametroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('per_parametro', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('persona_id')->unsigned();
            $table->integer('par_codigo')->unsigned();
            $table->integer('par_clase')->unsigned();
            $table->string('glosa',255)->default('');
            $table->smallInteger('estado')->unsigned()->default(1);

            $table->foreign('persona_id')
                    ->references('id')
                    ->on('persona');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('per_parametro');
    }
}
