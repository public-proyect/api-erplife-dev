<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreateControlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('control', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('control_padre_id')->unsigned()->nullable();
            $table->integer('tipo_control_id')->unsigned();
            $table->string('jerarquia',50);
            $table->string('nombre',100);
            $table->string('valor',250);
            $table->string('descripcion',100);
            $table->string('glosa',255)->default('');
            $table->smallInteger('estado')->default(1);

            $table->foreign('control_padre_id')
                    ->references('id')
                    ->on('control');

            $table->foreign('tipo_control_id')
                    ->references('id')
                    ->on('tipo_control');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('control');
    }
}
