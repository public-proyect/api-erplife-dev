<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreatePuntoEmisionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('punto_emision', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('per_id_padre')->unsigned() ;
            $table->integer('tienda_id')->unsigned() ;
            $table->integer('maquina_id')->unsigned();
            $table->string('descripcion',45);
            $table->smallInteger('estado')->unsigned()->default(1);
            $table->timestamps();


           /* $table->string('codigo',8)->default('');
            $table->string('nombre') ;
            $table->smallInteger('emite_ticket')->default(0) ;
            $table->string('modelo_maquina',100)->default('') ;
            $table->string('serie_maquina',50)->default('') ;*/

            $table->foreign('tienda_id')
                    ->references('id')
                    ->on('tienda');
            $table->foreign('maquina_id')
                    ->references('id')
                    ->on('maquina');

            $table->index('per_id_padre');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('punto_emision');
    }
}
