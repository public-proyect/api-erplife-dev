<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreateOauthIdentitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oauth_identities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('provider_id')->unsigned();
            $table->string('provider_user_id',45);
            $table->string('name',200);
            $table->string('screen_name')->nullable();
            $table->string('avatar')->nullable();
            $table->string('access_token',45);
            $table->timestamps();
            $table->smallInteger('estado')->default(1);

            $table->foreign('user_id')
                    ->references('id')
                    ->on('users');

            $table->foreign('provider_id')
                    ->references('id')
                    ->on('provider');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('oauth_identities');
    }
}
