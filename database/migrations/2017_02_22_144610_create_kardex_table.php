<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class CreateKardexTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kardex', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('documento_id')->unsigned();
            $table->integer('per_id_padre')->unsigned();
            $table->integer('tipo_operacion_id')->unsigned() ;
            $table->integer('almacen_id_origen')->unsigned() ;
            $table->integer('almacen_id_destino')->unsigned() ;
            $table->integer('user_id')->unsigned()->index() ;
            $table->dateTime('fecha') ;
            $table->string('glosa',255)->default('');
            $table->smallInteger('estado')->unsigned()->default(1) ;
            $table->timestamps();

            $table->foreign('documento_id')
                    ->references('id')
                    ->on('documento');

            $table->foreign('tipo_operacion_id')
                    ->references('id')
                    ->on('tipo_operacion');

            $table->index('per_id_padre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kardex');
    }
}
