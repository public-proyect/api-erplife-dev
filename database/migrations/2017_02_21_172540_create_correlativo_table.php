<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreateCorrelativoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('correlativo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('per_id_padre')->unsigned();
            $table->integer('tipo_comprobante_id')->unsigned();
            $table->integer('punto_emision_id')->unsigned();
            $table->string('serie', 5);
            $table->string('numero_ini', 10);
            $table->string('numero_fin', 10);
            $table->string('numero_actual', 10);
            $table->smallInteger('estado')->default(1);

            $table->foreign('tipo_comprobante_id')
                ->references('id')->on('tipo_comprobante');                
            $table->foreign('punto_emision_id')
                ->references('id')->on('punto_emision');

            $table->index('per_id_padre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('correlativo');
    }
}
