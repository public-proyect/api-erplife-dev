<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreatePerNaturalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('per_natural', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('persona_id')->unsigned();
            $table->char('dni',8)->nullable();
            $table->char('ruc',11)->nullable();
            $table->string('apellidos',200);
            $table->string('nombres',200);
            $table->smallInteger('sexo')->default(1);
            $table->smallInteger('estado_civil')->default(0);
            $table->smallInteger('estado')->default(1);

            $table->foreign('persona_id')
                    ->references('id')
                    ->on('persona');

            $table->index('dni');
            $table->index('ruc');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('per_natural');
    }
}
