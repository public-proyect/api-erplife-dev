<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * developer: @armandoaepp / armandoaepp@gmail.com
 */

class CreateDocPersonaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doc_persona', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('documento_id')->unsigned(); 
            $table->integer('per_grupo_id')->unsigned();
            $table->integer('persona_id')->unsigned();
            $table->integer('par_codigo')->unsigned(); 
            $table->string('glosa',255)->default('');
            $table->smallInteger('estado')->default(1); 

            $table->foreign('documento_id')
                    ->references('id')
                    ->on('documento');

            $table->index('per_grupo_id');
            $table->index('persona_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doc_persona');
    }
}
