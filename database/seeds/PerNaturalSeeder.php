<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class PerNaturalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();

		// $per_id_padre = 1 ;
		$empresa_id   = 0 ;


        for($i = 0; $i < 100; $i ++)
        {
            $firstName = $faker->firstName;
            $lastName = $faker->lastName;
            $create           = date('Y-m-d H:m:s') ;

			$empresas = App\Models\Personas\Persona::where('empresa_id','<>',0)->get() ;
			$per_id_padre = $empresas->random()->id ;

            $persona_id = \DB::table('persona')->insertGetId(array (
					'per_nombre'    => $faker->firstName,
					'per_apellidos' => $faker->lastName,
					'per_fecha_nac' => $faker->dateTimeBetween($startDate = '-45 years', $endDate = '-15 years'  )->format('Y-m-d'),
					'per_id_padre'  => $per_id_padre,
					'empresa_id'    => $empresa_id,
					'per_tipo'      => 1,
					'estado'        => 1,
            ));

            $dni = $faker->numerify('########') ;

            \DB::table('per_natural')->insert(array (
				'persona_id'   => $persona_id ,
				'dni'          => $dni ,
				'apellidos'    => $lastName,
				'nombres'      => $firstName ,
				'sexo'         => $faker->numberBetween($min = 1, $max = 2),
				'estado_civil' => $faker->numberBetween($min = 1, $max = 2),
            ));

        	DB::table('per_doc_identidad')->insert(array(
					'persona_id'            => $persona_id ,
					'tipo_per_doc_identidad_id' => 1,
					'numero'                => $dni,
					'fecha_emision'         => $faker->dateTimeBetween($startDate = '-3 years', $endDate = '+5 years'  )->format('Y-m-d'),
					'imagen'                => '',
					'estado'                => 1,
	            )
	        );

			\DB::table('per_mail')->insert(array (
				'persona_id'   => $persona_id ,
				'mail'          => $faker->unique()->email,
				'item'    => 1,
			));

			\DB::table('per_telefono')->insert(array (
				'persona_id'       => $persona_id ,
				'tipo_telefono_id' => 1,
				'telefono'         => $faker->numerify('9########'),
				'item'             => 1,
			));

			\DB::table('per_relacion')->insert(array(
					'per_id_padre'     => 1,
					'tipo_relacion_id' => 2,
					'persona_id'       => $persona_id,
					'referencia'       => 'cliente',
					'created_at'       => $create,
					'estado'           =>1,
	            )
	        );

    	}
    }
}
