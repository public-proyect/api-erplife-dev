<?php

use Illuminate\Database\Seeder;

class TipoRelacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data = array(
                    'Persona Natural',
                    'Persona Juridica',
                    'Usuario',
                    'Cliente',
                    'Empleado',
                    'Proveedor',
                    'Contacto',
                ) ;

    	for ($i=0; $i < count($data) ; $i++)
    	{
    		DB::table('tipo_relacion')->insert(array(
                    'descripcion' => $data[$i],
                    'glosa'       => $data[$i],
                    'estado'      => 1
				)
	        );
    	}
    }
}
