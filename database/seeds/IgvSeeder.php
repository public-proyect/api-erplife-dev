<?php

use Illuminate\Database\Seeder;

class IgvSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$json = File::get(base_path()."/database/seeds/jsondata/igv.json");
        $data = json_decode($json);

         $create = date('Y-m-d H:m:s') ;

        foreach ($data as $object) {
            \DB::table('igv')->insert(array(
				'valor' => $object->valor,
				'glosa' => empty($object->glosa) ? '' : $object->glosa,
				'created_at' => $create,
				'updated_at' => $create,
            ));
        }

    }
}
