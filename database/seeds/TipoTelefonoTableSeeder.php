<?php

use Illuminate\Database\Seeder;

class TipoTelefonoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array('Movistar', 'Claro','Nextel', 'RPM', 'RPC', 'Otro') ;

    	for ($i=0; $i < count($data) ; $i++)
    	{
    		 DB::table('tipo_telefono')->insert(array(
                    'descripcion' => $data[$i],
                    'glosa'       => $data[$i],
					'estado'=> 1
				)
	        );
    	}
    }
}
