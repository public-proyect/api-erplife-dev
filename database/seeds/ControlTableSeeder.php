<?php

use Illuminate\Database\Seeder;

class ControlTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$json = File::get(base_path()."/database/seeds/jsondata/control.json");
		$data = json_decode($json);

		foreach ($data as $object) {
			\DB::table('control')->insert(array(
				'control_padre_id' => empty($object->control_padre_id) ? NULL : $object->control_padre_id ,
				'tipo_control_id'  => $object->tipo_control_id ,
				'jerarquia'        => $object->jerarquia ,
				'nombre'           => $object->nombre ,
				'valor'            => empty($object->valor) ? '' : $object->valor ,
				'descripcion'      => empty($object->descripcion) ? '' : $object->descripcion  ,
				'glosa'            => empty($object->glosa) ? '' : $object->glosa  ,
				'estado'           => $object->estado ,
			));
		}






    }
}
