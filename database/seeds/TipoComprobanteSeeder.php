<?php

use Illuminate\Database\Seeder;

class TipoComprobanteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$json = File::get(base_path()."/database/seeds/jsondata/tipo_comprobante.json");
  		$data = json_decode($json);

  		foreach ($data as $object) {
  			\DB::table('tipo_comprobante')->insert(array(
  				'per_id_padre' => $object->per_id_padre,
  				'descripcion'  => $object->descripcion,
  				'numero'       => $object->numero,
  				'estado'       => $object->estado,
  			));
  		}
    }
}
