<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class SudoEmpresaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        #====== EMPRESA SUDO =========================
			$nombre           = "Life Digital " ;
			$nombre_comercial = "Life Digital Developer - @armandoaepp" ;
			$ruc              = "00000000000" ;
			$create           = date('Y-m-d H:m:s') ;

			$empresa_id = DB::table('empresa')->insertGetId(array(
									'nombre'           => $nombre,
									// 'nombre_comercial' => $nombre_comercial,
									'fecha_reg'        => $create,
									'estado'           => 0,
	                        )
	                    );

		    $persona_id_padre = DB::table('persona')->insertGetId(array(
									'per_nombre'    => $nombre,
									'per_apellidos' => $nombre_comercial,
									'per_fecha_nac' => $create,
									'per_tipo'      => 0,
									'per_id_padre'  => 0,
									'empresa_id'  => $empresa_id,
									'created_at'    => $create,
									'updated_at'    => $create,
	                        )
	                    );

	        DB::table('per_juridica')->insert(array(
				'persona_id'       => $persona_id_padre,
				'rubro_id'         => 0,
				'ruc'              => $ruc,
				'razon_social'     => $nombre,
				'nombre_comercial' => $nombre_comercial,
				'glosa'            => '',
				'estado'           => 1,
	            )
	        );

	        DB::table('per_doc_identidad')->insert(array(
				'persona_id'            => $persona_id_padre,
				'tipo_per_doc_identidad_id' => 2,
				'numero'                => $ruc,
				'fecha_emision'         => $create,
				'fecha_caducidad'       => $create,
				'imagen'                => '',
				'estado'                => 1,
	            )
	        );


    		$faker = Faker::create();

	        #===================================================================
	        # GRUPOS DE EMPRESAS
	        	$company = $faker->company ;
	        	DB::table('persona')->insert(array(
									'per_nombre'    => "002 ".$company,
									'per_apellidos' => "002 ".$company,
									'per_fecha_nac' => '1986-11-11',
									'per_tipo'      => 1,
									'per_id_padre'  => 0,
									'empresa_id'    => $empresa_id, # viene a ser cero y pertenece a -> persona_id_padre
									'created_at'    => $create,
									'updated_at'    => $create,
	                        )
	                    );

	        	$company = $faker->company ;
	        	DB::table('persona')->insert(array(
									'per_nombre'    => "003 ".$company,
									'per_apellidos' => "003 ".$company,
									'per_fecha_nac' => '1986-11-11',
									'per_tipo'      => 1,
									'per_id_padre'  => 0,
									'empresa_id'    => $empresa_id, # viene a ser cero y pertenece a -> persona_id_padre
									'created_at'    => $create,
									'updated_at'    => $create,
	                        )
	                    );

	        	$company = $faker->company ;
	        	DB::table('persona')->insert(array(
									'per_nombre'    => "004 ".$company,
									'per_apellidos' => "004 ".$company,
									'per_fecha_nac' => '1986-11-11',
									'per_tipo'      => 1,
									'per_id_padre'  => 0,
									'empresa_id'    => $empresa_id, # viene a ser cero y pertenece a -> persona_id_padre
									'created_at'    => $create,
									'updated_at'    => $create,
	                        )
	                    );
	        #===================================================================

    }
}
