<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class StockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create();

    	$presentacion = DB::table('presentacion')->get();

        foreach ($presentacion as $element)
        {
        	$presentacion_id = $element->id ;
        	$now     =  $faker->dateTimeBetween($startDate = '-1 years', $endDate = '0 years'  )->format('Y-m-d H:i:s') ;
			$number2 = $faker->numberBetween($min = 1, $max = 2) ;

			$stock_actual   = $faker->numberBetween($min = 50, $max = 100) ;
			$stock_anterior = $faker->numberBetween($min = 20, $max = 40) ;
			$stock_mov       = $stock_actual - $stock_anterior ;

			\DB::table('stock')->insert(array (
                'presentacion_id' => $presentacion_id,
                'almacen_id'      => 1,
                'per_id_padre'    => 1,
                'stock_actual'    => $stock_actual,
                // 'stock_anterior'  => $stock_anterior,
                // 'stock_mov'       => $stock_mov,
                'estado'          => 1,
                'created_at'      => $now,
                'updated_at'      => $now,
			));

        }

    }
}
