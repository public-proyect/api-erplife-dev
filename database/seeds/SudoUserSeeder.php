<?php

use Illuminate\Database\Seeder;

class SudoUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
         #==== user SUDO ===================================================
	        $nombre    = '@armandoaepp' ;
	        $apellidos = 'Pisfil Puemape' ;
	        $dni       = "4x3x8x8x" ;
	        $mail      = "armandoaepp@gmail.com" ;
	        $telefono  = '996393414' ;
	        $create    = date('Y-m-d H:m:s') ;

			$persona_id_padre = 1 ;
			$empresa_id       = 1 ;
			$rol_id           = 1 ;
    	# ==================================================================
			# PERSONA COMO USUARIO
	        /*$persona_id = DB::table('persona')->insertGetId(array(
									'per_nombre'    => $nombre,
									'per_apellidos' => $apellidos,
									'per_fecha_nac' => '1986-11-11',
									'per_tipo'      => 1,
									'per_id_padre'  => $persona_id_padre,
									'empresa_id'    => 0, # viene a ser cero y pertenece a -> persona_id_padre
									'created_at'    => $create,
									'updated_at'    => $create,
	                        )
	                    );



	        DB::table('per_natural')->insert(array(
	                'persona_id'   => $persona_id ,
	                'dni'          => $dni,
	                'apellidos'    => $apellidos,
	                'nombres'      => $nombre,
	                'sexo'         => 1,
	                'estado_civil' => 1,
	                'estado'       => 1,
	            )
	        );

	        DB::table('per_mail')->insert(array(
					'persona_id' => $persona_id ,
					'mail'       => $mail,
					'item'       => 1,
					'estado'     => 1,
	            )
	        );

	        DB::table('per_telefono')->insert(array(
					'persona_id'       => $persona_id ,
					'tipo_telefono_id' => 1,
					'telefono'         => $telefono,
					'item'             => 1,
					'estado'           => 1,
	            )
	        );

	        DB::table('per_doc_identidad')->insert(array(
					'persona_id'            => $persona_id ,
					'tipo_per_doc_identidad_id' => 1,
					'numero'                => $dni,
					'fecha_emision'         => $create,
					// 'fecha_caducidad'       => $create,
					'imagen'                => '',
					'estado'                => 1,
	            )
	        );

	        DB::table('per_imagen')->insert(array(
					'persona_id' => $persona_id ,
					'url'        => 'avatars/armandoaepp.png',
					'tipo'       => 1,
					'estado'     => 1,
	            )
	        );

	         DB::table('per_relacion')->insert(array(
					'per_id_padre'     => $persona_id_padre,
					'tipo_relacion_id' => 1,
					'persona_id'       => $persona_id,
					'referencia'       => 'SUDO',
					'created_at'       => $create,
					'estado'           =>1,
	            )
	        );*/

    	# ==================================================================

	        $user_id =  DB::table('users')->insertGetId(array(
					// 'persona_id'   => $persona_id ,
					// 'rol_id'       => $rol_id,
					'email'        => $mail,
					'password'     => Hash::make('armando'),
					'alias'        => 'armandoaepp',
					'empresa_id' => $empresa_id,
					'created_at'   => $create,
					'updated_at'   => $create,
					'estado'       => 1,
				)
	        );

			DB::table('user_info')->insert(array(
					'user_id'      => $user_id,
					'dni'          => $dni,
					'nombre'       => $nombre ,
					'apellidos'    => $apellidos ,
					'avatar'       => 'avatars/armandoaepp.png',
					'telefonos'    => '996393414',
					'direccion'    => 'C.P.N de Callanca/Monsefu/Chiclayo/Perú',
					'estado'       => 1,
	            )
	        );

	        $user_persona_id = DB::table('users_persona')->insertGetId(array(
					'per_id_padre'  => $persona_id_padre,
					'user_id'       => $user_id,
					// 'persona_id' => $persona_id,
					'rol_id'        => $rol_id,
					'estado'        => 1,
	            )
	        );

	        DB::table('users_persona')->insertGetId(array(
					'per_id_padre'  => 2,
					'user_id'       => $user_id,
					// 'persona_id' => $persona_id,
					'rol_id'        => $rol_id,
					'estado'        => 1,
	            )
	        );

	        DB::table('users_persona')->insertGetId(array(
					'per_id_padre'  => 3,
					'user_id'       => $user_id,
					// 'persona_id' => $persona_id,
					'rol_id'        => 2,
					'estado'        => 1,
	            )
	        );

	        DB::table('users_persona')->insertGetId(array(
					'per_id_padre'  => 4,
					'user_id'       => $user_id,
					// 'persona_id' => $persona_id,
					'rol_id'        => 2,
					'estado'        => 1,
	            )
	        );

	    #==== accesos Sudo ===================================================
	    	$controles = \DB::table('control')->get() ;


		// INSERT INTO `ld_db_gescom`.`rol_control` (`id`, `rol_id`, `control_id`, `referencia`, `estado`) VALUES (NULL, NULL, NULL, NULL, NULL);


	        # accesos para el rol_control SUDO
	    		$data_insert = array() ;
	            if (count($controles) > 0)
	            {
	                foreach ($controles as $row)
	                {
	                    $fill = array(
	    								'rol_id'     => 1,
	    								'control_id' => $row->id,
	    								'referencia' => $row->nombre ,
	    								'estado'     => $row->estado ,
	                                 ) ;
	                    array_push($data_insert, $fill) ;
	                }
	                # insertarmos los accesso
	               \DB::table('rol_control')->insert( $data_insert) ;

	            }

	            # accesos para el usuario sudo
	    		$data_accesos = array() ;

	            if (count($controles) > 0)
	            {
	                foreach ($controles as $row)
	                {
	                    $fill = array(
										'user_persona_id'    => $user_persona_id ,
										'control_id' => $row->id,
										'referencia' => "",
	                                 ) ;
	                    array_push($data_accesos, $fill) ;
	                }
	                # insertarmos los acceso
	               \DB::table('accesos')->insert( $data_accesos) ;
	            }

	            // id, user_persona_id, control_id, referencia, estado

	       /* # accesos para el usuario sudo
	    		$data_accesos = array() ;

	            if (count($controles) > 0)
	            {
	                foreach ($controles as $row)
	                {
	                    $fill = array(
										'user_id'    => $user_id ,
										'control_id' => $row->id,
										'referencia' => "",
	                                 ) ;
	                    array_push($data_accesos, $fill) ;
	                }
	                # insertarmos los acceso
	               \DB::table('accesos')->insert( $data_accesos) ;
	            }*/

    }
}
