<?php

use Illuminate\Database\Seeder;

class UbigeoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$userJson = File::get(base_path()."/database/seeds/jsondata/ubigeo.json");
			$user = json_decode($userJson);
			foreach ($user as $object){
				DB::table('ubigeo')->insert(array(
					'codigo'         => $object->codigo ,
					'ubigeo'         => $object->ubigeo ,
					'descripcion'    => $object->descripcion ,
					'tipo_ubigeo_id' => $object->tipo_ubigeo_id ,
					'pais_id'        => $object->pais_id ,
					// 'latitud'        => $object->latitud ,
					// 'longitud'       => $object->longitud ,
					'estado'         => $object->estado ,
				));
			}

		/**
		* SQL for PGSQL
		*/
			# actulizar UBIGEO_ID_PADRE DEPARTAMENTOS
			DB::select(' UPDATE ubigeo
						SET  ubigeo_id_padre = 0
						WHERE    ubigeo.id  = ubigeo.id
						AND ubigeo.codigo = concat(left(ubigeo.codigo,2),\'0000\') ;
					') ;

			# actulizar UBIGEO_ID_PADRE provincias
			DB::select('UPDATE ubigeo AS provincia
						SET ubigeo_id_padre = departamento.id
						FROM ubigeo AS departamento
						WHERE departamento.codigo = concat(left(provincia.codigo, 2),\'0000\') AND departamento.tipo_ubigeo_id =  1
						AND provincia.tipo_ubigeo_id = 2 ;
					') ;


			# actulizar UBIGEO_ID_PADRE distritos
			DB::select('UPDATE ubigeo AS distrito
						SET ubigeo_id_padre =   provincia.id
						FROM ubigeo AS provincia, ubigeo AS departamento
						WHERE provincia.codigo = concat(left(distrito.codigo, 4),\'00\') AND provincia.tipo_ubigeo_id =  2
						AND departamento.codigo = concat(left(distrito.codigo, 2),\'0000\') AND departamento.tipo_ubigeo_id =  1
						AND distrito.tipo_ubigeo_id = 3 ;
 					') ;

			# ================================================================================
			# actulizar DESCRIPCION DEPARTAMENTOS
			DB::select('
						UPDATE ubigeo
						SET descripcion = ubigeo.ubigeo
						WHERE ubigeo.codigo = concat(left(ubigeo.codigo,2),\'0000\') ;
					') ;

			# actulizar DESCRIPCION PROVINCIAS
			DB::select('
					UPDATE ubigeo AS provincia
					SET descripcion =  concat( provincia.ubigeo ,\', \' ,departamento.ubigeo)
					FROM ubigeo AS departamento
					WHERE departamento.codigo = concat(left(provincia.codigo, 2),\'0000\') AND departamento.tipo_ubigeo_id =  1
					AND provincia.tipo_ubigeo_id = 2 ;
				') ;

			# actulizar DESCRIPCION DISTRITOS
			DB::select('
					UPDATE ubigeo AS distrito
					SET descripcion =  concat(distrito.ubigeo,\', \' , provincia.ubigeo ,\', \' ,departamento.ubigeo)
					FROM ubigeo AS provincia, ubigeo AS departamento
					WHERE provincia.codigo = concat(left(distrito.codigo, 4),\'00\') AND provincia.tipo_ubigeo_id =  2
					AND departamento.codigo = concat(left(distrito.codigo, 2),\'0000\') AND departamento.tipo_ubigeo_id =  1
					AND distrito.tipo_ubigeo_id = 3 ;
				') ;






		/**
		* SQL for MYSQL
		*/
			# actulizar UBIGEO_ID_PADRE DEPARTAMENTOS


	/*			DB::select('UPDATE ubigeo
							SET  ubigeo_id_padre = 0
							WHERE    ubigeo.id  = ubigeo.id
							AND ubigeo.codigo = concat(left(ubigeo.codigo,2),"0000"); ') ;

				# actulizar UBIGEO_ID_PADRE provincias
				DB::select("UPDATE ubigeo AS provincia
							INNER JOIN ubigeo AS departamento ON departamento.codigo = concat(left(provincia.codigo, 2),'0000') AND departamento.tipo_ubigeo_id =  1
							SET
							provincia.ubigeo_id_padre = departamento.id
							WHERE provincia.tipo_ubigeo_id = 2  ;") ;

				# actulizar UBIGEO_ID_PADRE distritos
				DB::select("UPDATE ubigeo AS distrito
							INNER JOIN ubigeo AS provincia ON provincia.codigo = concat(left(distrito.codigo, 4),'00') AND provincia.tipo_ubigeo_id =  2
							INNER JOIN ubigeo AS departamento ON departamento.codigo = concat(left(distrito.codigo, 2),'0000') AND departamento.tipo_ubigeo_id =  1
							SET
							distrito.ubigeo_id_padre =   provincia.id
							WHERE distrito.tipo_ubigeo_id = 3 ; " ) ;




			# actulizar descripcion departamento



				DB::select('UPDATE ubigeo
							SET  descripcion = ubigeo.ubigeo
							WHERE  ubigeo.codigo = concat(left(ubigeo.codigo,2),"0000");') ;

				# actulizar descripcion provincias
				DB::select("UPDATE ubigeo AS provincia
							INNER JOIN ubigeo AS departamento ON departamento.codigo = concat(left(provincia.codigo, 2),'0000') AND departamento.tipo_ubigeo_id =  1
							SET
							provincia.descripcion =  concat( provincia.ubigeo ,', ' ,departamento.ubigeo)
							WHERE provincia.tipo_ubigeo_id = 2") ;

				# actulizar descripcion distritos
				DB::select("UPDATE ubigeo AS distrito
							INNER JOIN ubigeo AS provincia ON provincia.codigo = concat(left(distrito.codigo, 4),'00') AND provincia.tipo_ubigeo_id =  2
							INNER JOIN ubigeo AS departamento ON departamento.codigo = concat(left(distrito.codigo, 2),'0000') AND departamento.tipo_ubigeo_id =  1
							SET
							distrito.descripcion =  concat(distrito.ubigeo,', ' , provincia.ubigeo ,', ' ,departamento.ubigeo)
							WHERE distrito.tipo_ubigeo_id = 3" ) ;*/


			/*SELECT * FROM ubigeo_2 WHERE  codigo1 = CONCAT(left(codigo1,2),"0000") ;
			SELECT * FROM ubigeo_2 WHERE codigo1 = CONCAT(left(codigo1,4),"00")  AND codigo1 <> CONCAT(left(codigo1,2),"0000") ;
			SELECT * FROM ubigeo_2 WHERE codigo1 <> CONCAT(left(codigo1,4),"00")  AND codigo1 <> CONCAT(left(codigo1,2),"0000") ;*/

    }
}