<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # First tables create
        $this->call(TipoControlTableSeeder::class);
        $this->call(TipoDireccionTableSeeder::class);
        $this->call(TipoDocIdentidadTableSeeder::class);
        $this->call(TipoTelefonoTableSeeder::class);
        $this->call(TipoRelacionTableSeeder::class);
        $this->call(TipoWebTableSeeder::class);

        // accesos usuario
        $this->call(ControlTableSeeder::class);
        $this->call(RolTableSeeder::class);
        $this->call(UbigeoTableSeeder::class);


        /*$this->call(TipoOperacionSeeder::class);
        $this->call(TipoPagoSeeder::class);
        $this->call(TipoComprobanteSeeder::class);
        $this->call(MonedaSeeder::class);*/


       /* $this->call(ParametroSeeder::class);
        $this->call(IgvSeeder::class);*/

        $this->call(SpotSeeder::class);
        // prueba data

        $this->call(SudoEmpresaSeeder::class);

        # para pruebas
        $this->call(EmpresaTableSeeder::class) ;


        $this->call(SudoUserSeeder::class);


        $this->call(PerNaturalSeeder::class);
        $this->call(PerJuridicasSeeder::class);
        $this->call(UsersSeederTable::class);

        /*$this->call(PuntoEmisionSeeder::class);


        $this->call(AreaCargoSeeder::class);
        $this->call(CorrelativoSeeder::class);*/



        // $this->call(ProductoForPrestacionSeeder::class);

    }
}
