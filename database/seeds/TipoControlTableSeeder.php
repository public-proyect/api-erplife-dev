<?php

use Illuminate\Database\Seeder;

class TipoControlTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array('Module', 'Menu', 'Button') ;

    	for ($i=0; $i < count($data) ; $i++)
    	{
    		 DB::table('tipo_control')->insert(array(
                    'descripcion'=> $data[$i],
					'glosa'=> $data[$i],
					'estado'=> 1
				)
	        );
    	}
    }
}
