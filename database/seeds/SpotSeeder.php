<?php

use Illuminate\Database\Seeder;

class SpotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$json = File::get(base_path()."/database/seeds/jsondata/spot.json");
        $data = json_decode($json);
        $timestamp = date('Y-m-d H:m:s') ;

        foreach ($data as $object) {
            \DB::table('spot')->insert(array(
				'descripcion' => $object->descripcion ,
				'porcentaje'  => $object->porcentaje ,
				'glosa'       => $object->glosa ?: '' ,
				'estado'      => $object->estado ,
				'created_at'  => $object->created_at ?: $timestamp,
				'updated_at'  => $object->updated_at ?: $timestamp ,
            ));
        }
    }
}
