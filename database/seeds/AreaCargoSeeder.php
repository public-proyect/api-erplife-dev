<?php

use Illuminate\Database\Seeder;

class AreaCargoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	# area
        $json = File::get(base_path()."/database/seeds/jsondata/area.json");
        $data = json_decode($json);

        foreach ($data as $object) {
            \DB::table('area')->insert(array(
				'per_id_padre' => $object->per_id_padre,
				'descripcion'  => $object->descripcion,
				'glosa'        => $object->glosa,
				'estado'       => $object->estado,
            ));
        }

        # cargo
        $json = File::get(base_path()."/database/seeds/jsondata/cargo.json");
        $data = json_decode($json);

        foreach ($data as $object) {
            \DB::table('cargo')->insert(array(
				'area_id'      => $object->area_id,
				'per_id_padre' => $object->per_id_padre,
				'descripcion'  => $object->descripcion,
				'glosa'        => $object->glosa,
				'estado'       => $object->estado,
            ));
        }


    }
}
