<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class EmpresaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

			$create           = date('Y-m-d H:m:s') ;

			$faker = Faker::create();

	        for($i = 0; $i < 100; $i ++)
	        {
	        	$nombre = $faker->company ;



	        	$empresa_id = DB::table('empresa')->insertGetId(array(
									'nombre'    => $nombre,
									'fecha_reg' => $create,
									'estado'    => 1,
									'created_at'    => $create,
									'updated_at'    => $create,
	                        )
	                    );

	        	$persona = DB::table('persona')->insert(array(
									'per_nombre'    => $nombre,
									'per_apellidos' => $nombre ,
									'per_fecha_nac' => $create,
									'per_tipo'      => 2,
									'per_id_padre'  => 0,
									'empresa_id'    => $empresa_id,
									'created_at'    => $create,
									'updated_at'    => $create,
	                        )
	                    );

	        	$company = $faker->company ;
	        	$persona = DB::table('persona')->insert(array(
									'per_nombre'    => $company,
									'per_apellidos' => $company ,
									'per_fecha_nac' => $create,
									'per_tipo'      => 2,
									'per_id_padre'  => 0,
									'empresa_id'    => $empresa_id,
									'created_at'    => $create,
									'updated_at'    => $create,
	                        )
	                    );

	        	$company = $faker->company ;
	        	$persona = DB::table('persona')->insert(array(
									'per_nombre'    => $company,
									'per_apellidos' => $company ,
									'per_fecha_nac' => $create,
									'per_tipo'      => 2,
									'per_id_padre'  => 0,
									'empresa_id'    => $empresa_id,
									'created_at'    => $create,
									'updated_at'    => $create,
	                        )
	                    );



	        }




    }
}
