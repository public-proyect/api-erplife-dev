<?php

use Illuminate\Database\Seeder;

class TipoDocIdentidadTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data_json = File::get(base_path()."/database/seeds/jsondata/tipo_per_doc_identidad.json");
        $data = json_decode($data_json);

        foreach ($data as $object){
            DB::table('tipo_per_doc_identidad')->insert(array(
                'descripcion' => $object->descripcion ,
                'glosa'       => $object->glosa ,
                'estado'      => $object->estado ,
            ));
        }

    }
}
