<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class PerJuridicasSeeder extends Seeder
{
	// php artisan db:seed --class=PerJuridicasSeeder
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();

		$per_id_padre = 1 ;
		$empresa_id   = 0 ;


        for($i = 0; $i < 100; $i ++)
        {
            $companyName = $faker->company;
            $nombre_comercial = $faker->companySuffix;
            $create           = date('Y-m-d H:m:s') ;

            $persona_id = \DB::table('persona')->insertGetId(array (
					'per_nombre'    => $companyName,
					'per_apellidos' => $nombre_comercial,
					'per_fecha_nac' => $faker->dateTimeBetween($startDate = '-45 years', $endDate = '-15 years'  )->format('Y-m-d'),
					'per_id_padre'  => $per_id_padre,
					'empresa_id'    => $empresa_id,
					'per_tipo'      => 2,
					'estado'        => 1,
            ));

            $ruc = "2".$faker->numerify('##########') ;

            \DB::table('per_juridica')->insert(array (
				'persona_id'       => $persona_id ,
				'ruc'              => $ruc ,
				'razon_social'     => $companyName,
				'nombre_comercial' => $nombre_comercial ,
            ));

        	DB::table('per_doc_identidad')->insert(array(
					'persona_id'            => $persona_id ,
					'tipo_per_doc_identidad_id' => 1,
					'numero'                => $ruc,
					'fecha_emision'         => $faker->dateTimeBetween($startDate = '-3 years', $endDate = '+5 years'  )->format('Y-m-d'),
					'imagen'                => '',
					'estado'                => 1,
	            )
	        );

			\DB::table('per_mail')->insert(array (
				'persona_id'   => $persona_id ,
				'mail'          => $faker->unique()->email,
				'item'    => 1,
			));

			\DB::table('per_telefono')->insert(array (
				'persona_id'       => $persona_id ,
				'tipo_telefono_id' => 1,
				'telefono'         => $faker->numerify('9########'),
				'item'             => 1,
			));

			\DB::table('per_relacion')->insert(array(
					'per_id_padre'     => 1,
					'tipo_relacion_id' => 7,
					'persona_id'       => $persona_id,
					'referencia'       => 'Persona Juridica',
					'created_at'       => $create,
					'estado'           => 1,
	            )
	        );

    	}
    }
}
