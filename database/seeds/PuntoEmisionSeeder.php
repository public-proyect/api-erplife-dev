<?php

use Illuminate\Database\Seeder;

class PuntoEmisionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	# sucursal
        $json = File::get(base_path()."/database/seeds/jsondata/sucursal.json");
        $data = json_decode($json);

        foreach ($data as $object) {
            \DB::table('sucursal')->insert(array(
				'per_id_padre'    => $object->per_id_padre,
				'item'            => $object->item,
				'departamento_id' => $object->departamento_id,
				'codigo'          => $object->codigo,
				'nombre'          => $object->nombre,
				'descripcion'     => $object->descripcion,
				'estado'          => $object->estado,
				'created_at'      => $object->created_at,
				'updated_at'      => $object->updated_at,
            ));
        }

        # tienda
        $json = File::get(base_path()."/database/seeds/jsondata/tienda.json");
        $data = json_decode($json);

        foreach ($data as $object) {
            \DB::table('tienda')->insert(array(
				'per_id_padre' => $object->per_id_padre,
				'sucursal_id'  => $object->sucursal_id,
				'ubigeo_id'    => $object->ubigeo_id,
				'codigo'       => $object->codigo,
				'nombre'       => $object->nombre,
				'descripcion'  => empty($object->descripcion)? '' : $object->descripcion,
				'direccion'    => $object->direccion,
				'estado'       => $object->estado,
				'created_at'   => $object->created_at,
				'updated_at'   => $object->updated_at,
            ));
        }

        # tipo_almacen
        $json = File::get(base_path()."/database/seeds/jsondata/tipo_almacen.json");
        $data = json_decode($json);

        foreach ($data as $object) {
            \DB::table('tipo_almacen')->insert(array(
				'per_id_padre' => $object->per_id_padre,
				'descripcion'  => $object->descripcion,
				'glosa'        => empty($object->glosa)? '' : $object->glosa,
				'estado'       => $object->estado,
            ));
        }

        # almacen
        $json = File::get(base_path()."/database/seeds/jsondata/almacen.json");
        $data = json_decode($json);

        foreach ($data as $object) {
            \DB::table('almacen')->insert(array(
				'per_id_padre'    => $object->per_id_padre ,
				'tienda_id'       => $object->tienda_id ,
				'tipo_almacen_id' => $object->tipo_almacen_id ,
				'codigo'          => $object->codigo ,
				'nombre'          => $object->nombre ,
				'direccion'       => $object->direccion ,
				'telefono'        => $object->telefono ,
				'estado'          => $object->estado ,
				'created_at'      => $object->created_at ,
				'updated_at'      => $object->updated_at ,
            ));
        }

         # punto_emision
        $json = File::get(base_path()."/database/seeds/jsondata/punto_emision.json");
        $data = json_decode($json);

        foreach ($data as $object) {
            \DB::table('punto_emision')->insert(array(
				'per_id_padre'   => $object->per_id_padre,
				'tienda_id'      => $object->tienda_id,
				'codigo'         => $object->codigo,
				'nombre'         => $object->nombre,
				'emite_ticket'   => $object->emite_ticket,
				'modelo_maquina' => empty($object->modelo_maquina) ? '' : $object->modelo_maquina,
				'serie_maquina'  => empty($object->serie_maquina) ? '' : $object->serie_maquina,
				'estado'         => $object->estado,
				'created_at'     => $object->created_at,
				'updated_at'     => $object->updated_at,
            ));
        }




    }
}
