<?php

use Illuminate\Database\Seeder;

class ProductoForPrestacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	# unidad de medida
        $json = File::get(base_path()."/database/seeds/jsondata/unidad_medida.json");
        $data = json_decode($json);

        foreach ($data as $object) {
            \DB::table('unidad_medida')->insert(array(
                'unidad_medida_id' => $object->unidad_medida_id,
                'per_id_padre'     => $object->per_id_padre,
                'abreviatura'      => $object->abreviatura,
                'descripcion'      => $object->descripcion,
                'equivalente'      => $object->equivalente,
                'estado'           => $object->estado,
            ));
        }

        #  marca
        $json = File::get(base_path()."/database/seeds/jsondata/marca.json");
        $data = json_decode($json);

        foreach ($data as $object) {
            \DB::table('marca')->insert(array(
                'per_id_padre' => $object->per_id_padre,
                'descripcion' => $object->descripcion,
                'glosa' => $object->glosa,
                'estado' => $object->estado,
            ));
        }

        $json = File::get(base_path()."/database/seeds/jsondata/modelo.json");
        $data = json_decode($json);

        foreach ($data as $object) {
            \DB::table('modelo')->insert(array(
                'per_id_padre' => $object->per_id_padre,
                'marca_id'     => $object->marca_id,
                'descripcion'  => $object->descripcion,
                'glosa'        => $object->glosa,
                'estado'       => $object->estado,
            ));
        }

        # productos
        $json = File::get(base_path()."/database/seeds/jsondata/producto.json");
        $data = json_decode($json);

        foreach ($data as $object) {
            \DB::table('producto')->insert(array(
                'per_id_padre' => $object->per_id_padre,
                'codigo'       => $object->codigo,
                'descripcion'  => $object->descripcion,
                'percepcion'   => $object->percepcion,
                'estado'       => $object->estado,
                'created_at'   => $object->created_at,
                'updated_at'   => $object->updated_at,
            ));
        }

        # variantes
        $json = File::get(base_path()."/database/seeds/jsondata/variante.json");
        $data = json_decode($json);

        foreach ($data as $object) {
            \DB::table('variante')->insert(array(
                'producto_id'  => $object->producto_id,
                'per_id_padre' => $object->per_id_padre,
                'codigo'       => $object->codigo,
                'descripcion'  => $object->descripcion,
                'estado'       => $object->estado,
            ));
        }

        # presentacion
        $json = File::get(base_path()."/database/seeds/jsondata/presentacion.json");
        $data = json_decode($json);

        foreach ($data as $object) {
            \DB::table('presentacion')->insert(array(
                'variante_id'      => $object->variante_id,
                'modelo_id'        => $object->modelo_id,
                'stock_min'        => $object->stock_min,
                'unidad_medida_id' => $object->unidad_medida_id,
                'descripcion'      => $object->descripcion,
                'estado'           => $object->estado,
                'precio_base'      => $object->precio_base,

            ));
        }

        # stock
        $json = File::get(base_path()."/database/seeds/jsondata/stock.json");
        $data = json_decode($json);

        foreach ($data as $object) {
            \DB::table('stock')->insert(array(
                'presentacion_id' => $object->presentacion_id,
                'almacen_id'      => $object->almacen_id,
                'per_id_padre'    => $object->per_id_padre,
                'stock_actual'    => $object->stock_actual,
                'estado'          => $object->estado,
                'created_at'      => $object->created_at,
                'updated_at'      => $object->updated_at,
            ));
        }



    }
}
