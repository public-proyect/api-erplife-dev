<?php

use Illuminate\Database\Seeder;

class TipoDireccionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array('Dirección Domicilio','Dirección Fiscal') ;

    	for ($i=0; $i < count($data) ; $i++)
    	{
    		 DB::table('tipo_direccion')->insert(array(
                    'descripcion'=> $data[$i],
                    'glosa'=> $data[$i],
					'estado'=> 1
				)
	        );
    	}
    }
}
