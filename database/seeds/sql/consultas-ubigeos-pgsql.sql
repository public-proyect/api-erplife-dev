﻿PDATE ubigeo
SET  ubigeo_id_padre = 0
WHERE    ubigeo.id  = ubigeo.id
AND ubigeo.codigo = concat(left(ubigeo.codigo,2),'0000') ;

--------------------------------------------------------------------

UPDATE ubigeo AS provincia
SET ubigeo_id_padre = departamento.id
FROM ubigeo AS departamento
WHERE departamento.codigo = concat(left(provincia.codigo, 2),'0000') AND departamento.tipo_ubigeo_id =  1
AND provincia.tipo_ubigeo_id = 2 ;

----------------------------------------------------------------------

UPDATE ubigeo AS distrito
SET ubigeo_id_padre =   provincia.id
FROM ubigeo AS provincia, ubigeo AS departamento
WHERE provincia.codigo = concat(left(distrito.codigo, 4),'00') AND provincia.tipo_ubigeo_id =  2
AND departamento.codigo = concat(left(distrito.codigo, 2),'0000') AND departamento.tipo_ubigeo_id =  1
AND distrito.tipo_ubigeo_id = 3 ;

------------------------------------------------------------------------


UPDATE ubigeo
SET descripcion = ubigeo.ubigeo
WHERE ubigeo.codigo = concat(left(ubigeo.codigo,2),'0000') ;
-----------------------------------------------------------------------

UPDATE ubigeo AS provincia
SET descripcion =  concat( provincia.ubigeo ,', ' ,departamento.ubigeo)
FROM ubigeo AS departamento
WHERE departamento.codigo = concat(left(provincia.codigo, 2),'0000') AND departamento.tipo_ubigeo_id =  1
AND provincia.tipo_ubigeo_id = 2 ;
----------------------------------------------------------------------

UPDATE ubigeo AS distrito
SET descripcion =  concat(distrito.ubigeo,', ' , provincia.ubigeo ,', ' ,departamento.ubigeo)
FROM ubigeo AS provincia, ubigeo AS departamento
WHERE provincia.codigo = concat(left(distrito.codigo, 4),'00') AND provincia.tipo_ubigeo_id =  2
AND departamento.codigo = concat(left(distrito.codigo, 2),'0000') AND departamento.tipo_ubigeo_id =  1
AND distrito.tipo_ubigeo_id = 3 ;