<?php

use Illuminate\Database\Seeder;

class MonedaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data = array(
		    		array('S/','Soles',),
		    		array('$','Dolares',),
	    		) ;

    	// moneda (per_id_padre, simbolo, descripcion, estado

    	for ($i=0; $i < count($data) ; $i++)
    	{
    		 DB::table('moneda')->insert(array(
					'per_id_padre' => 1,
					'simbolo'      => $data[$i][0],
					'descripcion'  => $data[$i][1],
					'estado'       => 1
				)
	        );
    	}
    }
}
