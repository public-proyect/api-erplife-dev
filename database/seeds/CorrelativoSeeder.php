<?php

use Illuminate\Database\Seeder;

class CorrelativoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	# correlativos
        $json = File::get(base_path()."/database/seeds/jsondata/correlativo.json");
        $data = json_decode($json);

        foreach ($data as $object) {
            \DB::table('correlativo')->insert(array(
				'per_id_padre'        => $object->per_id_padre,
				'tipo_comprobante_id' => $object->tipo_comprobante_id,
				'punto_emision_id'    => $object->punto_emision_id,
				'serie'               => $object->serie,
				'numero_ini'          => $object->numero_ini,
				'numero_fin'          => $object->numero_fin,
				'numero_actual'       => $object->numero_actual,
				'estado'              => $object->estado,
            ));
        }



    }
}
