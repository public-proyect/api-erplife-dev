<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class UsersSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// factory(App\User::class,20)->create();

			$faker = Faker::create();

			$create = date('Y-m-d H:m:s') ;
			$rol_id = 1 ;


	        for($i = 0; $i < 200; $i ++)
	        {

	        	$empresas = App\Models\Personas\Empresa::where('estado','=',1)->get() ;
	        	$empresas_id = $empresas->random()->id;

	        	$empresas_persona = App\Models\Personas\Persona::where('empresa_id','<>',$empresas_id)->get() ;

	        	if (!$empresas_persona->isEmpty())
	        	{
	        		$persona_id_empresa = $empresas_persona->random()->id ;

		    	 	$user_id =  DB::table('users')->insertGetId(array(
							'email'      => $faker->unique()->safeEmail,
							'password'   => Hash::make('armando'),
							'alias'      => 'armandoaepp',
							'empresa_id' => $empresas_id,
							'created_at' => $create,
							'updated_at' => $create,
							'estado'     => 1,
						)
			        );

			        DB::table('users_persona')->insert(array(
							'per_id_padre' => $persona_id_empresa,
							'user_id'      => $user_id,
							// 'persona_id'   => $persona_id,
							'rol_id'       => $rol_id,
							'estado'       => 1,
			            )
			        );


			        DB::table('user_info')->insert(array(
							'user_id'      => $user_id,
							'dni'          => $faker->numerify('########'),
							'nombre'       => $faker->name  ,
							'apellidos'    => $faker->lastName ,
							// 'avatar'       => 'avatars/armandoaepp.png',
							'avatar'       => $faker->imageUrl($width = 250, $height = 180),
							'telefonos'    => $faker->numerify('9########'),
							'direccion'    => $faker->streetAddress,
							'estado'       => 1,
			            )
			        );
	        	}


		    }

    }
}
