<?php

use Illuminate\Database\Seeder;

class ParametroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	#  parametro
        $json = File::get(base_path()."/database/seeds/jsondata/parametro.json");
        $data = json_decode($json);

        foreach ($data as $object) {
            \DB::table('parametro')->insert(array(
				'per_id_padre'  => $object->per_id_padre,
				'par_codigo'    => $object->par_codigo,
				'par_clase'     => $object->par_clase,
				'par_jerarquia' => $object->par_jerarquia,
				'descripcion'   => $object->descripcion,
				'glosa'         => !empty($object->glosa) ? $object->glosa : '',
				'estado'        => $object->estado,
            ));
        }
    }
}
