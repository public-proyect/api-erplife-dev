<?php

use Illuminate\Database\Seeder;

class TipoPagoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$json = File::get(base_path()."/database/seeds/jsondata/tipo_pago.json");
		$data = json_decode($json);


		foreach ($data as $object) {
			\DB::table('tipo_pago')->insert(array(
				'descripcion' => $object->descripcion ,
				'glosa'        => $object->glosa ,
				'estado'      => $object->estado
			));
		}

    }
}
