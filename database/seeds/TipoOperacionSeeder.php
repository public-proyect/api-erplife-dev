<?php

use Illuminate\Database\Seeder;

class TipoOperacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$json = File::get(base_path()."/database/seeds/jsondata/tipo_operacion.json");
  		$data = json_decode($json);


  		foreach ($data as $object) {
  			\DB::table('tipo_operacion')->insert(array(
  				'descripcion' => $object->descripcion ,
  				'tipo'        => $object->tipo ,
  				'simbolo'     => $object->simbolo ,
  				'estado'      => $object->estado
  			));
  		}
    }
}
