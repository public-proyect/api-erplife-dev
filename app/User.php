<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                    'rol_id',
                    'email',
                    'password',
                    'alias',
                    'fecha',
                    'estado',
                    'empresa_id'
                    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function users_sesion(){
        return $this->hasMany('App\Models\Accesos\UserSesion','user_id');
    }
    public function bitacoras(){
        return $this->hasMany('App\Models\Accesos\Bitacora','user_id');
    }
    public function notificaciones(){
        return $this->hasMany('App\Models\Accesos\Notificacion','user_id');
    }

    public function oauth_identitiesses(){
        return $this->hasMany('App\Models\Accesos\OauthIdentities','user_id');
    }
    public function accesos_entidad(){
        return $this->hasMany('App\Models\Accesos\AccesoEntidad','user_id');
    }
    public function users_personas(){
        return $this->hasMany('App\Models\Accesos\UsersPersona','user_id');
    }
}
