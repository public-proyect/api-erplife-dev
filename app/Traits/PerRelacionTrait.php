<?php
namespace App\Traits;
use App\Http\Controllers\Personas\PerRelacionController ;

trait PerRelacionTrait {

    public function setPersonaTipoRelacion($params)
    {
        $ctrl = new PerRelacionController() ;
        $ctrl->setPersonaTipoRelacion($params);
    }

    public function updateEstadoPerRelacion($params)
    {
        $ctrl = new PerRelacionController() ;
        $ctrl->updateEstadoPerRelacion($params);
    }

}