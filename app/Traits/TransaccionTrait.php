<?php
namespace App\Traits;
use App\Http\Controllers\Accesos\TransaccionesController ;

trait TransaccionTrait {

    public function savedTransTrait($model,$type)
    {
        if (empty($model)) return ;
        $ctrl = new TransaccionesController() ;
        $ctrl->save($model,$type );
    }

    public function createdTransTrait($model = null)
    {
        if (empty($model)) return ;
        $ctrl = new TransaccionesController() ;
        $ctrl->created($model );
    }

    public function updatedTransTrait($model = null)
    {
        if (empty($model)) return ;
        $ctrl = new TransaccionesController() ;
        $ctrl->updated($model );
    }

    public function deletedTransTrait($model = null)
    {
        if (empty($model)) return ;
        $ctrl = new TransaccionesController() ;
        $ctrl->deleted($model );
    }

}