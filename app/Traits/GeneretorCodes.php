<?php
namespace App\Traits;

trait GeneretorCodes {

    public function generateCodeNumber($value,$len_generate = 3)
    {
        $longitud = strlen($value);
        if ($longitud >= $len_generate)
            return $value ;

        $code =  str_pad($value, $len_generate, "0", STR_PAD_LEFT);

        return $code ;
    }

    /**
    * value es un numero o letra menor o igual a 7 minimo 3letras-4numeros
    */
    public function generateCodeAlfaNumber($value = 0)
    {
        $longitud = strlen($value);
        if ($longitud >= 7)
            return $value ;

        if($longitud < 4)
        {
            $codigo_prex = $this->generateCodeNumber($value,4) ;
            $codigo_alfa = $this->generateCodeAlfa(3) ;
            $code        = $codigo_alfa.$codigo_prex ;
        }
        else
        {
           $code = $this->generateCodeAlfa(7-$longitud) ;
        }
        return $code ;
    }

    public function generateCodeAlfa($longitud)
    {
         $key = '';
         $pattern = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
         $max = strlen($pattern)-1;
         for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
         return $key;
    }

    public function generateCodeCliente($value = 0)
    {
        $longitud = strlen($value);
        if ($longitud >= 8)
            return $value ;

        $longitud = 8 - $longitud ;

        $cod = $this->generateCodeAlfa($longitud) ;
        $codigo = $cod.$value ;

        return $codigo ;
    }



    public function generateCodeAlfaNumeric($longitud)
    {
         $key = '';
         $pattern = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
         $max = strlen($pattern)-1;
         for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
         return $key;
    }



    # genera un codigo de persona_padre y valor(table)
    public function generateCodePerIdPadreValue($value,$len_generate = 3,$comodin='-')
    {
        $user = \Auth::user();
        $per_id_padre = $user->persona_id_padre ;
        $per_id_padre = $this->generateCodeNumber($per_id_padre,3) ;

        $longitud = strlen($value);
        if ($longitud >= $len_generate)
        {
            $code = $per_id_padre.$comodin.$value;
            return $code ;
        }
        else
        {
            $code_object =  $this->generateCodeNumber($value,$len_generate) ;
            $code = $per_id_padre.$comodin.$code_object;
            return $code ;
        }
    }


}