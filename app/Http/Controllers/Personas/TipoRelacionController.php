<?php

namespace App\Http\Controllers\Personas;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Personas\TipoRelacion;
use JWTAuth;


class TipoRelacionController extends Controller
{
    public function __construct(JWTAuth $auth)
    {
        $this->middleware('jwt.auth');
    }

    public function getTiposRelacion()
	{
        $data = TipoRelacion::where('estado',1)->get() ;

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function save(Request $request)
    {

        $descripcion = $request->input('descripcion') ;
        $glosa = $request->input('glosa') ;

    	$rules = [
            	'descripcion'      => 'descripcion',
            ];

        try 
        {

	        $tipo_relacion = TipoRelacion::where(['descripcion' => $descripcion])->first();

	        if (!$tipo_relacion)
	        {
	        	$tipo_relacion = new TipoRelacion() ;
                $tipo_relacion->descripcion = $descripcion ;
                $tipo_relacion->glosa = $glosa ;
	        	$tipo_relacion->save() ;
	        }
	        else
	        {
                $tipo_relacion->descripcion = $descripcion ;
                $tipo_relacion->glosa = $glosa ;
	        	$tipo_relacion->estado = 1 ;
	        	$tipo_relacion->save() ;
	        }

	        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => "OK",
	                    ]);

        } 
        catch (Exception $e)
        {
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }
    }

	public function getTipoRelacionById($id)
    {

        $tipo_relacion_id = $id ;

        $data = TipoRelacion::where('id',$tipo_relacion_id)->first() ;

        return response()->json([
                	'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function update(Request $request)
    {

        $tipo_relacion_id = $request->input('id') ;
        $descripcion = $request->input('descripcion') ;
        $glosa = $request->input('glosa') ;

        $tipo_relacion =  TipoRelacion::find($tipo_relacion_id) ;
		$tipo_relacion->descripcion = $descripcion ;
		$tipo_relacion->glosa = $glosa ;
        $tipo_relacion->save() ;

        $data =  TipoRelacion::find($tipo_relacion_id) ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function updateEstado(Request $request)
    {

		$tipo_relacion_id = $request->input('id') ;
		$estado = $request->input('estado') ;

        $tipo_relacion =  TipoRelacion::find($tipo_relacion_id) ;
        $tipo_relacion->estado = $estado ;
        $tipo_relacion->save() ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $estado,
                ]);
    }
}
