<?php

namespace App\Http\Controllers\Personas;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Personas\TipoWeb;
use JWTAuth;


class TipoWebController extends Controller
{
    public function __construct(JWTAuth $auth)
    {
        $this->middleware('jwt.auth');
    }

    public function getTiposWeb()
	{
        $data = TipoWeb::where('estado',1)->get() ;

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function save(Request $request)
    {

        $descripcion = $request->input('descripcion') ;
        $glosa = $request->input('glosa') ;

    	$rules = [
            	'descripcion'      => 'descripcion',
            ];

        try 
        {

	        $tipo_web = TipoWeb::where(['descripcion' => $descripcion])->first();

	        if (!$tipo_web)
	        {
	        	$tipo_web = new TipoWeb() ;
                $tipo_web->descripcion = $descripcion ;
                $tipo_web->glosa = $glosa ;
	        	$tipo_web->save() ;
	        }
	        else
	        {
                $tipo_web->descripcion = $descripcion ;
                $tipo_web->glosa = $glosa ;
	        	$tipo_web->estado = 1 ;
	        	$tipo_web->save() ;
	        }

	        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => "OK",
	                    ]);

        } 
        catch (Exception $e)
        {
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }
    }

	public function getTipoWebById($id)
    {

        $tipo_web_id = $id ;

        $data = TipoWeb::where('id',$tipo_web_id)->first() ;

        return response()->json([
                	'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function update(Request $request)
    {

        $tipo_web_id = $request->input('id') ;
        $descripcion = $request->input('descripcion') ;
        $glosa = $request->input('glosa') ;

        $tipo_web =  TipoWeb::find($tipo_web_id) ;
		$tipo_web->descripcion = $descripcion ;
		$tipo_web->glosa = $glosa ;
        $tipo_web->save() ;

        $data =  TipoWeb::find($tipo_web_id) ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function updateEstado(Request $request)
    {

		$tipo_web_id = $request->input('id') ;
		$estado = $request->input('estado') ;

        $tipo_web =  TipoWeb::find($tipo_web_id) ;
        $tipo_web->estado = $estado ;
        $tipo_web->save() ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $estado,
                ]);
    }
}
