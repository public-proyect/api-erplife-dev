<?php

namespace App\Http\Controllers\Personas;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Personas\TipoDireccion;
use JWTAuth;


class TipoDireccionController extends Controller
{
    public function __construct(JWTAuth $auth)
    {
        $this->middleware('jwt.auth');
    }

    public function getTiposDireccion()
	{
        $data = TipoDireccion::where('estado',1)->get() ;

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function save(Request $request)
    {

        $descripcion = $request->input('descripcion') ;
        $glosa = $request->input('glosa') ;

    	$rules = [
            	'descripcion'      => 'descripcion',
            ];

        try 
        {

	        $tipo_direccion = TipoDireccion::where(['descripcion' => $descripcion])->first();

	        if (!$tipo_direccion)
	        {
	        	$tipo_direccion = new TipoDireccion() ;
                $tipo_direccion->descripcion = $descripcion ;
                $tipo_direccion->glosa = $glosa ;
	        	$tipo_direccion->save() ;
	        }
	        else
	        {
                $tipo_direccion->descripcion = $descripcion ;
                $tipo_direccion->glosa = $glosa ;
	        	$tipo_direccion->estado = 1 ;
	        	$tipo_direccion->save() ;
	        }

	        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => "OK",
	                    ]);

        } 
        catch (Exception $e)
        {
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }
    }

	public function getTipoDireccionById($id)
    {

        $tipo_direccion_id = $id ;

        $data = TipoDireccion::where('id',$tipo_direccion_id)->first() ;

        return response()->json([
                	'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function update(Request $request)
    {

        $tipo_direccion_id = $request->input('id') ;
        $descripcion = $request->input('descripcion') ;
        $glosa = $request->input('glosa') ;

        $tipo_direccion =  TipoDireccion::find($tipo_direccion_id) ;
		$tipo_direccion->descripcion = $descripcion ;
		$tipo_direccion->glosa = $glosa ;
        $tipo_direccion->save() ;

        $data =  TipoDireccion::find($tipo_direccion_id) ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function updateEstado(Request $request)
    {

		$tipo_direccion_id = $request->input('id') ;
		$estado = $request->input('estado') ;

        $tipo_direccion =  TipoDireccion::find($tipo_direccion_id) ;
        $tipo_direccion->estado = $estado ;
        $tipo_direccion->save() ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $estado,
                ]);
    }
}
