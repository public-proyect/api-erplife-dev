<?php

namespace App\Http\Controllers\Personas;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Personas\TipoPerDocIdentidad;
use JWTAuth;


class TipoPerDocIdentidadController extends Controller
{
    public function __construct(JWTAuth $auth)
    {
        $this->middleware('jwt.auth');
    }

    public function getTiposPerDocIdentidad()
	{
        $data = TipoPerDocIdentidad::where('estado',1)->get() ;

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function save(Request $request)
    {

        $descripcion = $request->input('descripcion') ;
        $glosa = $request->input('glosa') ;

    	$rules = [
            	'descripcion'      => 'descripcion',
            ];

        try 
        {

	        $tipo_per_doc_identidad = TipoPerDocIdentidad::where(['descripcion' => $descripcion])->first();

	        if (!$tipo_per_doc_identidad)
	        {
	        	$tipo_per_doc_identidad = new TipoPerDocIdentidad() ;
                $tipo_per_doc_identidad->descripcion = $descripcion ;
                $tipo_per_doc_identidad->glosa = $glosa ;
	        	$tipo_per_doc_identidad->save() ;
	        }
	        else
	        {
                $tipo_per_doc_identidad->descripcion = $descripcion ;
                $tipo_per_doc_identidad->glosa = $glosa ;
	        	$tipo_per_doc_identidad->estado = 1 ;
	        	$tipo_per_doc_identidad->save() ;
	        }

	        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => "OK",
	                    ]);

        } 
        catch (Exception $e)
        {
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }
    }

	public function getTipoPerDocIdentidadById($id)
    {

        $tipo_per_doc_identidad_id = $id ;

        $data = TipoPerDocIdentidad::where('id',$tipo_per_doc_identidad_id)->first() ;

        return response()->json([
                	'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function update(Request $request)
    {

        $tipo_per_doc_identidad_id = $request->input('id') ;
        $descripcion = $request->input('descripcion') ;
        $glosa = $request->input('glosa') ;

        $tipo_per_doc_identidad =  TipoPerDocIdentidad::find($tipo_per_doc_identidad_id) ;
		$tipo_per_doc_identidad->descripcion = $descripcion ;
		$tipo_per_doc_identidad->glosa = $glosa ;
        $tipo_per_doc_identidad->save() ;

        $data =  TipoPerDocIdentidad::find($tipo_per_doc_identidad_id) ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function updateEstado(Request $request)
    {

		$tipo_per_doc_identidad_id = $request->input('id') ;
		$estado = $request->input('estado') ;

        $tipo_per_doc_identidad =  TipoPerDocIdentidad::find($tipo_per_doc_identidad_id) ;
        $tipo_per_doc_identidad->estado = $estado ;
        $tipo_per_doc_identidad->save() ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $estado,
                ]);
    }
}
