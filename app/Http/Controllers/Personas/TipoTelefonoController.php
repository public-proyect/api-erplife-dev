<?php

namespace App\Http\Controllers\Personas;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Personas\TipoTelefono;
use JWTAuth;


class TipoTelefonoController extends Controller
{
    public function __construct(JWTAuth $auth)
    {
        $this->middleware('jwt.auth');
    }

    public function getTiposTelefono()
	{
        $data = TipoTelefono::where('estado',1)->get() ;

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function save(Request $request)
    {

        $descripcion = $request->input('descripcion') ;
        $glosa = $request->input('glosa') ;

    	$rules = [
            	'descripcion'      => 'descripcion',
            ];

        try 
        {

	        $tipo_telefono = TipoTelefono::where(['descripcion' => $descripcion])->first();

	        if (!$tipo_telefono)
	        {
	        	$tipo_telefono = new TipoTelefono() ;
                $tipo_telefono->descripcion = $descripcion ;
                $tipo_telefono->glosa = $glosa ;
	        	$tipo_telefono->save() ;
	        }
	        else
	        {
                $tipo_telefono->descripcion = $descripcion ;
                $tipo_telefono->glosa = $glosa ;
	        	$tipo_telefono->estado = 1 ;
	        	$tipo_telefono->save() ;
	        }

	        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => "OK",
	                    ]);

        } 
        catch (Exception $e)
        {
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }
    }

	public function getTipoTelefonoById($id)
    {

        $tipo_telefono_id = $id ;

        $data = TipoTelefono::where('id',$tipo_telefono_id)->first() ;

        return response()->json([
                	'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function update(Request $request)
    {

        $tipo_telefono_id = $request->input('id') ;
        $descripcion = $request->input('descripcion') ;
        $glosa = $request->input('glosa') ;

        $tipo_telefono =  TipoTelefono::find($tipo_telefono_id) ;
		$tipo_telefono->descripcion = $descripcion ;
		$tipo_telefono->glosa = $glosa ;
        $tipo_telefono->save() ;

        $data =  TipoTelefono::find($tipo_telefono_id) ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function updateEstado(Request $request)
    {

		$tipo_telefono_id = $request->input('id') ;
		$estado = $request->input('estado') ;

        $tipo_telefono =  TipoTelefono::find($tipo_telefono_id) ;
        $tipo_telefono->estado = $estado ;
        $tipo_telefono->save() ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $estado,
                ]);
    }
}
