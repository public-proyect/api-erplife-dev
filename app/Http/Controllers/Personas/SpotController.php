<?php

namespace App\Http\Controllers\Personas;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Personas\Spot;
use JWTAuth;


class SpotController extends Controller
{
    public function __construct(JWTAuth $auth)
    {
        $this->middleware('jwt.auth');
    }

    public function getSpots()
	{
        $data = Spot::where('estado',1)->get() ;

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function save(Request $request)
    {

        $descripcion = $request->input('descripcion') ;
        $porcentaje = $request->input('porcentaje') ;
        $glosa = $request->input('glosa') ;

    	$rules = [
            	'descripcion'      => 'descripcion',
            ];

        try 
        {

	        $spot = Spot::where(['descripcion' => $descripcion])->first();

	        if (!$spot)
	        {
	        	$spot = new Spot() ;
                
                $spot->descripcion = $descripcion ;
                $spot->porcentaje = $porcentaje ;
                $spot->glosa = $glosa ;
	        	$spot->save() ;
	        }
	        else
	        {
                $spot->descripcion = $descripcion ;
                $spot->porcentaje = $porcentaje ;
                $spot->glosa = $glosa ;
	        	$spot->estado = 1 ;
	        	$spot->save() ;
	        }

	        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => "OK",
	                    ]);

        } 
        catch (Exception $e)
        {
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }
    }

	public function getSpotById($id)
    {

        $spot_id = $id ;

        $data = Spot::where('id',$spot_id)->first() ;

        return response()->json([
                	'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function update(Request $request)
    {

        $spot_id = $request->input('id') ;
        $descripcion = $request->input('descripcion') ;
        $porcentaje = $request->input('porcentaje') ;
        $glosa = $request->input('glosa') ;

        $spot =  Spot::find($spot_id) ;
		$spot->descripcion = $descripcion ;
        $spot->porcentaje = $porcentaje ;
		$spot->glosa = $glosa ;
        $spot->save() ;

        $data =  Spot::find($spot_id) ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function updateEstado(Request $request)
    {

		$spot_id = $request->input('id') ;
		$estado = $request->input('estado') ;

        $spot =  Spot::find($spot_id) ;
        $spot->estado = $estado ;
        $spot->save() ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $estado,
                ]);
    }
}
