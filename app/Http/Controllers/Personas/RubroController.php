<?php

namespace App\Http\Controllers\Personas;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Personas\Rubro;
use JWTAuth;


class RubroController extends Controller
{
    public function __construct(JWTAuth $auth)
    {
        $this->middleware('jwt.auth');
    }

    public function getRubros()
	{
        $data = Rubro::where('estado',1)->get() ;

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function save(Request $request)
    {

        $codigo = $request->input('codigo') ;
        $descripcion = $request->input('descripcion') ;
        $glosa = $request->input('glosa') ;

    	$rules = [
            	'descripcion'      => 'descripcion',
            ];

        try 
        {

	        $rubro = Rubro::where(['descripcion' => $descripcion])->first();

	        if (!$rubro)
	        {
	        	$rubro = new Rubro() ;
                $rubro->codigo = "000R1" ;
                $rubro->descripcion = $descripcion ;
                $rubro->glosa = $glosa ;
	        	$rubro->save() ;
	        }
	        else
	        {
                $rubro->codigo = $codigo ;
                $rubro->descripcion = $descripcion ;
                $rubro->glosa = $glosa ;
	        	$rubro->estado = 1 ;
	        	$rubro->save() ;
	        }

	        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => "OK",
	                    ]);

        } 
        catch (Exception $e)
        {
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }
    }

	public function getRubroById($id)
    {

        $rubro_id = $id ;

        $data = Rubro::where('id',$rubro_id)->first() ;

        return response()->json([
                	'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function update(Request $request)
    {

        $rubro_id = $request->input('id') ;
        $codigo = $request->input('codigo') ;
        $descripcion = $request->input('descripcion') ;
        $glosa = $request->input('glosa') ;

        $rubro =  Rubro::find($rubro_id) ;
        $rubro->codigo = $codigo ;
		$rubro->descripcion = $descripcion ;
		$rubro->glosa = $glosa ;
        $rubro->save() ;

        $data =  Rubro::find($rubro_id) ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function updateEstado(Request $request)
    {

		$rubro_id = $request->input('id') ;
		$estado = $request->input('estado') ;

        $rubro =  Rubro::find($rubro_id) ;
        $rubro->estado = $estado ;
        $rubro->save() ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $estado,
                ]);
    }
}
