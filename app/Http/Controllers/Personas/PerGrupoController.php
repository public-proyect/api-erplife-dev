<?php

namespace App\Http\Controllers\Personas;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Personas\PerGrupo;
use App\Models\Personas\Persona;
use App\Models\Personas\PerNatural;
use App\Models\Personas\PerJuridica;
use App\Models\Personas\PerImagen;
use App\Models\Personas\PerDireccion;
use App\Models\Personas\PerMail;
use App\Models\Personas\PerTelefono;
use App\Models\Personas\PerWeb;
use App\Models\Personas\PerDocIdentidad;
use App\Models\Personas\PerParametro;
use JWTAuth;

class PerGrupoController extends Controller
{
    public function __construct(JWTAuth $auth)
    {
        $this->middleware('jwt.auth', ['except' => ['uploadImagenProveedores', 'uploadImagenClientes']]);
    }

    public function getProveedores()
	{
        $data = PerGrupo::where([['per_grupo.estado', '=' , 1],['par_codigo', '=' , 1],['par_clase', '=' , 1001]])
                ->join('persona', function($join) {
					    $join->on('persona.id','=', 'per_grupo.persona_id')
						->where('persona.estado','=',1);
				})
                ->join('per_doc_identidad', function($join) {
						$join->on('per_doc_identidad.persona_id','=', 'persona.id')
						->where([['per_doc_identidad.estado', '=' ,1],['per_doc_identidad.item', '=' , 1],['per_doc_identidad.tipo_per_doc_identidad_id', '=' , 2]]);
				})
                ->join('per_mail', function($join) {
						$join->on('per_mail.persona_id','=', 'persona.id')
						->where([['per_mail.estado', '=' , 1],['per_mail.item', '=' , 1]]);
				})
                ->join('per_telefono', function($join) {
						$join->on('per_telefono.persona_id','=', 'persona.id')
						->where([['per_telefono.estado', '=' , 1],['per_telefono.item', '=' , 1]]);
				})
                ->select([
    					'per_grupo.id',
    					'per_grupo.codigo',
                        'persona.id as persona_id',
    					'persona.per_nombre as nombre',
    					'persona.per_apellidos as apellidos',
                        'persona.per_tipo',
                        'per_doc_identidad.numero as ruc',
                        'per_mail.mail as mail',
    					'per_telefono.telefono as telefono',
    					 ])
    					->get();

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function getClientes()
	{
        $par_clase = 4001  ;
        $per_id_padre = 1  ;
        $data = PerGrupo::where([['per_grupo.estado', '=' , 1],['per_grupo.par_codigo', '=' , 2],['per_grupo.par_clase', '=' , 1001]])
                ->join('persona', function($join) {
					    $join->on('persona.id','=', 'per_grupo.persona_id')
						->where('persona.estado','=',1);
				})
                ->join('per_doc_identidad', function($join) {
						$join->on('per_doc_identidad.persona_id','=', 'persona.id')
						->where([['per_doc_identidad.estado', '=' ,1],['per_doc_identidad.item', '=' , 1],['per_doc_identidad.tipo_per_doc_identidad_id', '=' , 2]]);
				})
                ->join('per_mail', function($join) {
						$join->on('per_mail.persona_id','=', 'persona.id')
						->where([['per_mail.estado', '=' , 1],['per_mail.item', '=' , 1]]);
				})
                ->join('per_telefono', function($join) {
						$join->on('per_telefono.persona_id','=', 'persona.id')
						->where([['per_telefono.estado', '=' , 1],['per_telefono.item', '=' , 1]]);
				})
                ->leftjoin('per_parametro', function($join) use ($par_clase) {
						$join->on('per_parametro.persona_id','=', 'persona.id')
						->where('per_parametro.estado', '=' , 1)
						->where('per_parametro.par_clase', '=' ,  $par_clase);
                })
                ->leftjoin('parametro', function($join) use ($par_clase, $per_id_padre) {
						$join->on('parametro.par_codigo', '=' , 'per_parametro.par_codigo')
						->where('parametro.par_clase', '=' , $par_clase)
						->where('parametro.per_id_padre', '=' , $per_id_padre )
                        ->where('parametro.estado', '=' , 1);						
				}) 
                ->select([
    					'per_grupo.id',
    					'per_grupo.codigo',
                        'persona.id as persona_id',
    					'persona.per_nombre as nombre',
    					'persona.per_apellidos as apellidos',
                        'persona.per_tipo',
                        'per_doc_identidad.numero as ruc',
                        'per_mail.mail as mail',
    					'per_telefono.telefono as telefono',
                        'parametro.par_descripcion as tipo_cliente'
    					 ])
    			->get();
        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function uploadImagenProveedores(Request $request){

        $file=$request->file('imagen-proveedor');
        foreach($file as $image){
            $name='proveedor_'.time().rand().'.'.$image->getClientOriginalExtension();
            $image->storeAs('proveedores',"{$name}",'asset');       
        }
        return \Response::Json(['status' => 'success',
                            'error'   => false,
                            'filename'    => $name],200);  
    } 

    public function uploadImagenClientes(Request $request){

        $file=$request->file('imagen-cliente');
        foreach($file as $image){
            $name='cliente_'.time().rand().'.'.$image->getClientOriginalExtension();
            $image->storeAs('clientes',"{$name}",'asset');       
        }
        return \Response::Json(['status' => 'success',
                            'error'   => false,
                            'filename'    => $name],200);  
    } 

    public function save(Request $request)
    {
        $per_id_padre = 1;

        $input_per_grupo = json_decode($request->input('per_grupo'),true); 
        $input_persona = json_decode($request->input('persona'),true); 
        $input_per_natural = json_decode($request->input('per_natural'),true);
        $input_per_juridica = json_decode($request->input('per_juridica'),true);
        $input_per_imagen = json_decode($request->input('per_imagen'),true);
        $input_per_parametro = json_decode($request->input('per_parametro'),true);
        $direcciones = json_decode($request->input('direcciones'),true);
        $correos = json_decode($request->input('correos'),true);
        $telefonos = json_decode($request->input('telefonos'),true);
        $webs = json_decode($request->input('webs'),true);
        $documentos_identidad = json_decode($request->input('documentos_identidad'),true);

        try 
        {

	        $persona = ($input_persona['per_tipo'] == 1) ? 
                            Persona::where(['per_nombre' => $input_per_natural['nombres'] ])->first() : 
                            Persona::where(['per_nombre' => $input_per_juridica['razon_social'] ])->first() ;

	        if (!$persona)
	        {
                \DB::beginTransaction();

	        	$persona = new Persona() ;
                $persona->per_nombre = ( $input_persona['per_tipo'] == 1 ) ? $input_per_natural['nombres'] : $input_per_juridica['razon_social']  ;
                $persona->per_apellidos = ( $input_persona['per_tipo'] == 1 ) ? $input_per_natural['apellidos'] : $input_per_juridica['razon_social'] ;
                $persona->per_fecha_nac = $input_persona['per_fecha_nac'] ;
                $persona->per_tipo = $input_persona['per_tipo'] ;
                $persona->per_id_padre = $per_id_padre ;
	        	$persona->save() ;

                $persona_id =  $persona->id ;   
                
                if( $input_persona['per_tipo'] == 1 )
                {
                    $per_natural = new PerNatural() ;
                    $per_natural->persona_id = $persona_id ;
                    $per_natural->dni = $input_per_natural['dni']  ;
                    $per_natural->ruc = $input_per_natural['ruc'] ;
                    $per_natural->apellidos = $input_per_natural['apellidos'] ;
                    $per_natural->nombres = $input_per_natural['nombres'] ;
                    $per_natural->sexo = $input_per_natural['sexo'] ;
                    $per_natural->estado_civil = $input_per_natural['estado_civil'] ;
                    $per_natural->save() ;

                    if($per_natural['dni']){
                        $per_doc_identidad = new PerDocIdentidad() ;
                        $per_doc_identidad->persona_id = $persona_id ;
                        $per_doc_identidad->tipo_per_doc_identidad_id = 1 ;
                        $per_doc_identidad->numero = $per_natural['dni'] ;
                        $per_doc_identidad->fecha_caducidad = null ;
                        $per_doc_identidad->fecha_emision = null ;
                        $per_doc_identidad->save() ;      
                    }
                }
                else
                {
                    $per_juridica = new PerJuridica() ;
                    $per_juridica->persona_id = $persona_id ;
                    $per_juridica->rubro_id = $input_per_juridica['rubro_id'] ;
                    $per_juridica->ruc = $input_per_juridica['ruc'] ;
                    $per_juridica->razon_social = $input_per_juridica['razon_social'] ;
                    $per_juridica->nombre_comercial = $input_per_juridica['nombre_comercial'] ;
                    $per_juridica->glosa = $input_per_juridica['glosa'] ;
                    $per_juridica->save() ;
                }
                
                $per_imagen = new PerImagen() ;
                $per_imagen->persona_id = $persona_id ;
                $per_imagen->url = $input_per_imagen['url'] ;
                $per_imagen->tipo = $input_per_imagen['tipo'] ;
	        	$per_imagen->save() ;
                
                if($input_per_parametro!=null){
                    $per_parametro = new PerParametro() ;
                    $per_parametro->persona_id = $persona_id ;
                    $per_parametro->par_codigo = $input_per_parametro['par_codigo'] ;
                    $per_parametro->par_clase = $input_per_parametro['par_clase'] ;
                    $per_parametro->glosa = $input_per_parametro['glosa'] ;
                    $per_parametro->save() ;
                }

                $per_grupo = new PerGrupo() ;
                $per_grupo->persona_id = $persona_id ;
                $per_grupo->per_id_padre = $per_id_padre ;
                $per_grupo->par_codigo = $input_per_grupo['per_codigo'] ;
                $per_grupo->par_clase = $input_per_grupo['par_clase'] ;
                $per_grupo->codigo = $persona_id + 1;
                $per_grupo->glosa = $input_per_juridica['glosa'] ;
	        	$per_grupo->save() ;

                $per_doc_identidad = new PerDocIdentidad() ;
                $per_doc_identidad->persona_id = $persona_id ;
                $per_doc_identidad->tipo_per_doc_identidad_id = 2 ;
                $per_doc_identidad->numero = ( $input_persona['per_tipo'] == 1 ) ? $per_natural['ruc'] : $per_juridica['ruc'] ;
                $per_doc_identidad->fecha_caducidad = null ;
                $per_doc_identidad->fecha_emision = null ;
                $per_doc_identidad->save() ;                

                $direcciones = $direcciones["direcciones"];
                for($i=0; $i< count($direcciones); $i++ ){
                    $per_direccion = new PerDireccion() ;
                    $per_direccion->persona_id = $persona_id ;
                    $per_direccion->tipo_direccion_id = $direcciones[$i]['tipo_direccion_id'] ;
                    $per_direccion->ubigeo_id = $direcciones[$i]['ubigeo']['id'] ;
                    $per_direccion->direccion = $direcciones[$i]['direccion'] ;
                    $per_direccion->referencia = $direcciones[$i]['referencia'] ;
                    $per_direccion->save() ; 
                }

                $correos = $correos["correos"];
                for($i=0; $i< count($correos); $i++ ){ 
                    $per_email = new PerMail() ;
                    $per_email->persona_id = $persona_id ;
                    $per_email->mail = $correos[$i]['mail'] ;
                    $per_email->save() ; 
                }

                $telefonos = $telefonos["telefonos"];
                for($i=0; $i< count($telefonos); $i++ ){
                    $per_telefono = new PerTelefono() ;
                    $per_telefono->persona_id = $persona_id ;
                    $per_telefono->tipo_telefono_id = $telefonos[$i]['tipo_telefono_id'] ;
                    $per_telefono->telefono = $telefonos[$i]['telefono'] ;
                    $per_telefono->item = 1 ;
                    $per_telefono->save() ;
                }

                $webs = $webs["webs"];
                for($i=0; $i< count($webs); $i++ ){
                    $per_web = new PerWeb() ;
                    $per_web->persona_id = $persona_id ;
                    $per_web->tipo_web_id = $webs[$i]['tipo_web_id'] ;
                    $per_web->url = $webs[$i]['url'] ;
                    $per_web->save() ;
                }

                $documentos_identidad = $documentos_identidad["documentos_identidad"];
                for($i=0; $i< count($documentos_identidad); $i++ ){
                    $per_doc_identidad = new PerDocIdentidad() ;
                    $per_doc_identidad->persona_id = $persona_id ;
                    $per_doc_identidad->tipo_per_doc_identidad_id = $documentos_identidad[$i]['tipo_per_doc_identidad_id'] ;
                    $per_doc_identidad->numero = $documentos_identidad[$i]['numero'] ;
                    $per_doc_identidad->fecha_caducidad = (isset($documentos_identidad[$i]['fecha_caducidad']['formatted'])) ? $documentos_identidad[$i]['fecha_caducidad']['formatted'] : null ;
                    $per_doc_identidad->fecha_emision = (isset($documentos_identidad[$i]['fecha_emision']['formatted'])) ? $documentos_identidad[$i]['fecha_emision']['formatted'] : null ;
                    $per_doc_identidad->save() ;
                }

                \DB::commit();
	        }
	        else
	        {
                return \Response::json([
							'message' => 'DNI, se encuentra registrada',
							'error'   => true,
							'errors'  => [] ,
							'data'    => [],
		                ]);
	        }

	        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => "OK",
	                    ]);

        } 
        catch (Exception $e)
        {
            \DB::rollback();

            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }
    }

	public function getProveedorById($id)
    {

        $proveedor_id = $id ;

        $proveedor = PerGrupo::where('id',$proveedor_id)->first() ;
        $persona = Persona::where([['id',$proveedor->persona_id],['estado',1] ])->first() ;
        $per_natural = null;
        $per_juridica = null;
        if($persona->per_tipo == 1) $per_natural = PerNatural::where([['persona_id',$proveedor->persona_id],['estado',1] ])->first() ;
        else $per_juridica = PerJuridica::where([['persona_id',$proveedor->persona_id],['estado',1] ])->first() ;
        $per_imagen = PerImagen::where([['persona_id',$proveedor->persona_id],['estado',1] ])->first() ;
        $per_doc_identidad = PerDocIdentidad::where([['persona_id',$proveedor->persona_id],['estado',1] ])->get() ;
        $per_mail = PerMail::where([['persona_id',$proveedor->persona_id],['estado',1] ])->get() ;
        $per_direccion = PerDireccion::where([['persona_id',$proveedor->persona_id],['estado',1] ])->get() ;
        $per_telefono = PerTelefono::where([['persona_id',$proveedor->persona_id],['estado',1] ])->get() ;
        $per_web = PerWeb::where([['persona_id',$proveedor->persona_id],['estado',1] ])->get() ;

        $data = ['proveedor' => $proveedor, 'persona' => $persona, 'per_natural' => $per_natural, 'per_juridica' => $per_juridica, 
                    'per_imagen' => $per_imagen, 'per_doc_identidad' => $per_doc_identidad, 'per_mail' => $per_mail, 
                    'per_direccion' => $per_direccion,'per_telefono' => $per_telefono, 'per_web' => $per_web  ];

        return response()->json([
                	'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

    public function getClienteById($id)
    {

        $cliente_id = $id ;

        $cliente = PerGrupo::where('id',$cliente_id)->first() ;
        $persona = Persona::where([['id',$cliente->persona_id],['estado',1] ])->first() ;
        $per_natural = null;
        $per_juridica = null;
        if($persona->per_tipo == 1) $per_natural = PerNatural::where([['persona_id',$cliente->persona_id],['estado',1] ])->first() ;
        else $per_juridica = PerJuridica::where([['persona_id',$cliente->persona_id],['estado',1] ])->first() ;
        $per_imagen = PerImagen::where([['persona_id',$cliente->persona_id],['estado',1] ])->first() ;
        $tipo_cliente = PerParametro::where([['persona_id',$cliente->persona_id],['estado',1],['par_clase',4001] ])->first() ;
        $per_doc_identidad = PerDocIdentidad::where([['persona_id',$cliente->persona_id],['estado',1] ])->get() ;
        $per_mail = PerMail::where([['persona_id',$cliente->persona_id],['estado',1] ])->get() ;
        $per_direccion = PerDireccion::where([['persona_id',$cliente->persona_id],['estado',1] ])->get() ;
        $per_telefono = PerTelefono::where([['persona_id',$cliente->persona_id],['estado',1] ])->get() ;
        $per_web = PerWeb::where([['persona_id',$cliente->persona_id],['estado',1] ])->get() ;

        $data = ['cliente' => $cliente, 'persona' => $persona, 'per_natural' => $per_natural, 'per_juridica' => $per_juridica, 
                    'per_imagen' => $per_imagen, 'tipo_cliente' => $tipo_cliente, 'per_doc_identidad' => $per_doc_identidad, 'per_mail' => $per_mail, 
                    'per_direccion' => $per_direccion,'per_telefono' => $per_telefono, 'per_web' => $per_web  ];

        return response()->json([
                	'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function updateInformacion(Request $request)
    {
        $input_persona = json_decode($request->input('persona'),true);
        $input_per_natural = json_decode($request->input('per_natural'),true);
        $input_per_juridica = json_decode($request->input('per_juridica'),true);
        $input_per_imagen = json_decode($request->input('per_imagen'),true);
        $input_per_grupo = json_decode($request->input('per_grupo'),true); 
        $input_per_parametro = json_decode($request->input('per_parametro'),true);
        try 
        {
            \DB::beginTransaction();

            $persona =  Persona::find($input_persona['id']) ;
            $persona->per_nombre = ( $input_persona['per_tipo'] == 1 ) ? $input_per_natural['nombres'] : $input_per_juridica['razon_social']  ;
            $persona->per_apellidos = ( $input_persona['per_tipo'] == 1 ) ? $input_per_natural['apellidos'] : $input_per_juridica['razon_social'] ;
            $persona->per_fecha_nac = $input_persona['per_fecha_nac'] ;
            $persona->save() ;

            if($persona->per_tipo == 1)
            {
                $per_natural =  PerNatural::find($input_per_natural['id']) ;
                $per_natural->dni = $input_per_natural['dni'] ;
                $per_natural->ruc = $input_per_natural['ruc'] ;
                $per_natural->apellidos = $input_per_natural['apellidos'] ;
                $per_natural->nombres = $input_per_natural['nombres'] ;
                $per_natural->sexo = $input_per_natural['sexo'] ;
                $per_natural->estado_civil = $input_per_natural['estado_civil'] ;
                $per_natural->save() ;
            }
            else
            {
                $per_juridica =  PerJuridica::find($input_per_juridica['id']) ;
                $per_juridica->rubro_id = $input_per_juridica['rubro_id'] ;
                $per_juridica->ruc = $input_per_juridica['ruc'] ;
                $per_juridica->razon_social = $input_per_juridica['razon_social'] ;
                $per_juridica->nombre_comercial = $input_per_juridica['nombre_comercial'] ;
                $per_juridica->glosa = $input_per_juridica['glosa'] ;
                $per_juridica->save() ;
            }
            
            $per_imagen =  PerImagen::find($input_per_imagen['id']) ;
            $per_imagen->url = $input_per_imagen['url'] ;
            $per_imagen->save() ;

            if($input_per_parametro!=null){
                $per_parametro =  PerParametro::find($input_per_parametro['id']) ;
                $per_parametro->par_codigo = $input_per_parametro['par_codigo'] ;
                $per_parametro->save() ;
            }    

            $input_per_grupo =  PerGrupo::find($input_per_grupo['id']) ;
            $input_per_grupo->glosa = $input_per_juridica['glosa'] ;
            $input_per_grupo->save() ;

            \DB::commit();
        }  
        catch (Exception $e)
        {
            \DB::rollback();
            
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }  

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => 'OK',
        ]);
    }

    public function updateDirecciones(Request $request)
    {
        $direcciones = json_decode($request->input('direcciones'),true);
        $persona = json_decode($request->input('persona'),true);
        try 
        {
            \DB::beginTransaction();

            PerDireccion::where([['estado', '=', 1],['persona_id', '=', $persona['id']]])->update(['estado' => 0]);

            $direcciones = $direcciones["direcciones"];
            for($i=0; $i< count($direcciones); $i++ )
            {
                if($direcciones[$i]['id']!=null)
                {
                    $per_direccion = PerDireccion::find($direcciones[$i]['id']) ;
                    $per_direccion->tipo_direccion_id = $direcciones[$i]['tipo_direccion_id'] ;
                    $per_direccion->ubigeo_id = (isset($direcciones[$i]['ubigeo']['id'])) ? $direcciones[$i]['ubigeo']['id'] : $per_direccion->ubigeo_id;
                    $per_direccion->direccion = $direcciones[$i]['direccion'] ;
                    $per_direccion->referencia = $direcciones[$i]['referencia'] ;
                    $per_direccion->estado = 1;
                    $per_direccion->save() ;
                }
                else
                {
                    $per_direccion = new PerDireccion() ;
                    $per_direccion->persona_id = $persona['id'] ;
                    $per_direccion->tipo_direccion_id = $direcciones[$i]['tipo_direccion_id'] ;
                    $per_direccion->ubigeo_id = $direcciones[$i]['ubigeo']['id'] ;
                    $per_direccion->direccion = $direcciones[$i]['direccion'] ;
                    $per_direccion->referencia = $direcciones[$i]['referencia'] ;
                    $per_direccion->save() ; 
                } 
            }

            \DB::commit();
        }  
        catch (Exception $e)
        {
            \DB::rollback();
            
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }  

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => 'OK',
        ]);
    }

    public function updateCorreos(Request $request)
    {
        $correos = json_decode($request->input('correos'),true);
        $persona = json_decode($request->input('persona'),true);

        try 
        {
            \DB::beginTransaction();

            PerMail::where([['estado', '=', 1],['persona_id', '=', $persona['id']]])->update(['estado' => 0]);

            $correos = $correos["correos"];
            for($i=0; $i< count($correos); $i++ )
            {
                if($correos[$i]['id']!=null)
                {
                    $per_mail = PerMail::find($correos[$i]['id']) ;
                    $per_mail->mail = $correos[$i]['mail'] ;
                    $per_mail->estado = 1;
                    $per_mail->save() ; 
                }
                else
                {
                    $per_email = new PerMail() ;
                    $per_email->persona_id = $persona['id'] ;
                    $per_email->mail = $correos[$i]['mail'] ;
                    $per_email->save() ;
                }        
            }

            \DB::commit();
        }  
        catch (Exception $e)
        {
            \DB::rollback();
            
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }  

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => 'OK',
        ]);
    }

    public function updateTelefonos(Request $request)
    {
        $telefonos = json_decode($request->input('telefonos'),true);
        $persona = json_decode($request->input('persona'),true);

        try 
        {
            \DB::beginTransaction();

            PerTelefono::where([['estado', '=', 1],['persona_id', '=', $persona['id']]])->update(['estado' => 0]);

            $telefonos = $telefonos["telefonos"];
            for($i=0; $i< count($telefonos); $i++ )
            {
                if($telefonos[$i]['id']!=null)
                {
                    $per_telefono = PerTelefono::find($telefonos[$i]['id']) ;
                    $per_telefono->tipo_telefono_id = $telefonos[$i]['tipo_telefono_id'] ;
                    $per_telefono->telefono = $telefonos[$i]['telefono'] ;
                    $per_telefono->estado = 1;
                    $per_telefono->save() ; 
                }   
                else
                {
                    $per_telefono = new PerTelefono() ;
                    $per_telefono->persona_id = $persona['id'] ;
                    $per_telefono->tipo_telefono_id = $telefonos[$i]['tipo_telefono_id'] ;
                    $per_telefono->telefono = $telefonos[$i]['telefono'] ;
                    $per_telefono->item = 1 ;
                    $per_telefono->save() ;
                } 
            }

            \DB::commit();
        }  
        catch (Exception $e)
        {
            \DB::rollback();
            
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }  

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => 'OK',
        ]);
    }

    public function updateWebs(Request $request)
    {
        $webs = json_decode($request->input('webs'),true);
        $persona = json_decode($request->input('persona'),true);

        try 
        {
            \DB::beginTransaction();

            PerWeb::where([['estado', '=', 1],['persona_id', '=', $persona['id']]])->update(['estado' => 0]);

            $webs = $webs["webs"];
            for($i=0; $i< count($webs); $i++ )
            {
                if($webs[$i]['id']!=null)
                {
                    $per_web = PerWeb::find($webs[$i]['id']) ;
                    $per_web->tipo_web_id = $webs[$i]['tipo_web_id'] ;
                    $per_web->url = $webs[$i]['url'] ;
                    $per_web->estado = 1 ;
                    $per_web->save() ; 
                }
                else
                {
                    $per_web = new PerWeb() ;
                    $per_web->persona_id = $persona['id'] ;
                    $per_web->tipo_web_id = $webs[$i]['tipo_web_id'] ;
                    $per_web->url = $webs[$i]['url'] ;
                    $per_web->save() ;
                }
            }

            \DB::commit();
        }  
        catch (Exception $e)
        {
            \DB::rollback();
            
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }  

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => 'OK',
        ]);
    }

    public function updateDocumentosIdentidad(Request $request)
    {
        $documentos_identidad = json_decode($request->input('documentos_identidad'),true);
        $persona = json_decode($request->input('persona'),true);

        try 
        {
            \DB::beginTransaction();

            PerDocIdentidad::where([['estado', '=', 1],['persona_id', '=', $persona['id']]])->update(['estado' => 0]);

            $documentos_identidad = $documentos_identidad["documentos_identidad"];
            for($i=0; $i< count($documentos_identidad); $i++ )
            {
                if($documentos_identidad[$i]['id']!=null)
                {
                    $per_doc_identidad = PerDocIdentidad::find($documentos_identidad[$i]['id']) ;
                    $per_doc_identidad->tipo_per_doc_identidad_id = $documentos_identidad[$i]['tipo_per_doc_identidad_id'] ; 
                    $per_doc_identidad->numero = $documentos_identidad[$i]['numero'] ; 
                    $per_doc_identidad->fecha_caducidad = (isset($documentos_identidad[$i]['fecha_caducidad']['formatted'])) ? $documentos_identidad[$i]['fecha_caducidad']['formatted'] : $documentos_identidad[$i]['fecha_caducidad']['date']['year']."-".$documentos_identidad[$i]['fecha_caducidad']['date']['month']."-".$documentos_identidad[$i]['fecha_caducidad']['date']['day'] ;
                    $per_doc_identidad->fecha_emision = (isset($documentos_identidad[$i]['fecha_emision']['formatted'])) ? $documentos_identidad[$i]['fecha_emision']['formatted'] : $documentos_identidad[$i]['fecha_emision']['date']['year']."-".$documentos_identidad[$i]['fecha_emision']['date']['month']."-".$documentos_identidad[$i]['fecha_emision']['date']['day'] ;
                    $per_doc_identidad->estado = 1 ;
                    $per_doc_identidad->save() ; 
                }
                else
                {
                    $per_doc_identidad = new PerDocIdentidad() ;
                    $per_doc_identidad->persona_id = $persona['id'] ;
                    $per_doc_identidad->tipo_per_doc_identidad_id = $documentos_identidad[$i]['tipo_per_doc_identidad_id'] ;
                    $per_doc_identidad->numero = $documentos_identidad[$i]['numero'] ;
                    $per_doc_identidad->fecha_caducidad = (isset($documentos_identidad[$i]['fecha_caducidad']['formatted'])) ? $documentos_identidad[$i]['fecha_caducidad']['formatted'] : null ;
                    $per_doc_identidad->fecha_emision = (isset($documentos_identidad[$i]['fecha_emision']['formatted'])) ? $documentos_identidad[$i]['fecha_emision']['formatted'] : null ;
                    $per_doc_identidad->save() ;
                }
            }

            \DB::commit();
        }  
        catch (Exception $e)
        {
            \DB::rollback();
            
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }  

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => 'OK',
        ]);
    }

	public function updateEstado(Request $request)
    {

		$per_grupo_id = $request->input('id') ;
        $persona_id = $request->input('persona_id') ;
		$estado = $request->input('estado') ;

        $per_grupo =  PerGrupo::find($per_grupo_id) ;
        $per_grupo->estado = $estado ;
        $per_grupo->save() ;

        $persona =  Persona::find($persona_id) ; 

        PerImagen::where([['estado', '=', 1],['persona_id', '=', $persona_id]])->update(['estado' => 0]);
        PerWeb::where([['estado', '=', 1],['persona_id', '=', $persona_id]])->update(['estado' => 0]);
        PerTelefono::where([['estado', '=', 1],['persona_id', '=', $persona_id]])->update(['estado' => 0]);
        PerDireccion::where([['estado', '=', 1],['persona_id', '=', $persona_id]])->update(['estado' => 0]);
        PerMail::where([['estado', '=', 1],['persona_id', '=', $persona_id]])->update(['estado' => 0]);
        PerDocIdentidad::where([['estado', '=', 1],['persona_id', '=', $persona_id]])->update(['estado' => 0]);
        Persona::where([['estado', '=', 1],['id', '=', $persona_id]])->update(['estado' => 0]);

        if($persona->per_tipo == 1) PerNatural::where([['estado', '=', 1],['persona_id', '=', $persona_id]])->update(['estado' => 0]);
        else PerJuridica::where([['estado', '=', 1],['persona_id', '=', $persona_id]])->update(['estado' => 0]);
        
        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $estado,
                ]);
    }
}
