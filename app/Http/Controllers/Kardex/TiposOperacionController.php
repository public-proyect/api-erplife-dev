<?php

namespace App\Http\Controllers\Kardex;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Models\Kardex\TipoOperacion;

class TiposOperacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct(){
         $this->middleware(['jwt.auth']); 
     }

    public function getTiposOperacion(){
        return response()->json([
            "data"=>TipoOperacion::orderBy('id','desc')->where('estado',1)->get()
            ]);
    }

    public function postTipoOperacion(Request $request){
         try{
            $validator=Validator::make($request->all(),[
                'descripcion'=>'required|max:255',
                'tipo'=>'required|max:1',
                'simbolo'=>'required|max:1'
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
                $tipoOperacion =new TipoOperacion();
                $tipoOperacion->descripcion = $request->input('descripcion') ; 
                $tipoOperacion->tipo = $request->input('tipo');
                $tipoOperacion->simbolo = $request->input('simbolo');      
                $tipoOperacion->save();
                return response()->json([
                    "res"=>"El tipo operacion con id {$tipoOperacion->id} ha sido guardado"
                ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
        
    } 

    public function getTipoOperacionById($id){
        $tipoOperacion= TipoOperacion::find($id);
        return response()->json([
            "data"=>$tipoOperacion
        ]);
    }

    public function updateTipoOperacion(Request $request){
         try{
            $validator=Validator::make($request->all(),[
                'id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'descripcion'=>'required|max:255',
                'tipo'=>'required|max:1',
                'simbolo'=>'required|max:1'
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
                $tipooperacion_id=$request->input('id');
                $tipoOperacion= TipoOperacion::find($tipooperacion_id);
                $tipoOperacion->descripcion = $request->input('descripcion') ; 
                $tipoOperacion->tipo = $request->input('tipo');
                $tipoOperacion->simbolo = $request->input('simbolo');    
                $tipoOperacion->save();
                return response()->json([
                    "res"=>"El tipo operacion con id {$tipoOperacion->id} ha sido editado"
                ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
         
    }

    public function deleteTipoOperacion(Request $request){
         $tipooperacion_id=$request->input('id');
         $tipoOperacion= TipoOperacion::find($tipooperacion_id);
         $tipoOperacion->estado=$request->input('estado');
         $tipoOperacion->save();
           return response()->json([
            "res"=>"El tipo operacion con id {$tipoOperacion->id} ha sido eliminado"
        ]);
    }
}
