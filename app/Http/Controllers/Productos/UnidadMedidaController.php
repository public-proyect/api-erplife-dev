<?php

namespace App\Http\Controllers\Productos;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Productos\UnidadMedida;
use JWTAuth;


class UnidadMedidaController extends Controller
{
    public function __construct(JWTAuth $auth)
    {
        $this->middleware('jwt.auth');
    }

    public function getUnidadesMedida()
	{
        $data = UnidadMedida::select('unidad_medida.*','unidad_medida_padre.descripcion AS unidad_medida_padre')
                ->leftjoin('unidad_medida AS unidad_medida_padre', 'unidad_medida_padre.id', '=', 'unidad_medida.unidad_medida_id')
                ->where('unidad_medida.estado',1)->get() ;

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function save(Request $request)
    {

        $per_id_padre = 1;

        $unidad_medida_id = $request->input('unidad_medida_id') ;
        $abreviatura = $request->input('abreviatura') ;
        $descripcion = $request->input('descripcion') ;
        $equivalente = $request->input('equivalente') ;
        $glosa = $request->input('glosa') ;

    	$rules = [
            	'descripcion'      => 'descripcion',
            ];

        try 
        {

	        $unidad_medida = UnidadMedida::where(['descripcion' => $descripcion])->first();

	        if (!$unidad_medida)
	        {
	        	$unidad_medida = new UnidadMedida() ;
                $unidad_medida->per_id_padre = $per_id_padre ;
                $unidad_medida->unidad_medida_id = ($unidad_medida_id != 0) ? $unidad_medida_id : null ;
                $unidad_medida->abreviatura = $abreviatura ;
                $unidad_medida->descripcion = $descripcion ;
                $unidad_medida->equivalente = $equivalente ;
                $unidad_medida->glosa = $glosa ;
	        	$unidad_medida->save() ;
	        }
	        else
	        {
                $unidad_medida->per_id_padre = $per_id_padre ;
                $unidad_medida->unidad_medida_id = $unidad_medida_id ;
                $unidad_medida->abreviatura = $abreviatura ;
                $unidad_medida->descripcion = $descripcion ;
                $unidad_medida->equivalente = $equivalente ;
                $unidad_medida->glosa = $glosa ;
	        	$unidad_medida->estado = 1 ;
	        	$unidad_medida->save() ;
	        }

	        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => "OK",
	                    ]);

        } 
        catch (Exception $e)
        {
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }
    }

	public function getUnidadMedidaById($id)
    {

        $unidad_medida_id = $id ;

        $data = UnidadMedida::where('id',$unidad_medida_id)->first() ;

        return response()->json([
                	'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function update(Request $request)
    {

        $id = $request->input('id') ;
        $per_id_padre = $request->input('per_id_padre') ;
        $unidad_medida_id = $request->input('unidad_medida_id') ;
        $abreviatura = $request->input('abreviatura') ;
        $descripcion = $request->input('descripcion') ;
        $equivalente = $request->input('equivalente') ;
        $glosa = $request->input('glosa') ;

        $unidad_medida =  UnidadMedida::find($id) ;
	    $unidad_medida->per_id_padre = $per_id_padre ;
        $unidad_medida->unidad_medida_id = ($unidad_medida_id != 0) ? $unidad_medida_id : null ;
        $unidad_medida->abreviatura = $abreviatura ;
        $unidad_medida->descripcion = $descripcion ;
        $unidad_medida->equivalente = $equivalente ;
        $unidad_medida->glosa = $glosa ;
        $unidad_medida->save() ;

        $data =  UnidadMedida::find($id) ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function updateEstado(Request $request)
    {

		$unidad_medida_id = $request->input('id') ;
		$estado = $request->input('estado') ;

        $unidad_medida =  UnidadMedida::find($unidad_medida_id) ;
        $unidad_medida->estado = $estado ;
        $unidad_medida->save() ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $estado,
                ]);
    }
}
