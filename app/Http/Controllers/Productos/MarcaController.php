<?php

namespace App\Http\Controllers\Productos;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Productos\Marca;
use JWTAuth;


class MarcaController extends Controller
{
    public function __construct(JWTAuth $auth)
    {
        $this->middleware('jwt.auth', ['except' => ['uploadImagen']]);
    }

    public function getMarcas()
	{
        $data = Marca::where('estado',1)->get() ;

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function uploadImagen(Request $request){

        $file=$request->file('imagen-marca');
        foreach($file as $image){
            $name='marca_'.time().rand().'.'.$image->getClientOriginalExtension();
            $image->storeAs('marcas',"{$name}",'asset');       
        }
        return \Response::Json(['status' => 'success',
                            'error'   => false,
                            'filename'    => $name],200);  
    } 

    public function save(Request $request)
    {

        $per_id_padre = 1;

        $descripcion = $request->input('descripcion') ;
        $imagen = $request->input('imagen') ;

    	$rules = [
            	'descripcion'      => 'descripcion',
            ];

        try 
        {

	        $marca = Marca::where(['descripcion' => $descripcion])->first();

	        if (!$marca)
	        {
	        	$marca = new Marca() ;
                $marca->descripcion = $descripcion ;
                $marca->imagen = $imagen ;
                $marca->per_id_padre = $per_id_padre ;
	        	$marca->save() ;
	        }
	        else
	        {
                $marca->descripcion = $descripcion ;
                $marca->imagen = $imagen ;
                $marca->per_id_padre = $per_id_padre ;
	        	$marca->estado = 1 ;
	        	$marca->save() ;
	        }

	        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => "OK",
	                    ]);

        } 
        catch (Exception $e)
        {
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }
    }

	public function getMarcaById($id)
    {

        $marca_id = $id ;

        $data = Marca::where('id',$marca_id)->first() ;

        return response()->json([
                	'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function update(Request $request)
    {

        $marca_id = $request->input('id') ;
        $descripcion = $request->input('descripcion') ;
        $imagen = $request->input('imagen') ;

        $marca =  Marca::find($marca_id) ;
		$marca->descripcion = $descripcion ;
		$marca->imagen = $imagen ;
        $marca->save() ;

        $data =  Marca::find($marca_id) ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function updateEstado(Request $request)
    {

		$marca_id = $request->input('id') ;
		$estado = $request->input('estado') ;

        $marca =  Marca::find($marca_id) ;
        $marca->estado = $estado ;
        $marca->save() ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $estado,
                ]);
    }
}
