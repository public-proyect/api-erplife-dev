<?php

namespace App\Http\Controllers\Productos;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Productos\Producto;
use JWTAuth;

class ProductoController extends Controller
{
    public function __construct(JWTAuth $auth)
    {
        $this->middleware('jwt.auth', ['except' => ['uploadImagen']]);
    }

    public function getProductos()
	{
        $data = Producto::select('producto.*','sub_familia.descripcion AS sub_familia','unidad_medida.descripcion AS unidad_medida','modelo.descripcion AS modelo')
                ->join('sub_familia', 'sub_familia.id', '=', 'producto.sub_familia_id')
                ->join('unidad_medida', 'unidad_medida.id', '=', 'producto.unidad_medida_id')
                ->join('modelo', 'modelo.id', '=', 'producto.modelo_id')
                ->where('producto.estado',1)->get() ;

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function uploadImagen(Request $request){

        $file=$request->file('imagen-producto');
        foreach($file as $image){
            $name='producto_'.time().rand().'.'.$image->getClientOriginalExtension();
            $image->storeAs('productos',"{$name}",'asset');       
        }
        return \Response::Json(['status' => 'success',
                            'error'   => false,
                            'filename'    => $name],200);  
    } 

    public function save(Request $request)
    {

        $per_id_padre = 1;

        $sub_familia_id = $request->input('sub_familia_id');
        $unidad_medida_id = $request->input('unidad_medida_id');
        $modelo_id = $request->input('modelo_id');
        $codigo = '000P1';
        $codigo_barra = $request->input('codigo_barra');
        $stock_min = $request->input('stock_min');
        $descripcion = $request->input('descripcion');
        $imagen = $request->input('imagen');
        $percepcion = $request->input('percepcion');
        $glosa = $request->input('glosa');

        try 
        {

	        $producto = Producto::where(['descripcion' => $descripcion])->first();

	        if (!$producto)
	        {
	        	$producto = new Producto() ;
                $producto->sub_familia_id = $sub_familia_id ;
                $producto->unidad_medida_id = $unidad_medida_id ;
                $producto->modelo_id = $modelo_id ;
                $producto->codigo = $codigo ;
                $producto->codigo_barra = $codigo_barra ;
                $producto->stock_min = $stock_min ;
                $producto->descripcion = $descripcion ;
                $producto->imagen = $imagen ;
                $producto->percepcion = $percepcion ;
                $producto->glosa = $glosa ;
                $producto->per_id_padre = $per_id_padre ;
	        	$producto->save() ;
	        }
	        else
	        {
                $producto->sub_familia_id = $sub_familia_id ;
                $producto->unidad_medida_id = $unidad_medida_id ;
                $producto->modelo_id = $modelo_id ;
                $producto->codigo = $codigo ;
                $producto->codigo_barra = $codigo_barra ;
                $producto->stock_min = $stock_min ;
                $producto->descripcion = $descripcion ;
                $producto->imagen = $imagen ;
                $producto->percepcion = $percepcion ;
                $producto->glosa = $glosa ;
                $producto->per_id_padre = $per_id_padre ;
	        	$producto->estado = 1 ;
	        	$producto->save() ;
	        }

	        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => "OK",
	                    ]);

        } 
        catch (Exception $e)
        {
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }
    }

	public function getProductoById($id)
    {

        $producto_id = $id ;

        $data = Producto::where('producto.id',$producto_id)
                            ->join('modelo', 'modelo.id', '=', 'producto.modelo_id')
                            ->join('sub_familia', 'sub_familia.id', '=', 'producto.sub_familia_id')
                            ->join('familia', 'familia.id', '=', 'sub_familia.familia_id')
                            ->select('producto.*', 'modelo.marca_id AS marca_id', 'familia.clase_id AS clase_id')
                            ->first() ;

        return response()->json([
                	'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function update(Request $request)
    {

        $producto_id = $request->input('id') ;
        $sub_familia_id = $request->input('sub_familia_id');
        $unidad_medida_id = $request->input('unidad_medida_id');
        $modelo_id = $request->input('modelo_id');
        $codigo = $request->input('codigo');
        $codigo_barra = $request->input('codigo_barra');
        $stock_min = $request->input('stock_min');
        $descripcion = $request->input('descripcion');
        $imagen = $request->input('imagen');
        $percepcion = $request->input('percepcion');
        $glosa = $request->input('glosa');
        
        $producto =  Producto::find($producto_id) ;
        $producto->sub_familia_id = $sub_familia_id ;
        $producto->unidad_medida_id = $unidad_medida_id ;
        $producto->modelo_id = $modelo_id ;
        $producto->codigo = $codigo ;
        $producto->codigo_barra = $codigo_barra ;
        $producto->stock_min = $stock_min ;
        $producto->descripcion = $descripcion ;
        $producto->imagen = $imagen ;
        $producto->percepcion = $percepcion ;
        $producto->glosa = $glosa ;
        $producto->save() ;

        $data =  Producto::find($producto_id) ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function updateEstado(Request $request)
    {

		$producto_id = $request->input('id') ;
		$estado = $request->input('estado') ;

        $producto = Producto::find($producto_id) ;
        $producto->estado = $estado ;
        $producto->save() ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $estado,
                ]);
    }
}
