<?php

namespace App\Http\Controllers\Productos;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Productos\Modelo;
use JWTAuth;

class ModeloController extends Controller
{
    public function __construct(JWTAuth $auth)
    {
        $this->middleware('jwt.auth');
    }

    public function getModelos()
	{
        $data = Modelo::select('modelo.*','marca.descripcion AS marca')
                ->join('marca', 'marca.id', '=', 'modelo.marca_id')
                ->where('modelo.estado',1)->get() ;

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function getModelosByMarcaId(Request $request)
	{ 
        $id_marca = $request["id_marca"];

        $data = Modelo::select('modelo.*')
                ->where('modelo.marca_id', '=' , $id_marca)
                ->where('modelo.estado', '=' , 1)
                ->get() ;

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function save(Request $request)
    {

        $per_id_padre = 1;

        $marca_id = $request->input('marca_id');
        $descripcion = $request->input('descripcion');
        $glosa = $request->input('glosa');

        try 
        {

	        $modelo = Modelo::where(['descripcion' => $descripcion])->first();

	        if (!$modelo)
	        {
	        	$modelo = new Modelo() ;
                $modelo->marca_id = $marca_id ;
                $modelo->descripcion = $descripcion ;
                $modelo->glosa = $glosa ;
                $modelo->per_id_padre = $per_id_padre ;
	        	$modelo->save() ;
	        }
	        else
	        {
                $modelo->marca_id = $marca_id ;
                $modelo->descripcion = $descripcion ;
                $modelo->glosa = $glosa ;
                $modelo->per_id_padre = $per_id_padre ;
	        	$modelo->estado = 1 ;
	        	$modelo->save() ;
	        }

	        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => "OK",
	                    ]);

        } 
        catch (Exception $e)
        {
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }
    }

	public function getModeloById($id)
    {

        $modelo_id = $id ;

        $data = Modelo::where('id',$modelo_id)->first() ;

        return response()->json([
                	'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function update(Request $request)
    {

        $modelo_id = $request->input('id') ;
        $marca_id = $request->input('marca_id');
        $descripcion = $request->input('descripcion');
        $glosa = $request->input('glosa');
        
        $modelo =  Modelo::find($modelo_id) ;
        $modelo->marca_id = $marca_id ;
		$modelo->descripcion = $descripcion ;
		$modelo->glosa = $glosa ;
        $modelo->save() ;

        $data =  Modelo::find($modelo_id) ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function updateEstado(Request $request)
    {

		$modelo_id = $request->input('id') ;
		$estado = $request->input('estado') ;

        $modelo =  Modelo::find($modelo_id) ;
        $modelo->estado = $estado ;
        $modelo->save() ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $estado,
                ]);
    }
}
