<?php

namespace App\Http\Controllers\Productos;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Productos\Clase;
use JWTAuth;

class ClaseController extends Controller
{
    public function __construct(JWTAuth $auth)
    {
        $this->middleware('jwt.auth', ['except' => ['uploadImagen']]);
    }

    public function getClases()
	{
        $data = Clase::where('estado',1)->get() ;

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function uploadImagen(Request $request){

        $file=$request->file('imagen-clase');
        foreach($file as $image){
            $name='clase_'.time().rand().'.'.$image->getClientOriginalExtension();
            $image->storeAs('clases',"{$name}",'asset');       
        }
        return \Response::Json(['status' => 'success',
                            'error'   => false,
                            'filename'    => $name],200);  
    } 
    
    public function save(Request $request)
    {

        $per_id_padre = 1;

        $codigo = '000C1' ;
        $descripcion = $request->input('descripcion') ;
        $glosa = $request->input('glosa') ;
        $imagen = $request->input('imagen') ;

    	$rules = [
            	'descripcion'      => 'descripcion',
            ];

        try 
        {

	        $clase = Clase::where(['descripcion' => $descripcion])->first();

	        if (!$clase)
	        {
	        	$clase = new Clase() ;
                $clase->per_id_padre = $per_id_padre ;
                $clase->codigo = $codigo;
                $clase->descripcion = $descripcion ;
                $clase->glosa = $glosa ;
                $clase->imagen = $imagen;
	        	$clase->save() ;
	        }
	        else
	        {
                $clase->per_id_padre = $per_id_padre ;
                $clase->codigo = $codigo;
                $clase->descripcion = $descripcion ;
                $clase->glosa = $glosa ;
                $clase->imagen = $imagen;
	        	$clase->estado = 1 ;
	        	$clase->save() ;
	        }

	        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => "OK",
	                    ]);

        } 
        catch (Exception $e)
        {
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }
    }

	public function getClaseById($id)
    {

        $clase_id = $id ;

        $data = Clase::where('id',$clase_id)->first() ;

        return response()->json([
                	'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function update(Request $request)
    {

        $clase_id = $request->input('id') ;
        $codigo = $request->input('codigo') ;
        $descripcion = $request->input('descripcion') ;
        $glosa = $request->input('glosa') ;
        $imagen = $request->input('imagen') ;

        $clase =  Clase::find($clase_id) ;
		$clase->codigo = $codigo ;
        $clase->descripcion = $descripcion ;
        $clase->glosa = $glosa ;
        $clase->imagen = $imagen ;
        $clase->save() ;

        $data =  Clase::find($clase_id) ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function updateEstado(Request $request)
    {

		$clase_id = $request->input('id') ;
		$estado = $request->input('estado') ;

        $clase =  Clase::find($clase_id) ;
        $clase->estado = $estado ;
        $clase->save() ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $estado,
                ]);
    }
}
