<?php

namespace App\Http\Controllers\Productos;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Productos\SubFamilia;
use JWTAuth;

class SubFamiliaController extends Controller
{
    public function __construct(JWTAuth $auth)
    {
        $this->middleware('jwt.auth', ['except' => ['uploadImagen']]);
    }

    public function getSubFamilias()
	{
        $data = SubFamilia::select('sub_familia.*','familia.descripcion AS familia')
                ->join('familia', 'familia.id', '=', 'sub_familia.familia_id')
                ->where('sub_familia.estado',1)->get() ;

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function getSubFamiliasByClaseId(Request $request)
	{ 
        $id_clase = $request["id_clase"];

        $data = SubFamilia::select('sub_familia.*','familia.descripcion AS familia')
                ->join('familia', 'familia.id', '=', 'sub_familia.familia_id')
                ->where('familia.clase_id', '=' , $id_clase)
                ->where('sub_familia.estado', '=' , 1)
                ->get() ;

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function uploadImagen(Request $request){

        $file=$request->file('imagen-sub-familia');
        foreach($file as $image){
            $name='sub_familia_'.time().rand().'.'.$image->getClientOriginalExtension();
            $image->storeAs('sub-familias',"{$name}",'asset');       
        }
        return \Response::Json(['status' => 'success',
                            'error'   => false,
                            'filename'    => $name],200);  
    } 

    public function save(Request $request)
    {

        $per_id_padre = 1;

        $familia_id = $request->input('familia_id');
        $codigo = $request->input('codigo');
        $descripcion = $request->input('descripcion');
        $glosa = $request->input('glosa');
        $imagen = $request->input('imagen');

        try 
        {

	        $sub_familia = SubFamilia::where(['descripcion' => $descripcion])->first();

	        if (!$sub_familia)
	        {
	        	$sub_familia = new SubFamilia() ;
                $sub_familia->familia_id = $familia_id ;
                $sub_familia->codigo = '00SF1' ;
                $sub_familia->descripcion = $descripcion ;
                $sub_familia->glosa = $glosa ;
                $sub_familia->imagen = $imagen ;
                $sub_familia->per_id_padre = $per_id_padre ;
	        	$sub_familia->save() ;
	        }
	        else
	        {
                $sub_familia->familia_id = $familia_id ;
                $sub_familia->codigo = $codigo ;
                $sub_familia->descripcion = $descripcion ;
                $sub_familia->glosa = $glosa ;
                $sub_familia->imagen = $imagen ;
                $sub_familia->per_id_padre = $per_id_padre ;
	        	$sub_familia->estado = 1 ;
	        	$sub_familia->save() ;
	        }

	        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => "OK",
	                    ]);

        } 
        catch (Exception $e)
        {
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }
    }

	public function getSubFamiliaById($id)
    {

        $sub_familia_id = $id ;

        $data = SubFamilia::where('sub_familia.id',$sub_familia_id)
                            ->join('familia', 'familia.id', '=', 'sub_familia.familia_id')
                            ->join('clase', 'clase.id', '=', 'familia.clase_id')
                            ->select('sub_familia.*','clase.id AS clase_id')
                            ->first() ;

        return response()->json([
                	'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function update(Request $request)
    {

        $sub_familia_id = $request->input('id') ;
        $familia_id = $request->input('familia_id');
        $codigo = $request->input('codigo');
        $descripcion = $request->input('descripcion');
        $glosa = $request->input('glosa');
        $imagen = $request->input('imagen');
        
        $sub_familia =  SubFamilia::find($sub_familia_id) ;
        $sub_familia->familia_id = $familia_id ;
        $sub_familia->codigo = $codigo ;
		$sub_familia->descripcion = $descripcion ;
		$sub_familia->glosa = $glosa ;
        $sub_familia->imagen = $imagen ;
        $sub_familia->save() ;

        $data =  SubFamilia::find($sub_familia_id) ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function updateEstado(Request $request)
    {

		$sub_familia_id = $request->input('id') ;
		$estado = $request->input('estado') ;

        $sub_familia = SubFamilia::find($sub_familia_id) ;
        $sub_familia->estado = $estado ;
        $sub_familia->save() ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $estado,
                ]);
    }
}
