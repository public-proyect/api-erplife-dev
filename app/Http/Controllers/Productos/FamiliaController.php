<?php

namespace App\Http\Controllers\Productos;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Productos\Familia;
use JWTAuth;

class FamiliaController extends Controller
{
    public function __construct(JWTAuth $auth)
    {
        $this->middleware('jwt.auth', ['except' => ['uploadImagen']]);
    }

    public function getFamilias()
	{
        $data = Familia::select('familia.*','clase.descripcion AS clase')
                ->join('clase', 'clase.id', '=', 'familia.clase_id')
                ->where('familia.estado',1)->get() ;

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function getFamiliasByClaseId(Request $request)
	{ 
        $id_clase = $request["id_clase"];

        $data = Familia::select('familia.*','clase.descripcion AS clase')
                ->join('clase', 'clase.id', '=', 'familia.clase_id')
                ->where('familia.clase_id', '=' , $id_clase)
                ->where('familia.estado', '=' , 1)
                ->get() ;

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function uploadImagen(Request $request){

        $file=$request->file('imagen-familia');
        foreach($file as $image){
            $name='familia_'.time().rand().'.'.$image->getClientOriginalExtension();
            $image->storeAs('familias',"{$name}",'asset');       
        }
        return \Response::Json(['status' => 'success',
                            'error'   => false,
                            'filename'    => $name],200);  
    } 

    public function save(Request $request)
    {

        $per_id_padre = 1;

        $clase_id = $request->input('clase_id');
        $codigo = '000F1';
        $descripcion = $request->input('descripcion');
        $glosa = $request->input('glosa');
        $imagen = $request->input('imagen');

        try 
        {

	        $familia = Familia::where(['descripcion' => $descripcion])->first();

	        if (!$familia)
	        {
	        	$familia = new Familia() ;
                $familia->clase_id = $clase_id ;
                $familia->codigo = $codigo ;
                $familia->descripcion = $descripcion ;
                $familia->glosa = $glosa ;
                $familia->imagen = $imagen ;
                $familia->per_id_padre = $per_id_padre ;
	        	$familia->save() ;
	        }
	        else
	        {
                $familia->clase_id = $clase_id ;
                $familia->codigo = $codigo ;
                $familia->descripcion = $descripcion ;
                $familia->glosa = $glosa ;
                $familia->imagen = $imagen ;
                $familia->per_id_padre = $per_id_padre ;
	        	$familia->estado = 1 ;
	        	$familia->save() ;
	        }

	        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => "OK",
	                    ]);

        } 
        catch (Exception $e)
        {
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }
    }

	public function getFamiliaById($id)
    {

        $familia_id = $id ;

        $data = Familia::where('id',$familia_id)->first() ;

        return response()->json([
                	'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function update(Request $request)
    {

        $familia_id = $request->input('id') ;
        $clase_id = $request->input('clase_id');
        $codigo = $request->input('codigo');
        $descripcion = $request->input('descripcion');
        $glosa = $request->input('glosa');
        $imagen = $request->input('imagen');
        
        $familia =  Familia::find($familia_id) ;
        $familia->clase_id = $clase_id ;
        $familia->codigo = $codigo ;
		$familia->descripcion = $descripcion ;
		$familia->glosa = $glosa ;
        $familia->imagen = $imagen ;
        $familia->save() ;

        $data =  Familia::find($familia_id) ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function updateEstado(Request $request)
    {

		$familia_id = $request->input('id') ;
		$estado = $request->input('estado') ;

        $familia = Familia::find($familia_id) ;
        $familia->estado = $estado ;
        $familia->save() ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $estado,
                ]);
    }
}
