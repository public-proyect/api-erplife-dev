<?php

namespace App\Http\Controllers\Accesos;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Accesos\Control;
use JWTAuth;

class ControlController extends Controller
{
    public function __construct(JWTAuth $auth)
    {
        $this->middleware('jwt.auth');
    }

    public function getControles()
	{
        $data = Control::select('control.*','tipo_control.descripcion AS tipo_control','control_padre.descripcion AS control_padre')
                ->join('tipo_control', 'tipo_control.id', '=', 'control.tipo_control_id')
                ->leftjoin('control AS control_padre', 'control_padre.id', '=', 'control.control_padre_id')
                ->where('control.estado',1)->get() ;

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function save(Request $request)
    {
        $control_padre_id = $request->input('control_padre_id') ;
        $tipo_control_id = $request->input('tipo_control_id') ;
        $jerarquia = $request->input('jerarquia') ;
        $nombre = $request->input('nombre') ;
        $valor = $request->input('valor') ;
        $descripcion = $request->input('descripcion') ;
        $glosa = $request->input('glosa') ;

    	$rules = [
            	'nombre'      => 'nombre',
            ];

        try 
        {

	        $control = Control::where(['nombre' => $nombre])->first();

	        if (!$control)
	        {
	        	$control = new Control() ;
                $control->control_padre_id = $control_padre_id ;
                $control->tipo_control_id = $tipo_control_id ;
                $control->jerarquia = $jerarquia ;
                $control->nombre = $nombre ;
                $control->valor = $valor ;
                $control->descripcion = $descripcion ;
                $control->glosa = $glosa ;
	        	$control->save() ;
	        }
	        else
	        {
                $control->control_padre_id = $control_padre_id ;
                $control->tipo_control_id = $tipo_control_id ;
                $control->jerarquia = $jerarquia ;
                $control->nombre = $nombre ;
                $control->valor = $valor ;
                $control->descripcion = $descripcion ;
                $control->glosa = $glosa ;
	        	$control->estado = 1 ;
	        	$control->save() ;
	        }

	        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => "OK",
	                    ]);

        } 
        catch (Exception $e)
        {
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }
    }

	public function getControlById($id)
    {

        $control_id = $id ;

        $data = Control::where('id',$control_id)->first() ;

        return response()->json([
                	'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function update(Request $request)
    {
        $control_id = $request->input('id') ;
        $control_padre_id = $request->input('control_padre_id') ;
        $tipo_control_id = $request->input('tipo_control_id') ;
        $jerarquia = $request->input('jerarquia') ;
        $nombre = $request->input('nombre') ;
        $valor = $request->input('valor') ;
        $descripcion = $request->input('descripcion') ;
        $glosa = $request->input('glosa') ;

        $control =  Control::find($control_id) ;
		$control->control_padre_id = $control_padre_id ;
        $control->tipo_control_id = $tipo_control_id ;
        $control->jerarquia = $jerarquia ;
        $control->nombre = $nombre ;
        $control->valor = $valor ;
        $control->descripcion = $descripcion ;
        $control->glosa = $glosa ;
        $control->save() ;

        $data =  Control::find($control_id) ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $tipo_control_id,
                ]);
    }

	public function updateEstado(Request $request)
    {

		$control_id = $request->input('id') ;
		$estado = $request->input('estado') ;

        $control =  Control::find($control_id) ;
        $control->estado = $estado ;
        $control->save() ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $estado,
                ]);
    }
}
