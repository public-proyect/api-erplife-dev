<?php

namespace App\Http\Controllers\Accesos;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User ;
use App\Models\Accesos\Acceso ;
use App\Models\Accesos\Rol ;
use App\Models\Accesos\RolControl ;
use JWTAuth;
//use App\Traits\TreeMenu ;


class RolesController extends Controller
{
    //use TreeMenu;
    public function __construct(JWTAuth $auth)
    {
        $this->middleware('jwt.auth');
    }
	public function getRoles()
	{
             $data = Rol::where('estado',1) 
                    ->get() ;
        /*$user = \Auth::user();
        $rol_id_user = $user->rol_id ;
        $per_id_padre = $user->per_id_padre ;


        if ($rol_id_user > 2)
        {

            $data = Rol::where('estado',1)
                    ->where('per_id_padre',$per_id_padre)
                    ->get() ;
        }
        # sudo
        else if ($rol_id_user == 1)
        {
            $data = Rol::whereIn('estado', [1, 2])
                    ->get() ;        
        }*/

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
	}

    public function getRoles2($page)
    {
        $page = $page;
        $limit = 10;
        return \Response::json([
            'message'  => 'Operación Correcta',
            'errors' => '' ,
            'count'=> Rol::count(),
            'data' => Rol::skip(($page - 1)*$limit)->take($limit)->get()->toArray()
        ]);
    }

	public function save(Request $request)
    {

        /*$user = \Auth::user();
        $per_id_padre = $user->per_id_padre ;*/
        $per_id_padre = 1;

        $nombre      = $request->input('nombre') ;
        $descripcion = $request->input('descripcion') ;

       /* $per_id_padre =1 ;
        $nombre      = 'prueba' ;
        $descripcion ='1' ;*/

    	$rules = [
            	'nombre'      => 'required',
            ];

        try {
        	$validator = \Validator::make(['nombre'=> $nombre],  $rules);

	        if ($validator->fails())
	        {
	            $errors = (array)$validator->errors()->toArray();

	            return \Response::json([
					'message'  => 'Operación Fallida',
					'error' => true,
					'errors' => $errors ,
					'data'   => "",
	            ]);
	        }

	        $rol = Rol::where(['nombre' => $nombre])->first();

	        if (!$rol)
	        {
	        	$rol = new Rol() ;
                $rol->nombre       = $nombre ;
                $rol->descripcion  = $descripcion ;
                $rol->per_id_padre = $per_id_padre ;
	        	$rol->save() ;
	        }
	        else
	        {
	        	$rol->estado = 1 ;
	        	$rol->save() ;
	        }

	        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => "OK",
	                    ]);

        } catch (Exception $e)
        {
            // \Log::info('Error creating user: '.$e);
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }
    }

    public function getRoleById($id)
    {

        $rol_id = $id ;

        $data = Rol::where('id',$rol_id)
                    		->first() ;

        return response()->json([
                	'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

    public function update(Request $request)
    {

        $rol_id = $request->input('id') ;
        $nombre = $request->input('nombre') ;
        $descripcion = $request->input('descripcion') ;

        $rol =  Rol::find($rol_id) ;
		$rol->nombre = $nombre ;
		$rol->descripcion = $descripcion ;
        $rol->save() ;

        $data =  Rol::find($rol_id) ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

    public function updateEstado(Request $request)
    {

		$rol_id = $request->input('id') ;
		$estado = $request->input('estado') ;

        $rol =  Rol::find($rol_id) ;
        $rol->estado = $estado ;
        $rol->save() ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $estado,
                ]);
    }

    public function getAccesosRol(Request $request)
    {
        $rol_id = $request->input('rol_id') ;
        // $rol_id = 2;

        $sql = "SELECT
                    control.id,
                    control.control_padre_id,
                    control.jerarquia,
                    control.nombre,
                    control.valor,
                    control.descripcion,
                    control.tipo_control_id,
                    control.glosa ,
                  	(CASE WHEN (SELECT rol_control.id  FROM rol_control
								WHERE rol_control.control_id = control.id
								AND rol_control.rol_id = $rol_id
								AND rol_control.estado = 1) > 0 THEN true
								ELSE false
					 END)  AS checked
                FROM control
                WHERE  control.estado =  1
                ORDER BY control.jerarquia  ASC;" ;

        $data = \DB::select($sql) ;

       return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'    => $data,
                    	]);
    }


    /*public function checkForChildrenOtherwiseAddToArray(&$output, $array, $parent_id = null)
    {
        // loop through all sub-arrays inside this instance (if any)
        foreach($array as $each)
        {
            // check for children
            if(isset($each['children']))
            {
                // go deeper, passing in children and parent ID
                $this->checkForChildrenOtherwiseAddToArray($output, $each['children'], $each['id']);
            }
            // add current iteration to array as well
            $__ivhTreeviewExpanded      = isset($each['__ivhTreeviewExpanded'] )? $each['__ivhTreeviewExpanded']  : false ;
            $__ivhTreeviewIndeterminate = isset($each['__ivhTreeviewIndeterminate'])  ? $each['__ivhTreeviewIndeterminate'] : false  ;

            $output[] = array(
                'parent'                     => $parent_id,
                'id'                         => $each['id'],
                '__ivhTreeviewExpanded'      => $__ivhTreeviewExpanded,
                '__ivhTreeviewIndeterminate' => $__ivhTreeviewIndeterminate,
                'checked'                    => $each['checked'],
                'control_padre_id'           => $each['control_padre_id'],
                'descripcion'                => $each['descripcion'],
                'glosa'                      => $each['glosa'],
                'id'                         => $each['id'],
                'jerarquia'                  => $each['jerarquia'],
                'nombre'                     => $each['nombre'],
                'tipo_control_id'            => $each['tipo_control_id'],
                'valor'                      => $each['valor'],
            );
        }

        return $output ;
    }*/


    public function updateAccesosRol(Request $request)
    {

        $rol_id                  = $request->input('rol_id');
        $data                    = $request->input('controles');
        $reasignar_accesos_users = $request->input('reasignar_accesos_users');

        $update_rol_ctrl = RolControl::where('rol_id', $rol_id)
                                    ->update(['estado' => 0]);


        /*$rol_id                  = 2;
        $controles = '[{"id":34,"control_padre_id":null,"jerarquia":"10","nombre":"Gestión de Sms","valor":"sms","descripcion":"Gestión Sms","tipo_control_id":1,"glosa":"","checked":false,"children":[{"id":35,"control_padre_id":34,"jerarquia":"3001","nombre":"Envios Sms","valor":"sends","descripcion":"fa fa-comments","tipo_control_id":2,"glosa":"","checked":false,"children":[{"id":36,"control_padre_id":35,"jerarquia":"300101","nombre":"Speedy Sms","valor":"sends.speedy","descripcion":"sub menu","tipo_control_id":2,"glosa":"","checked":false,"children":[{"id":37,"control_padre_id":36,"jerarquia":"30010101","nombre":"Listar","valor":"list","descripcion":"fa-list","tipo_control_id":3,"glosa":"","checked":false,"children":[],"isExpanded":false,"$$hashKey":"object:300","__ivhTreeviewExpanded":true},{"id":38,"control_padre_id":36,"jerarquia":"30010102","nombre":"Nuevo","valor":"new","descripcion":"fa-plus","tipo_control_id":3,"glosa":"","checked":false,"children":[],"isExpanded":false,"$$hashKey":"object:301","__ivhTreeviewExpanded":true},{"id":39,"control_padre_id":36,"jerarquia":"30010103","nombre":"Editar","valor":"edit","descripcion":"fa-pencil-square-o","tipo_control_id":3,"glosa":"intable","checked":false,"children":[],"isExpanded":false,"$$hashKey":"object:302","__ivhTreeviewExpanded":true},{"id":40,"control_padre_id":36,"jerarquia":"30010104","nombre":"Eliminar","valor":"delete","descripcion":"fa-trash-o","tipo_control_id":3,"glosa":"intable","checked":false,"children":[],"isExpanded":false,"$$hashKey":"object:303","__ivhTreeviewExpanded":true}],"isExpanded":false,"$$hashKey":"object:283","__ivhTreeviewExpanded":true},{"id":41,"control_padre_id":35,"jerarquia":"300102","nombre":"Sms de Archivo","valor":"sends.files","descripcion":"sub menu","tipo_control_id":2,"glosa":"","checked":false,"children":[],"isExpanded":false,"$$hashKey":"object:284","__ivhTreeviewExpanded":true}],"isExpanded":false,"$$hashKey":"object:270","__ivhTreeviewExpanded":false}],"isExpanded":false,"$$hashKey":"object:245","__ivhTreeviewExpanded":true},{"id":2,"control_padre_id":null,"jerarquia":"20","nombre":"Gestión de Información","valor":"informacion","descripcion":"Gestión de Información","tipo_control_id":1,"glosa":"","checked":false,"children":[{"id":17,"control_padre_id":2,"jerarquia":"2001","nombre":"Personas","valor":"personas","descripcion":"fa fa-table","tipo_control_id":2,"glosa":"","checked":false,"children":[{"id":18,"control_padre_id":17,"jerarquia":"200101","nombre":"Personas","valor":"personas.personas","descripcion":"sub menu","tipo_control_id":2,"glosa":"","checked":false,"children":[{"id":19,"control_padre_id":18,"jerarquia":"20010101","nombre":"Listar","valor":"list","descripcion":"fa-list","tipo_control_id":3,"glosa":"","checked":false,"children":[],"isExpanded":false,"$$hashKey":"object:385","__ivhTreeviewExpanded":true},{"id":20,"control_padre_id":18,"jerarquia":"20010102","nombre":"Nuevo","valor":"new","descripcion":"fa-plus","tipo_control_id":3,"glosa":"","checked":false,"children":[],"isExpanded":false,"$$hashKey":"object:386","__ivhTreeviewExpanded":true},{"id":21,"control_padre_id":18,"jerarquia":"20010103","nombre":"Editar","valor":"edit","descripcion":"fa-pencil-square-o","tipo_control_id":3,"glosa":"","checked":false,"children":[],"isExpanded":false,"$$hashKey":"object:387","__ivhTreeviewExpanded":true},{"id":22,"control_padre_id":18,"jerarquia":"20010104","nombre":"Eliminar","valor":"delete","descripcion":"fa-trash-o","tipo_control_id":3,"glosa":"","checked":false,"children":[],"isExpanded":false,"$$hashKey":"object:388","__ivhTreeviewExpanded":true}],"isExpanded":false,"$$hashKey":"object:372","__ivhTreeviewExpanded":false}],"isExpanded":false,"$$hashKey":"object:355","__ivhTreeviewExpanded":true},{"id":23,"control_padre_id":2,"jerarquia":"2002","nombre":"Adm. Carteras","valor":"carteras","descripcion":"fa fa-briefcase","tipo_control_id":2,"glosa":"","checked":false,"children":[{"id":24,"control_padre_id":23,"jerarquia":"200201","nombre":"Carteras / Listas","valor":"carteras.listas","descripcion":"sub menu","tipo_control_id":2,"glosa":"","checked":false,"children":[{"id":25,"control_padre_id":24,"jerarquia":"20020101","nombre":"Listar","valor":"list","descripcion":"fa-list","tipo_control_id":3,"glosa":"","checked":false,"children":[],"isExpanded":false,"$$hashKey":"object:451","__ivhTreeviewExpanded":true},{"id":26,"control_padre_id":24,"jerarquia":"20020102","nombre":"Nuevo","valor":"new","descripcion":"fa-plus","tipo_control_id":3,"glosa":"","checked":false,"children":[],"isExpanded":false,"$$hashKey":"object:452","__ivhTreeviewExpanded":true},{"id":27,"control_padre_id":24,"jerarquia":"20020103","nombre":"Editar","valor":"edit","descripcion":"fa-pencil-square-o","tipo_control_id":3,"glosa":"intable","checked":false,"children":[],"isExpanded":false,"$$hashKey":"object:453","__ivhTreeviewExpanded":true},{"id":28,"control_padre_id":24,"jerarquia":"20020104","nombre":"Eliminar","valor":"delete","descripcion":"fa-trash-o","tipo_control_id":3,"glosa":"intable","checked":false,"children":[],"isExpanded":false,"$$hashKey":"object:454","__ivhTreeviewExpanded":true}],"isExpanded":false,"$$hashKey":"object:434","__ivhTreeviewExpanded":false},{"id":29,"control_padre_id":23,"jerarquia":"200202","nombre":"Contactos","valor":"carteras.contactos","descripcion":"sub menu","tipo_control_id":2,"glosa":"","checked":true,"children":[{"id":30,"control_padre_id":29,"jerarquia":"20020201","nombre":"Listar","valor":"list","descripcion":"fa-list","tipo_control_id":3,"glosa":"","checked":true,"children":[],"isExpanded":false,"$$hashKey":"object:500","__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false},{"id":31,"control_padre_id":29,"jerarquia":"20020202","nombre":"Nuevo","valor":"new","descripcion":"fa-plus","tipo_control_id":3,"glosa":"","checked":true,"children":[],"isExpanded":false,"$$hashKey":"object:501","__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false},{"id":32,"control_padre_id":29,"jerarquia":"20020203","nombre":"Editar","valor":"edit","descripcion":"fa-pencil-square-o","tipo_control_id":3,"glosa":"intable","checked":true,"children":[],"isExpanded":false,"$$hashKey":"object:502","__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false},{"id":33,"control_padre_id":29,"jerarquia":"20020204","nombre":"Eliminar","valor":"delete","descripcion":"fa-trash-o","tipo_control_id":3,"glosa":"intable","checked":true,"children":[],"isExpanded":false,"$$hashKey":"object:503","__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false}],"isExpanded":false,"$$hashKey":"object:435","__ivhTreeviewExpanded":false,"__ivhTreeviewIndeterminate":false}],"isExpanded":false,"$$hashKey":"object:356","__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":true}],"isExpanded":false,"$$hashKey":"object:246","__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":true},{"id":1,"control_padre_id":null,"jerarquia":"30","nombre":"Configuracion","valor":"configuracion","descripcion":"Configuracion","tipo_control_id":1,"glosa":"","checked":false,"children":[{"id":3,"control_padre_id":1,"jerarquia":"1002","nombre":"Accesos","valor":"accesos","descripcion":"fa fa-key","tipo_control_id":2,"glosa":"","checked":false,"children":[{"id":4,"control_padre_id":3,"jerarquia":"100101","nombre":"Roles","valor":"accesos.roles","descripcion":"sub menu","tipo_control_id":2,"glosa":"","checked":false,"children":[{"id":5,"control_padre_id":4,"jerarquia":"10010101","nombre":"Listar","valor":"list","descripcion":"fa-list","tipo_control_id":3,"glosa":"inbarra","checked":false,"children":[],"isExpanded":false,"$$hashKey":"object:579","__ivhTreeviewExpanded":true},{"id":6,"control_padre_id":4,"jerarquia":"10010102","nombre":"Nuevo","valor":"new","descripcion":"fa-plus","tipo_control_id":3,"glosa":"inbarra","checked":false,"children":[],"isExpanded":false,"$$hashKey":"object:580","__ivhTreeviewExpanded":true},{"id":7,"control_padre_id":4,"jerarquia":"10010103","nombre":"Editar","valor":"edit","descripcion":"fa-pencil-square-o","tipo_control_id":3,"glosa":"intable","checked":false,"children":[],"isExpanded":false,"$$hashKey":"object:581","__ivhTreeviewExpanded":true},{"id":8,"control_padre_id":4,"jerarquia":"10010104","nombre":"Eliminar","valor":"delete","descripcion":"fa-trash-o","tipo_control_id":3,"glosa":"intable","checked":false,"children":[],"isExpanded":false,"$$hashKey":"object:582","__ivhTreeviewExpanded":true},{"id":9,"control_padre_id":4,"jerarquia":"10010105","nombre":"Accesos Rol","valor":"access","descripcion":"Fa-check-square-o","tipo_control_id":3,"glosa":"inbarra","checked":false,"children":[],"isExpanded":false,"$$hashKey":"object:583","__ivhTreeviewExpanded":true}],"isExpanded":false,"$$hashKey":"object:562","__ivhTreeviewExpanded":true},{"id":10,"control_padre_id":3,"jerarquia":"100102","nombre":"Usuarios","valor":"accesos.usuarios","descripcion":"sub menu","tipo_control_id":2,"glosa":"","checked":false,"children":[{"id":11,"control_padre_id":10,"jerarquia":"10010201","nombre":"Listar","valor":"list","descripcion":"fa-list","tipo_control_id":3,"glosa":"","checked":false,"children":[],"isExpanded":false,"$$hashKey":"object:638","__ivhTreeviewExpanded":true},{"id":12,"control_padre_id":10,"jerarquia":"10010202","nombre":"Nuevo","valor":"new","descripcion":"fa-plus","tipo_control_id":3,"glosa":"","checked":false,"children":[],"isExpanded":false,"$$hashKey":"object:639","__ivhTreeviewExpanded":true},{"id":13,"control_padre_id":10,"jerarquia":"10010203","nombre":"Editar Rol","valor":"edit","descripcion":"fa-pencil-square-o","tipo_control_id":3,"glosa":"","checked":false,"children":[],"isExpanded":false,"$$hashKey":"object:640","__ivhTreeviewExpanded":true},{"id":14,"control_padre_id":10,"jerarquia":"10010204","nombre":"Eliminar","valor":"delete","descripcion":"fa-trash-o","tipo_control_id":3,"glosa":"","checked":false,"children":[],"isExpanded":false,"$$hashKey":"object:641","__ivhTreeviewExpanded":true},{"id":15,"control_padre_id":10,"jerarquia":"10010205","nombre":"Accesos","valor":"access","descripcion":"fa-check-square-o","tipo_control_id":3,"glosa":"","checked":false,"children":[],"isExpanded":false,"$$hashKey":"object:642","__ivhTreeviewExpanded":true},{"id":16,"control_padre_id":10,"jerarquia":"10010206","nombre":"Cambiar Contraseña","valor":"reset-password","descripcion":"fa-key","tipo_control_id":3,"glosa":"","checked":false,"children":[],"isExpanded":false,"$$hashKey":"object:643","__ivhTreeviewExpanded":true}],"isExpanded":false,"$$hashKey":"object:563","__ivhTreeviewExpanded":true}],"isExpanded":false,"$$hashKey":"object:549","__ivhTreeviewExpanded":true}],"isExpanded":false,"$$hashKey":"object:247","__ivhTreeviewExpanded":true}]' ;
        $data = json_decode($controles) ;
        $data = json_decode(json_encode($data), true); */


        // $your_array = json_decode($str);
        $output = array();
        $data_save   = $this->checkForChildrenOtherwiseAddToArray($output, $data);


        for ($i=0; $i < count($data_save) ; $i++)
        {
            $control_id = $data_save[$i]['id'] ;
            // $estado     = $data_save[$i]['checked'] ? 1 : 0 ;
            // $estado     = $data_save[$i]['isSelected'] ? 1 : 0 ;
            $referencia = $data_save[$i]['nombre'] ;
            if ($data_save[$i]['__ivhTreeviewIndeterminate'] == true || $data_save[$i]['checked'] == true)
            {
                $estado     = 1;
            }else{
                $estado     = 0;
            }

            $rol_control = RolControl::where('rol_id',$rol_id)
                            ->where('control_id',$control_id)
                            ->first() ;
            if ($rol_control )
            {
                 $rol_control->estado = $estado ;
                 $rol_control->save() ;
            }else
            {
                if ($estado == 1 )
                {
                    RolControl::create(array(
                            'rol_id'     => $rol_id,
                            'control_id' => $control_id,
                            'referencia' => $referencia,
                            'estado'     => $estado,
                        ));
                }

            }
        }

        if ($reasignar_accesos_users)
        {
           $this->ReAsigarAcesosUsersByRolId($rol_id) ;
        }


        $data = "Registro correcto" ;

         return \Response::json([
                            'message'  => 'Operación Correcta',
                            'error' => false,
                            'errors' => '' ,
                            'data'    => $data,
                        ]);

    }

    public function ReAsigarAcesosUsersByRolId($rol_id = 0)
    {
        // $rol_id = $request->input('rol_id');
        // $rol_id = 2;

        $data_users = User::where('rol_id',$rol_id)->get();
        $data_users = $data_users->toArray();

        # extraemos del array la columna id de users
        $users_ids = array_column($data_users, 'id') ;
        // $users_ids = implode(',', $users_ids) ;

        // $data = \DB::table('accesos')->whereIn('user_id', $users_ids)->get();
        $upd_accesos_users = \DB::table('accesos')
                                    ->whereIn('user_id', $users_ids)
                                    ->update(array('estado' => 0));

        $data_rol_control = RolControl::where('rol_id',$rol_id)
                                        ->where('estado',1)
                                        ->get() ;
         // dd($data_rol_control);

       if (count($data_rol_control) > 0)
       {
            if (count($data_users))
            {
                for ($i=0; $i < count($data_users); $i++)
                {
                    $user_id = $data_users[$i]['id'] ;

                    foreach ($data_rol_control as $row)
                    {
                        $control_id = $row->control_id ;

                        $acceso = Acceso::where('user_id',$user_id)
                                        ->where('control_id',$control_id)
                                        ->first();
                        if ($acceso)
                        {
                            $acceso->estado = 1 ;
                            $acceso->save();
                        }
                        else
                        {
                            $acceso = new Acceso();
                            $acceso->user_id = $user_id ;
                            $acceso->control_id = $control_id ;
                            $acceso->referencia = $row->referencia ;
                            $acceso->estado = 1 ;
                            $acceso->save();
                        }
                    }
                }
            }
        }
        // dd($data_users);

        return $upd_accesos_users ;

    }
}
