<?php

namespace App\Http\Controllers\Accesos;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Accesos\Transaccion ;
class TransaccionesController extends Controller
{
	public function save($model = null,$type='')
	{
		$user = \Auth::user();
		$user_id = $user->id ;

		if (empty($model))  return ;

		$descripcion = $type;


		$tableName = $model->getTable() ;

		$columns = $model->toArray() ;

		$object_id = $columns['id'] ;
		$glosa = json_encode($columns,true);

		$transaccion = new Transaccion ;
		$transaccion->user_id     = $user_id ;
		$transaccion->object_id   = $object_id ;
		$transaccion->object      = $tableName ;
		$transaccion->glosa       = $glosa  ;
		$transaccion->descripcion = $descripcion  ;
		$transaccion->save();
	}

	public function created($model = null)
	{
		$this->save($model,'created') ;
	}

	public function updated($model = null)
	{
		$this->save($model,'updated') ;
	}

	public function deleted($model = null)
	{
		$this->save($model,'deleted') ;
	}
}
