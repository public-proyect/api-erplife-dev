<?php

namespace App\Http\Controllers\Accesos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Auth;


use App\User;
use App\Models\Personas\Persona;
use App\Models\Personas\PerImagen;
use App\Models\Accesos\Rol;
use App\Models\Accesos\UserPersona;
use App\Models\Accesos\UserInfo;

use App\Http\Controllers\Accesos\UserSessionsController ;

class AuthenticationController extends Controller
{
    public function __construct(){
        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

    public function authenticate(Request $request){

       $email    = $request->input('email');
       $password = $request->input('password');
       $token    = null;

        $credentials = array(
                'email'    => $email,
                'password' => $password,
                'estado'   => 1,
            );

        try{
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'message' => 'Usuario o Contraseña Invalidas',
                    'error'   => true,
                    'errors'  => [],
                    'data'    => [] ,

                ],401);
            }
        }catch(JWTException $ex){
            return response()->json([
                    'message' => 'Algo salió mal',
                    'error'   => true,
                    'errors'  => [],
                    'data'    => [],

                ],500);
        }

        $user = JWTAuth::toUser($token);
        UserSessionsController::save();

        # referencia del usuario registralo una sola ves
            /*$user_id    = $user->id ;
            $empresa_id = $user->empresa_id ;

            $params_empresa =  array(
                    'user_id'    => $user_id ,
                    'empresa_id' => $empresa_id ,
                 );
        */
           /*$empresas = $this->getEmpresasByUserEmpresaId($params_empresa);
            $data = $empresas ;      */

        return response()->json([
                'message' => 'Validación Correcta',
                'error'   => false,
                'errors'  => [],
                /*'data'    => $data ,*/
                'token'   => $token ,
            ]);
    }

    /**
     * Metodo que seleccionas la empresas a las que tiene acceso un usuario
    */
    public function getEmpresasByUserEmpresaId(Request $request)
    {
        /*$token = (String)JWTAuth::getToken() ;
        $user_session = UserSessionsController::getSesion($token);
        $user_id = $user_session->user_id;*/

        $user = \Auth::user();
        $user_id    = $user->id ;
        // $empresa_id = $user->empresa_id ;

        // $user_id    = 1 ;
        $empresas = UserPersona::where('user_id',$user_id)
                            ->join('persona', function($join) {
                                      $join->on('users_persona.per_id_padre','=', 'persona.id')
                                      ->where('persona.per_id_padre','=',0)
                                      ->where('persona.estado','=',1);
                                    })
                            ->select([
                                'persona.id as per_id_padre', # id empresa(table persona)
                                'persona.per_nombre as razon_social',
                                'persona.per_apellidos as nombre_comercial',
                                'users_persona.rol_id',
                                'users_persona.id as user_persona_id',

                             ])
                            ->get() ;

        // encriptar variables de usuario
        foreach ($empresas as $row)
        {
            $row->user_persona_id = \Crypt::encrypt($row->user_persona_id);
            $row->per_id_padre    = \Crypt::encrypt($row->per_id_padre);
        }

       return response()->json([
                'message' => 'Operación Correcta',
                'error'   => false,
                'errors'  => [],
                'data'    => $empresas ,
            ]);

    }

    //  extraer la datos del usuario desde el api
    public function getUserInfo(Request $request)
    {
        // $user_persona_id = $request->input('user_persona_id');
        // $access_token = \Request::header('Authorization');
        // $parmas_adictional_1 = \Request::header('parmas_adictional_1');
        // $parmas_adictional_2 = $request->input('parmas_adictional_1');

        $user = \Auth::user() ;
        $user_id = $user->id ;
        $email   = $user->email ;
        $alias   = $user->alias ;


        $user_info = UserInfo::where('id',$user_id)
                    ->first() ;

        $avatar = '';
        if (empty($user_info))
        {
            $avatar = 'avatars/avatar_user.png' ;
            $user_info->avatar = $avatar ;
        }

        $params = array(
                'email'     => $email,
                'alias'     => $alias,
                'apellidos' => $user_info->apellidos,
                'avatar'    => $user_info->avatar,
                'direccion' => $user_info->direccion,
                'dni'       => $user_info->dni,
                'nombre'    => $user_info->nombre,
                'telefonos' => $user_info->telefonos,
            ) ;

        $data = $params ;

        return response()->json([
                'message' => '',
                'error'   => false,
                'errors'  => [],
                'data'    => $data ,
            ]);

    }

     // cerrar session de app
    public function logout(Request $request)
    {

        $token = (String)JWTAuth::getToken() ;

        JWTAuth::invalidate(JWTAuth::getToken());
        \Auth::logout();

        $data = UserSessionsController::updateFechaSalida($token) ;


        return response()->json([
                'message' => 'Session Cerrada Correctamente',
                'error'   => false,
                'errors'  => [],
                'data'    => $data ,
                // 'data1'    => $data1 ,

            ]);
    }

    public function getAuthenticatedUser(Request $request)
    {

        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        // the token is valid and we have found the user via the sub claim
        return response()->json(compact('user'));
    }

    public function getAuthUser(Request $request)
    {
        $user = JWTAuth::toUser($request->token);
        return response()->json(['result' => $user]);
    }

    /*public function getUserInfo($user_persona_id)
    {

        $user = \Auth::user() ;
        $user_id = $user->id ;
        $email   = $user->email ;
        $alias   = $user->alias ;

        $user_persona = UserPersona::find($user_persona_id);
        $persona_id   = $user_persona->persona_id ;
        $per_id_padre = $user_persona->per_id_padre ;
        $rol_id       = $user_persona->rol_id ;


        $user_per = array(
                            'user_persona_id' => $user_persona_id,
                            'persona_id'      => $persona_id,
                            'per_id_padre'    => $per_id_padre,
                            'rol_id  '        => $rol_id,
                        );

        \Session::put('auth_u_p',$user_per);


        $persona = Persona::where('id',$persona_id)
                    ->first() ;

        $per_imagen = PerImagen::where('persona_id',$persona_id)
                ->first() ;

        if (!empty($per_imagen))
        {
            $avatar = $per_imagen->url ;
        }else
        {
            $avatar = 'img_avatars/avatar_user.png' ; ;
        }

        $rol = Rol::where('id',$rol_id)
                    ->first() ;


        $params = array(
            'user_id'         => $user_id,
            'user_persona_id' => $user_persona_id,
            'avatar'          => $avatar,
            'rol_id'          => $rol_id,
            'rol'             => $rol->nombre,
            'email'           => $email,
            'alias'           => $alias,
            'nombre'          => $persona->per_nombre,
            'apellidos'       => $persona->per_apellidos,
            'per_tipo'        => $persona->per_tipo,
            ) ;

        $data = array_merge($params) ;
        return $data ;
    }
    */
}
