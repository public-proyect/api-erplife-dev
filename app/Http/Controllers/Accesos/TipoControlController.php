<?php

namespace App\Http\Controllers\Accesos;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Accesos\TipoControl;
use JWTAuth;


class TipoControlController extends Controller
{
    public function __construct(JWTAuth $auth)
    {
        $this->middleware('jwt.auth');
    }

    public function getTiposControl()
	{
        $data = TipoControl::where('estado',1)->get() ;

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function save(Request $request)
    {

        $descripcion = $request->input('descripcion') ;
        $glosa = $request->input('glosa') ;

    	$rules = [
            	'descripcion'      => 'descripcion',
            ];

        try 
        {

	        $tipo_control = TipoControl::where(['descripcion' => $descripcion])->first();

	        if (!$tipo_control)
	        {
	        	$tipo_control = new TipoControl() ;
                $tipo_control->descripcion = $descripcion ;
                $tipo_control->glosa = $glosa ;
	        	$tipo_control->save() ;
	        }
	        else
	        {
                $tipo_control->descripcion = $descripcion ;
                $tipo_control->glosa = $glosa ;
	        	$tipo_control->estado = 1 ;
	        	$tipo_control->save() ;
	        }

	        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => "OK",
	                    ]);

        } 
        catch (Exception $e)
        {
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }
    }

	public function getTipoControlById($id)
    {

        $tipo_control_id = $id ;

        $data = TipoControl::where('id',$tipo_control_id)->first() ;

        return response()->json([
                	'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function update(Request $request)
    {

        $tipo_control_id = $request->input('id') ;
        $descripcion = $request->input('descripcion') ;
        $glosa = $request->input('glosa') ;

        $tipo_control =  TipoControl::find($tipo_control_id) ;
		$tipo_control->descripcion = $descripcion ;
		$tipo_control->glosa = $glosa ;
        $tipo_control->save() ;

        $data =  TipoControl::find($tipo_control_id) ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function updateEstado(Request $request)
    {

		$tipo_control_id = $request->input('id') ;
		$estado = $request->input('estado') ;

        $tipo_control =  TipoControl::find($tipo_control_id) ;
        $tipo_control->estado = $estado ;
        $tipo_control->save() ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $estado,
                ]);
    }
}
