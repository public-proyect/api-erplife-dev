<?php

namespace App\Http\Controllers\Accesos;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Accesos\UserSession ;

class UserSessionsController extends Controller
{
    public static function save()
    {
        $user = \Auth::user();
        $user_id = $user->id ;

        $user_session = new UserSession();

        $user_session->user_id       = $user_id ;
        $user_session->fecha_entrada = date('Y-m-d H:i:s');
        $user_session->token         = \JWTAuth::getToken() ;
        $user_session->save();

        $user_session_id = $user_session->id ;

        return  $user_session_id ;

        // \Session::push('user_session.id', $user_session_id);
    } 

    public static function getSesion($token){
        $user_session = UserSession::where('token',$token)
                    ->first();
        return $user_session;             
    }

    public static function updateFechaSalida($token)
    {
          // $user_session_id = \Session::get('user_session.id');

          $user_session = UserSession::where('token',$token)
                    ->first();

          if (!$user_session) return ;

          $user_session->fecha_salida = date('Y-m-d H:i:s');
          $user_session->save();

          // \Session::forget('user_session.id');
    }

    public function fecha()
    {
        $date = \Carbon\Carbon::now();
        var_dump($date->format('Y-m-d H-i-s') );
        dd($date);
        dd(date('Y-m-d H:m:s'));
    }

}
