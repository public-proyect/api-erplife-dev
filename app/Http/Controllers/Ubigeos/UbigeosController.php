<?php

namespace App\Http\Controllers\Ubigeos;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use JWTAuth;
use App\User;

use App\Models\Ubigeos\Ubigeo ;

class UbigeosController extends Controller
{
      public function __construct(\JWTAuth $auth)
      {
        $this->middleware('jwt.auth');
      }

   	public function getDistritos(Request $request)
   	{
   		$pais_id = !empty($request->input('pais_id'))? $request->input('pais_id'): 1;

   		$data = Ubigeo::where('tipo_ubigeo_id',3)
   				->where('pais_id',$pais_id)
               	->orderBy('descripcion','asc')
   				->get();


   		return \Response::json([
                     'message' => 'Operación Correcta',
                     'error' => false,
                     'errors' => '' ,
                     'data' => $data,
	            ]);
   	}

   	public function getProvincias(Request $request)
   	{
   		$pais_id = !empty($request->input('pais_id'))? $request->input('pais_id'): 1;
   		$data = Ubigeo::where('tipo_ubigeo_id',2)
   				->where('pais_id',$pais_id)
               ->orderBy('descripcion','asc')
   				->get();

   		return \Response::json([
	             	'message' => 'Operación Correcta',
	                'error' => false,
					    'errors' => '' ,
	                'data' => $data,
	            ]);
   	}

   	public function getDepartamentos(Request $request)
   	{
   		$pais_id = !empty($request->input('pais_id'))? $request->input('pais_id'): 1;

   		$data = Ubigeo::where('tipo_ubigeo_id',1)
   				->where('pais_id',$pais_id)
               ->orderBy('descripcion','asc')
   				->get();

   		return \Response::json([
	             	'message' => 'Operación Correcta',
	                'error' => false,
					    'errors' => '' ,
	                'data' => $data,
	            ]);
   	}

      public function getDistritosByDepartameto(Request $request)
      {
         $departamento_id =  $request->input('departamento_id');
		
         // $departamento_id = 1351 ;
         $data = Ubigeo::where('ubigeo.ubigeo_id_padre',$departamento_id)
               ->join('ubigeo as distrito', function($join) {
                        $join->on('distrito.ubigeo_id_padre','=', 'ubigeo.id')
                        ->where('distrito.estado','=',1);
                      })
               ->orderBy('distrito.descripcion','asc')
               ->get();
		

         return \Response::json([
                     'message' => 'Operación Correcta',
                     'error'   => false,
                     'errors'  => '' ,
                     'data'    => $data,
               ]);
      }
}
