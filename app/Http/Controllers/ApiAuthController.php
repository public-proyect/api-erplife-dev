<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use JWTAuth;
use Auth;

use Tymon\JWTAuth\Exceptions\JWTException;


use App\User;
use App\Models\Personas\Persona;
use App\Models\Personas\PerImagen;
use App\Models\Accesos\Rol;
use App\Models\Accesos\UserPersona;

use App\Http\Controllers\Accesos\UserSessionsController ;

class ApiAuthController extends Controller
{
     public function __construct()
    {
       // Apply the jwt.auth middleware to all methods in this controller
       // except for the index method.
       $this->middleware('jwt.auth', ['except' => ['authenticate']]);
       $this->middleware('cors');

    }

    public function authenticate(Request $request)
    {
        $email    = $request->input('email');
        $password = $request->input('password');

        // $email    = "armandoaepp@gmail.com";
        // $email    = "giovanni.lockman@example.org";
        // $password = "armando";

         /*return response()->json([
                'status' => app('Illuminate\Http\Response')->status(),
            ]);
            */
        $token    = null;

        $credentials = array(
                'email'    => $email,
                'password' => $password,
                'estado'   => 1,
            );

        try{
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'message' => 'Usuario o Contraseña Invalidas',
                    'error'   => true,
                    'errors'  => [],
                    'data'    => [] ,

                ]);
                // return response()->json(['error' => 'invalid_credentials']);
            }
        }catch(JWTException $ex){
            return response()->json([
                    'message' => 'someting_went_wrong',
                    'error'   => true,
                    'errors'  => [],
                    'data'    => [],

                ]);
        }

        $user = JWTAuth::toUser($token);
        UserSessionsController::save();

         # referencia del usuario registralo una sola ves
            // $user = Auth::user();
            $user_id    = $user->id ;
            $empresa_id = $user->empresa_id ;

            $params_empresa =  array(
                    'user_id'    => $user_id ,
                    'empresa_id' => $empresa_id ,
                 );

            $empresas = $this->getEmpresasByUserEmpresaId($params_empresa);

            /* # para usuario paa una sola empresa
                $empresa_only = 0 ;
                $data_user = [] ;
                $data_empresa = [] ;

                $cant_empresas = count($empresas) ;

                if ($cant_empresas == 1 )
                {
                    $empresa      = $empresas->first();
                    $user_persona_id = $empresa->user_persona_id ;

                    $empresa_only = 1 ;
                    $data_user    = $this->getUserInfo($user_persona_id);
                    $data_empresa = $empresa  ;

                }
                else if ($cant_empresas > 1)
                {
                    $empresa_only = $cant_empresas ;
                    $data_user    = [];
                    $data_empresa = $empresas  ;

                }else
                {
                    $empresa_only = 0 ;
                    $data_user = [] ;
                    $data_empresa = []  ;
                }

                $data = array(
                                'empresa_only' => $empresa_only,
                                'data_user'    => $data_user,
                                'data_empresa' => $data_empresa,
                            ) ;
            */

            $data = $empresas ;
            // $data['token']    = $token ;

        return response()->json([
                'message' => 'Validación Correcta',
                'error'   => false,
                'errors'  => [],
                'data'    => $data ,
                'token'   => $token ,
            ]);

        // return response()->json(compact('token','user','info'));
    }

    /**
     * Metodo que seleccionas la empresas a las que tiene acceso un usuario
    */
    public function getEmpresasByUserEmpresaId($params)
    {
       /* $user = \Auth::user();
        $user_id    = $user->id ;
        $empresa_id = $user->empresa_id ;*/

        $user_id    = $params['user_id'] ;
        $empresa_id = $params['empresa_id'] ;

        $empresas = UserPersona::where('user_id',$user_id)
                            ->join('persona', function($join) {
                                      $join->on('persona.id','=', 'users_persona.per_id_padre')
                                      ->where('persona.estado','=',1);
                                    })
                            ->select([
                                'persona.id as persona_id', # id de la empresa
                                'persona.per_nombre',
                                'persona.per_apellidos',
                                'persona.per_fecha_nac',
                                // 'persona.per_id_padre',
                                'users_persona.rol_id',
                                'users_persona.id as user_persona_id',
                             ])
                            ->get() ;

        // $empresas = $empresas->toArray();
       return $empresas ;

    }

    public function getUserInfo($user_persona_id)
    {

        $user = \Auth::user() ;
        $user_id = $user->id ;
        $email   = $user->email ;
        $alias   = $user->alias ;

        $user_persona = UserPersona::find($user_persona_id);
        $persona_id   = $user_persona->persona_id ;
        $per_id_padre = $user_persona->per_id_padre ;
        $rol_id       = $user_persona->rol_id ;


        $user_per = array(
                            'user_persona_id' => $user_persona_id,
                            'persona_id'      => $persona_id,
                            'per_id_padre'    => $per_id_padre,
                            'rol_id  '        => $rol_id,
                        );

        \Session::put('users_persona',$user_per);


        $persona = Persona::where('id',$persona_id)
                    ->first() ;

        $per_imagen = PerImagen::where('persona_id',$persona_id)
                ->first() ;

        if (!empty($per_imagen))
        {
            $avatar = $per_imagen->url ;
        }else
        {
            $avatar = 'img_avatars/avatar_user.png' ; ;
        }

        $rol = Rol::where('id',$rol_id)
                    ->first() ;


        $params = array(
            'user_id'         => $user_id,
            'user_persona_id' => $user_persona_id,
            'avatar'          => $avatar,
            'rol_id'          => $rol_id,
            'rol'             => $rol->nombre,
            'email'           => $email,
            'alias'           => $alias,
            'nombre'          => $persona->per_nombre,
            'apellidos'       => $persona->per_apellidos,
            'per_tipo'        => $persona->per_tipo,
            ) ;

        $data = array_merge($params) ;
        return $data ;
    }


    //  extraer la datos del usuario desde el api
    public function getDataUserInfo(Request $request)
    {
        $user_persona_id = $request->input('user_persona_id');

        $data = $this->getUserInfo($user_persona_id) ;

        return response()->json([
                'message' => '',
                'error'   => false,
                'errors'  => [],
                'data'    => $data ,
            ]);

    }

     // cerrar session de app
    public function logout(Request $request)
    {

        $token = (String)JWTAuth::getToken() ;

        JWTAuth::invalidate(JWTAuth::getToken());
        \Auth::logout();

        $request->session()->forget('users_persona');
        $request->session()->flush();

        $data = UserSessionsController::updateFechaSalida($token) ;


        return response()->json([
                'message' => 'Session Cerrada Correctamente',
                'error'   => false,
                'errors'  => [],
                'data'    => $data ,
                // 'data1'    => $data1 ,

            ]);
    }

    public function getAuthenticatedUser(Request $request)
    {

        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        // the token is valid and we have found the user via the sub claim
        return response()->json(compact('user'));
    }

    public function getDataUserPersona(Request $request)
    {
        $data_session = $request->session()->get('users_persona');
        return response()->json(compact( 'data_session'));
    }

    public function getAuthUser(Request $request)
    {
        $user = JWTAuth::toUser($request->token);
        return response()->json(['result' => $user]);
    }


}
