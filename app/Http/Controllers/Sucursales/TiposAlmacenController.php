<?php

namespace App\Http\Controllers\Sucursales;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests\Sucursales\TipoAlmacenRequest;
use App\Http\Controllers\Controller;
use App\Models\Sucursales\TipoAlmacen;
class TiposAlmacenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct(){
         $this->middleware(['jwt.auth']); 
     }

    public function getTiposAlmacen(){
        return response()->json([
            "data"=>TipoAlmacen::orderBy('id','desc')->where([['estado',1],['per_id_padre',1]])->get()
            ]);
    }

    public function postTipoAlmacen(Request $request){        
        try{
            $validator=Validator::make($request->all(),[
                'descripcion' => 'required|max:255',
                'glosa' => 'max:255',
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'res'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
             $tipoAlmacen =new TipoAlmacen();
             $tipoAlmacen->per_id_padre = 1 ; 
             $tipoAlmacen->descripcion = $request->input('descripcion') ; 
             $tipoAlmacen->glosa = ($request->input('glosa') !=null)?$request->input('glosa'):'';      
             $tipoAlmacen->save();
                return response()->json([
                    "res"=>"El tipo almacen con id {$tipoAlmacen->id} ha sido guardado"
                ]);

        }catch(Exception $e){   
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
         
    } 

    public function getTipoAlmacenById($id){
        $tipoAlmacen= TipoAlmacen::find($id);
        return response()->json([
            "data"=>$tipoAlmacen
        ]);
    }

    public function updateTipoAlmacen(Request $request){
        try{
            $validator=Validator::make($request->all(),[
                'id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'descripcion' => 'required|max:255',
                'glosa' => 'max:255',
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'res'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
              $tipoalmacen_id=$request->input('id');
              $tipoAlmacen= TipoAlmacen::find($tipoalmacen_id);
              $tipoAlmacen->descripcion = $request->input('descripcion') ; 
              $tipoAlmacen->glosa = ($request->input('glosa') !=null)?$request->input('glosa'):'';      
              $tipoAlmacen->save();
              return response()->json([
              "res"=>"El tipo almacen con id {$tipoAlmacen->id} ha sido editado"
              ]);
        }catch(Exception $e){
            return response()->json([
                    'message'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }

        
    }

    public function deleteTipoAlmacen(Request $request){
         $tipoalmacen_id=$request->input('id');
         $tipoAlmacen= TipoAlmacen::find($tipoalmacen_id);
         $tipoAlmacen->estado=$request['estado'];
         $tipoAlmacen->save();
           return response()->json([
            "res"=>"El tipo almacen con id {$tipoAlmacen->id} ha sido eliminado"
        ]);
    }
}
