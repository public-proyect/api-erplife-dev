<?php

namespace App\Http\Controllers\Sucursales;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Sucursales\Maquina;
class MaquinasController extends Controller
{
    public function __construct(){
         $this->middleware(['jwt.auth']); 
     }

    public function getMaquinas(){
        return response()->json([
            "data"=>Maquina::with('tienda')->where([['estado',1]])
            ->orderBy('id','desc')->get()
            ]);    
    }

    public function postMaquina(Request $request){
         try{
            $validator=Validator::make($request->all(),[
                'tienda_id' => 'required|integer|regex:/^[1-9]+[0-9]*$/',
                'codigo_maquina' => 'required|max:45|regex:/^[a-zA-Z0-9_-]*$/',
                'modelo_maquina'=>'required|max:45|regex:/^[a-zA-Z0-9 _-]*$/',
                'serie_maquina'=>'required|max:45|regex:/^[a-zA-Z0-9_-]*$/',
                'descripcion'=>'required|max:255',
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
            $maquina =new Maquina();
            $maquina->tienda_id = $request->input('tienda_id') ; 
            $maquina->codigo_maquina = $request->input('codigo_maquina') ; 
            $maquina->modelo_maquina = $request->input('modelo_maquina');  
            $maquina->serie_maquina = $request->input('serie_maquina');   
            $maquina->descripcion = $request->input('descripcion');      
            $maquina->save();
            return response()->json([
                "res"=>"La maquina con id {$maquina->id} ha sido guardado"
            ]);

        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
         
    } 

    public function getMaquinaById($id){
        $maquina= Maquina::find($id);
        return response()->json([
            "data"=>$maquina
        ]);
    }

    public function updateMaquina(Request $request){
         try{
            $validator=Validator::make($request->all(),[
                'id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'tienda_id' => 'required|integer|regex:/^[1-9]+[0-9]*$/',
                'codigo_maquina' => 'required|max:45|regex:/^[a-zA-Z0-9_-]*$/',
                'modelo_maquina'=>'required|max:45|regex:/^[a-zA-Z0-9 _-]*$/',
                'serie_maquina'=>'required|max:45|regex:/^[a-zA-Z0-9_-]*$/',
                'descripcion'=>'required|max:255',
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
            $maquina_id=$request->input('id');
            $maquina= Maquina::find($maquina_id);
            $maquina->tienda_id = $request->input('tienda_id') ; 
            $maquina->codigo_maquina = $request->input('codigo_maquina') ; 
            $maquina->modelo_maquina = $request->input('modelo_maquina');  
            $maquina->serie_maquina = $request->input('serie_maquina');   
            $maquina->descripcion = $request->input('descripcion');         
            $maquina->save();
            return response()->json([
                "res"=>"La tienda con id {$maquina->id} ha sido editado"
            ]);

        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
         
    }

    public function deleteMaquina(Request $request){
         $maquina_id=$request['id'];
         $maquina= Maquina::find($maquina_id);
         $maquina->estado=$request->input('estado');
         $maquina->save();
           return response()->json([
            "res"=>"La tienda con id {$maquina->id} ha sido eliminado"
        ]);
    }
}
