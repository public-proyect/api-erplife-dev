<?php

namespace App\Http\Controllers\Sucursales;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Models\Sucursales\Tienda;
class TiendasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct(){
         $this->middleware(['jwt.auth']); 
     }

    public function getTiendas(){
        return response()->json([
            "data"=>Tienda::with('sucursal')
            ->where([['tienda.estado',1],['tienda.per_id_padre',1]])
            ->select(['tienda.*','distrito.descripcion as dist_des'])
            ->join('ubigeo as distrito',function($join){
                $join->on('distrito.id','=','distrito_id')                
                ->where('distrito.estado',1);
            })
            ->orderBy('tienda.id','desc')->get()
            ]);   
    }

    public function postTienda(Request $request){
        try{
            $validator=Validator::make($request->all(),[
                'sucursal_id' => 'required|integer|regex:/^[1-9]+[0-9]*$/',
                'distrito_id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'codigo'=>'required|max:255|regex:/^[a-zA-Z0-9_-]*$/',
                'nombre'=>'required|max:255|regex:/^[a-zA-Z0-9 _-]*$/',
                'descripcion'=>'required|max:255',
                'direccion'=>'required|max:255'
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
            $tienda =new Tienda();
            $tienda->per_id_padre = 1 ;
            $tienda->sucursal_id = $request->input('sucursal_id'); 
            $tienda->distrito_id = $request->input('distrito_id') ; 
            $tienda->codigo = $request->input('codigo');  
            $tienda->nombre = $request->input('nombre');   
            $tienda->descripcion = $request->input('descripcion');  
            $tienda->direccion = $request->input('direccion');      
            $tienda->save();
            return response()->json([
                "res"=>"La tienda con id {$tienda->id} ha sido guardado"
            ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
         
    } 

    public function getTiendaById($id){
        $tienda= Tienda::find($id);
        return response()->json([
            "data"=>$tienda
        ]);
    }

    public function updateTienda(Request $request){
        try{
            $validator=Validator::make($request->all(),[
                'id'=>'required|integer',
                'sucursal_id' => 'required|integer|regex:/^[1-9]+[0-9]*$/',
                'distrito_id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'codigo'=>'required|max:255|regex:/^[a-zA-Z0-9_-]*$/',
                'nombre'=>'required|max:255|regex:/^[a-zA-Z0-9 _-]*$/',
                'descripcion'=>'required|max:255',
                'direccion'=>'required|max:255'
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
            $tienda_id=$request->input('id');
            $tienda= Tienda::find($tienda_id);
            $tienda->sucursal_id = $request->input('sucursal_id'); 
            $tienda->distrito_id = $request->input('distrito_id') ; 
            $tienda->codigo = $request->input('codigo');  
            $tienda->nombre = $request->input('nombre');   
            $tienda->descripcion = $request->input('descripcion');  
            $tienda->direccion = $request->input('direccion');       
            $tienda->save();
            return response()->json([
                "res"=>"La tienda con id {$tienda->id} ha sido editado"
            ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
         
    }

    public function deleteTienda(Request $request){
         $tienda_id=$request->input('id');
         $tienda= Tienda::find($tienda_id);
         $tienda->estado=$request->input('estado');
         $tienda->save();
           return response()->json([
            "res"=>"La sucursal con id {$tienda->id} ha sido eliminado"
        ]);
    }
}
