<?php

namespace App\Http\Controllers\Sucursales;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Sucursales\PuntoEmision;

class PuntosEmisionController extends Controller
{
    public function __construct(){
         $this->middleware(['jwt.auth']); 
     }

    public function getPuntosEmision(){
        return response()->json([
            "data"=>PuntoEmision::with('tienda','maquina')->where([['estado',1],['per_id_padre',1]])
            ->orderBy('id','desc')->get()
            ]);    
    }

    public function postPuntoEmision(Request $request){
         try{
            $validator=Validator::make($request->all(),[
                'tienda_id' => 'required|integer|regex:/^[1-9]+[0-9]*$/',
                'maquina_id' => 'required|integer|regex:/^[1-9]+[0-9]*$/',
                'descripcion'=>'required|max:255'
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
             $puntoEmision =new PuntoEmision();
             $puntoEmision->tienda_id = $request->input('tienda_id') ; 
             $puntoEmision->maquina_id = $request->input('maquina_id') ; 
             $puntoEmision->per_id_padre =1;  
             $puntoEmision->descripcion = $request->input('descripcion');   
             $puntoEmision->save();
             return response()->json([
                "res"=>"El punto de emision con id {$puntoEmision->id} ha sido guardado"
             ]);

        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
        
    } 

    public function getPuntoEmisionById($id){
        $puntoEmision= PuntoEmision::find($id);
        return response()->json([
            "data"=>$puntoEmision
        ]);
    }

    public function updatePuntoEmision(Request $request){
        try{
            $validator=Validator::make($request->all(),[
                'id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'tienda_id' => 'required|integer|regex:/^[1-9]+[0-9]*$/',
                'maquina_id' => 'required|integer|regex:/^[1-9]+[0-9]*$/',
                'descripcion'=>'required|max:255'
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
             $puntoEmision_id=$request['id'];
             $puntoEmision= PuntoEmision::find($puntoEmision_id);
             $puntoEmision->tienda_id = $request->input('tienda_id') ; 
             $puntoEmision->maquina_id = $request->input('maquina_id') ; 
             $puntoEmision->per_id_padre =1;  
             $puntoEmision->descripcion = $request->input('descripcion');    
             $puntoEmision->save();
             return response()->json([
                "res"=>"El punto de emision con id {$puntoEmision->id} ha sido editado"
             ]);

        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
        
    }

    public function deletePuntoEmision(Request $request){
         $puntoEmision_id=$request->input('id');
         $puntoEmision= PuntoEmision::find($puntoEmision_id);
         $puntoEmision->estado=$request->input('estado');
         $puntoEmision->save();
           return response()->json([
            "res"=>"El punto de emision con id {$puntoEmision->id} ha sido eliminado"
        ]);
    }
}
