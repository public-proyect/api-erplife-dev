<?php

namespace App\Http\Controllers\Sucursales;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Sucursales\Almacen;
class AlmacenesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct(){
         $this->middleware(['jwt.auth']); 
     }

    public function getAlmacenes(){
        return response()->json([
            "data"=>Almacen::with(['tipoAlmacen','tienda'])
            ->where([['almacen.estado',1],['almacen.per_id_padre',1]])
             ->select(['almacen.*','distrito.descripcion as dist_des'])
            ->join('ubigeo as distrito',function($join){
                $join->on('distrito.id','=','distrito_id')                
                ->where('distrito.estado',1);
            })
            ->orderBy('almacen.id','desc')->get()
            ]);   
    }

    public function postAlmacen(Request $request){
        try{
            $validator=Validator::make($request->all(),[
                'tienda_id' => 'required|integer|regex:/^[1-9]+[0-9]*$/',
                'tipo_almacen_id' => 'required|integer|regex:/^[1-9]+[0-9]*$/',
                'distrito_id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'codigo'=>'required|max:10',
                'descripcion'=>'required|max:255',
                'direccion'=>'required|max:255',
                'telefono'=>'required|max:45',
                'glosa'=>'max:255'
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
            $almacen =new Almacen();
            $almacen->per_id_padre = 1 ;
            $almacen->tienda_id = $request->input('tienda_id') ; 
            $almacen->tipo_almacen_id = $request->input('tipo_almacen_id'); 
            $almacen->distrito_id = $request->input('distrito_id'); 
            $almacen->codigo = $request->input('codigo');  
            $almacen->descripcion = $request->input('descripcion');   
            $almacen->direccion = $request->input('direccion');  
            $almacen->telefono = $request->input('telefono');  
            $almacen->glosa =  ($request->input('glosa') !=null)?$request->input('glosa'):'';      
            $almacen->save();
            return response()->json([
                    "res"=>"El almacen con id {$almacen->id} ha sido guardado"
                    ]);

        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
         
      
    } 

    public function getAlmacenById($id){
        $almacen= Almacen::find($id);
        return response()->json([
            "data"=>$almacen
        ]);
    }

    public function updateAlmacen(Request $request){

        try{
            $validator=Validator::make($request->all(),[
                'id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'tienda_id' => 'required|integer|regex:/^[1-9]+[0-9]*$/',
                'tipo_almacen_id' => 'required|integer|regex:/^[1-9]+[0-9]*$/',
                'distrito_id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'codigo'=>'required|max:10',
                'descripcion'=>'required|max:255',
                'direccion'=>'required|max:255',
                'telefono'=>'required|max:45',
                'glosa'=>'max:255'
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'messsage'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
            $almacen_id=$request->input('id');
            $almacen= Almacen::find($almacen_id);
            $almacen->per_id_padre = 1 ;
            $almacen->tienda_id = $request->input('tienda_id') ; 
            $almacen->tipo_almacen_id = $request->input('tipo_almacen_id'); 
            $almacen->distrito_id = $request->input('distrito_id'); 
            $almacen->codigo = $request->input('codigo');  
            $almacen->descripcion = $request->input('descripcion');   
            $almacen->direccion = $request->input('direccion');  
            $almacen->telefono = $request->input('telefono');  
            $almacen->glosa =  ($request->input('glosa') !=null)?$request->input('glosa'):'';
            $almacen->save();
             return response()->json([
            "res"=>"El almacen con id {$almacen->id} ha sido editado"
        ]);

        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
       
          
    }

    public function deleteAlmacen(Request $request){
         $almacen_id=$request['id'];
         $almacen= Almacen::find($almacen_id);
         $almacen->estado=$request->input('estado');
         $almacen->save();
           return response()->json([
            "res"=>"El almacenl con id {$almacen->id} ha sido eliminado"
        ]);
    }
}
