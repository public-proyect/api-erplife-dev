<?php

namespace App\Http\Controllers\Sucursales;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Models\Sucursales\Sucursal;
class SucursalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct(){
         $this->middleware(['jwt.auth']); 
     }

    public function getSurcusales(){
        return response()->json([
            "data"=>Sucursal::where([['sucursal.estado',1],['sucursal.per_id_padre',1]])
            ->select(['sucursal.*','departamento.descripcion as dep_des'])
            ->join('ubigeo as departamento',function($join){
                $join->on('departamento.id','=','departamento_id')                
                ->where('departamento.estado',1);
            })
            ->orderBy('sucursal.id','desc')->get()
            ]);    
    }

    public function postSucursal(Request $request){

        try{
            $validator=Validator::make($request->all(),[
                'departamento_id' => 'required|integer|regex:/^[1-9]+[0-9]*$/',
                'codigo'=>'required|max:10|regex:/^[a-zA-Z0-9_-]*$/',
                'nombre'=>'required|max:255|regex:/^[a-zA-Z0-9 _-]*$/',
                'descripcion'=>'required|max:255'
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
             $item_max=Sucursal::where('per_id_padre',1)->max('item');
             $item_max=(empty($item_max))?0:$item_max;
             $sucursal =new Sucursal();
             $sucursal->per_id_padre = 1 ;
             $sucursal->departamento_id = $request->input('departamento_id'); 
             $sucursal->item = $item_max+1; 
             $sucursal->codigo = $request->input('codigo');  
             $sucursal->nombre = $request->input('nombre');   
             $sucursal->descripcion = $request->input('descripcion');      
             $sucursal->save();
             return response()->json([
                "res"=>"La sucursal con id {$sucursal->id} ha sido guardado"
             ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
        
        
    } 

    public function getSucursalById($id){
        $sucursal= Sucursal::find($id);
        return response()->json([
            "data"=>$sucursal
        ]);
    }

    public function updateSucursal(Request $request){
        try{
            $validator=Validator::make($request->all(),[
                'id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'departamento_id' => 'required|integer|regex:/^[1-9]+[0-9]*$/',
                'codigo'=>'required|max:10|regex:/^[a-zA-Z0-9_-]*$/',
                'nombre'=>'required|max:255|regex:/^[a-zA-Z0-9 _-]*$/',
                'descripcion'=>'required|max:255'
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
                $sucursal_id=$request->input('id');
                $sucursal= Sucursal::find($sucursal_id);
                $sucursal->per_id_padre = 1 ;
                $sucursal->departamento_id = $request->input('departamento_id'); 
                $sucursal->codigo = $request->input('codigo');  
                $sucursal->nombre = $request->input('nombre');   
                $sucursal->descripcion = $request->input('descripcion');      
                $sucursal->save();
                return response()->json([
                    "res"=>"La sucursal con id {$sucursal->id} ha sido editado"
                ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
        
        
      
    }

    public function deleteSucursal(Request $request){
         $sucursal_id=$request->input('id');
         $sucursal= Sucursal::find($sucursal_id);
         $sucursal->estado=$request->input('estado');
         $sucursal->save();
           return response()->json([
            "res"=>"La sucursal con id {$sucursal->id} ha sido eliminado"
        ]);
    }
}
