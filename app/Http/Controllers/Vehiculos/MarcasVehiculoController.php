<?php

namespace App\Http\Controllers\Vehiculos;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Vehiculos\MarcaVehiculo;
class MarcasVehiculoController extends Controller
{
    public function __construct(){
         $this->middleware(['jwt.auth']); 
     }

    public function getMarcasVehiculo(){
        return response()->json([
            "data"=>MarcaVehiculo::orderBy('id','desc')->where([['estado',1],['per_id_padre',1]])->get()
            ]);
    }

    public function postMarcaVehiculo(Request $request){
        try{
            $validator=Validator::make($request->all(),[
                'descripcion'=>'required|max:255',
                'glosa'=>'max:255'
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
                $marcaVehiculo =new MarcaVehiculo();
                $marcaVehiculo->per_id_padre = 1 ; 
                $marcaVehiculo->descripcion =$request->input('descripcion'); 
                $marcaVehiculo->glosa =($request->input('glosa') !=null)?$request->input('glosa'):'';      
                $marcaVehiculo->save();
                return response()->json([
                    "res"=>"La marca del vehiculo con id {$marcaVehiculo->id} ha sido guardado"
                ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
         
    } 

    public function getMarcaVehiculoById($id){
        $marcaVehiculo= MarcaVehiculo::find($id);
        return response()->json([
            "data"=>$marcaVehiculo
        ]);
    }

    public function updateMarcaVehiculo(Request $request){
         try{
            $validator=Validator::make($request->all(),[
                'id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'descripcion'=>'required|max:255',
                'glosa'=>'max:255'
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
                $marca_vehiculo_id=$request->input('id');
                $marcaVehiculo= MarcaVehiculo::find($marca_vehiculo_id);
                $marcaVehiculo->descripcion =$request->input('descripcion'); 
                $marcaVehiculo->glosa =($request->input('glosa') !=null)?$request->input('glosa'):'';
                $marcaVehiculo->save();
                return response()->json([
                    "res"=>"La marca del vehiculo con id {$marca_vehiculo_id->id} ha sido editado"
                ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
         
    }

    public function deleteMarcaVehiculo(Request $request){
         $marca_vehiculo_id=$request->input('id');
         $marcaVehiculo= MarcaVehiculo::find($marca_vehiculo_id);
         $marcaVehiculo->estado=$request->input('estado');
         $marcaVehiculo->save();
           return response()->json([
            "res"=>"La marca de vehiculo con id {$marcaVehiculo->id} ha sido eliminado"
        ]);
    }
}
