<?php

namespace App\Http\Controllers\Vehiculos;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Models\Vehiculos\Vehiculo;

class VehiculosController extends Controller
{
   public function __construct(){
         $this->middleware(['jwt.auth']); 
     }

    public function getVehiculos(){
       return response()->json([
            "data"=>Vehiculo::with(['marcaVehiculo','tipoVehiculo.claseVehiculo'])
            ->where([['vehiculo.estado',1],['vehiculo.per_id_padre',1]])
            ->orderBy('vehiculo.id','desc')->get()
            ]);   
        
     
    }

    public function postVehiculo(Request $request){
        try{
            $validator=Validator::make($request->all(),[
                'tipo_vehiculo_id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'marca_vehiculo_id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'placa'=>'required|max:20|regex:/^[a-zA-Z0-9_-]*$/',
                'tarjeta_propiedad'=>'required|max:50|regex:/^[a-zA-Z0-9_-]*$/',
                'anio_fabricacion'=>'required|integer',
                'largo'=>'required|numeric',
                'ancho'=>'required|numeric',
                'alto'=>'required|numeric',
                'capacidad'=>'required|numeric',
                'glosa'=>'max:255'

            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
                $vehiculo =new Vehiculo();
                $vehiculo->per_id_padre = 1 ;
                $vehiculo->tipo_vehiculo_id = $request->input('tipo_vehiculo_id'); 
                $vehiculo->marca_vehiculo_id = $request->input('marca_vehiculo_id');  
                $vehiculo->placa = $request->input('placa');    
                $vehiculo->tarjeta_propiedad = $request->input('tarjeta_propiedad');    
                $vehiculo->anio_fabricacion = $request->input('anio_fabricacion');      
                $vehiculo->largo = $request->input('largo');    
                $vehiculo->ancho = $request->input('ancho'); 
                $vehiculo->alto = $request->input('alto');      
                $vehiculo->capacidad = $request->input('capacidad');             
                $vehiculo->glosa = ($request->input('glosa') !=null)?$request->input('glosa'):'';
                $vehiculo->save();
                return response()->json([
                    "res"=>"El vehiculo con id {$vehiculo->id} ha sido guardado"
                ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
         
    } 

    public function getVehiculoById($id){
        $vehiculo= Vehiculo::with('tipoVehiculo')->find($id);
        return response()->json([
            "data"=>$vehiculo
        ]);
    }

    public function updateVehiculo(Request $request){
        try{
            $validator=Validator::make($request->all(),[
                'id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'tipo_vehiculo_id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'marca_vehiculo_id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'placa'=>'required|max:20|regex:/^[a-zA-Z0-9_-]*$/',
                'tarjeta_propiedad'=>'required|max:50|regex:/^[a-zA-Z0-9_-]*$/',
                'anio_fabricacion'=>'required|integer',
                'largo'=>'required|numeric',
                'ancho'=>'required|numeric',
                'alto'=>'required|numeric',
                'capacidad'=>'required|numeric',
                'glosa'=>'max:255'

            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
                $vehiculo_id=$request->input('id');
                $vehiculo= Vehiculo::find($vehiculo_id);
                $vehiculo->per_id_padre = 1 ;
                $vehiculo->tipo_vehiculo_id = $request->input('tipo_vehiculo_id'); 
                $vehiculo->marca_vehiculo_id = $request->input('marca_vehiculo_id');  
                $vehiculo->placa = $request->input('placa');    
                $vehiculo->tarjeta_propiedad = $request->input('tarjeta_propiedad');    
                $vehiculo->anio_fabricacion = $request->input('anio_fabricacion');      
                $vehiculo->largo = $request->input('largo');    
                $vehiculo->ancho = $request->input('ancho'); 
                $vehiculo->alto = $request->input('alto');      
                $vehiculo->capacidad = $request->input('capacidad');             
                $vehiculo->glosa = ($request->input('glosa') !=null)?$request->input('glosa'):''; 
                $vehiculo->save();
                return response()->json([
                    "res"=>"El vehiculo con id {$vehiculo->id} ha sido editado"
                ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
         
    }

    public function deleteVehiculo(Request $request){
         $vehiculo_id=$request->input('id');
         $vehiculo= Vehiculo::find($vehiculo_id);
         $vehiculo->estado=$request->input('estado');
         $vehiculo->save();
           return response()->json([
            "res"=>"El vehiculo con id {$vehiculo->id} ha sido eliminado"
        ]);
    }
}
