<?php

namespace App\Http\Controllers\Vehiculos;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Vehiculos\TipoVehiculo;

class TiposVehiculoController extends Controller
{
    public function __construct(){
         $this->middleware(['jwt.auth']); 
     }

    public function getTiposVehiculo(){
       return response()->json([
            "data"=>TipoVehiculo::with(['claseVehiculo'])
            ->where([['tipo_vehiculo.estado',1],['tipo_vehiculo.per_id_padre',1]])
            ->orderBy('tipo_vehiculo.id','desc')->get()
            ]);   
        
     
    }

    public function postTipoVehiculo(Request $request){
        try{
            $validator=Validator::make($request->all(),[
                'clase_vehiculo_id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'descripcion'=>'required|max:255',
                'glosa'=>'max:255'
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
                $tipoVehiculo =new TipoVehiculo();
                $tipoVehiculo->per_id_padre = 1 ;
                $tipoVehiculo->clase_vehiculo_id = $request->input('clase_vehiculo_id') ;  
                $tipoVehiculo->descripcion = $request->input('descripcion');      
                $tipoVehiculo->glosa =($request->input('glosa') !=null)?$request->input('glosa'):''; 
                $tipoVehiculo->save();
                return response()->json([
                    "res"=>"El tipo de vehiculo con id {$tipoVehiculo->id} ha sido guardado"
                ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
        
    } 

    public function getTipoVehiculoById($id){
        $tipoVehiculo= TipoVehiculo::find($id);
        return response()->json([
            "data"=>$tipoVehiculo
        ]);
    }

    public function updateTipoVehiculo(Request $request){
        try{
            $validator=Validator::make($request->all(),[
                'id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'clase_vehiculo_id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'descripcion'=>'required|max:255',
                'glosa'=>'max:255'
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
                    $tipo_vehiculo_id=$request->input('id');
                    $tipoVehiculo= TipoVehiculo::find($tipo_vehiculo_id);
                    $tipoVehiculo->per_id_padre = 1 ;
                    $tipoVehiculo->clase_vehiculo_id = $request->input('clase_vehiculo_id') ;  
                    $tipoVehiculo->descripcion = $request->input('descripcion');      
                    $tipoVehiculo->glosa =($request->input('glosa') !=null)?$request->input('glosa'):''; 
                    $tipoVehiculo->save();
                    return response()->json([
                        "res"=>"El tipo de vehiculo con id {$tipoVehiculo->id} ha sido editado"
                    ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
         
    }

    public function deleteTipoVehiculo(Request $request){
         $tipo_vehiculo_id=$request['id'];
         $tipoVehiculo= TipoVehiculo::find($tipo_vehiculo_id);
         $tipoVehiculo->estado=$request['estado'];
         $tipoVehiculo->save();
           return response()->json([
            "res"=>"El tipo de vehiculo con id {$tipoVehiculo->id} ha sido eliminado"
        ]);
    }

    public function getTiposVehiculoById($id){
       return response()->json([
            "data"=>TipoVehiculo::where([['estado',1],['per_id_padre',1],
            ['clase_vehiculo_id',$id]])
            ->orderBy('id','desc')->get()
            ]);   
        
     
    }


}
