<?php

namespace App\Http\Controllers\Vehiculos;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Vehiculos\ClaseVehiculo;
class ClasesVehiculoController extends Controller
{
   public function __construct(){
         $this->middleware(['jwt.auth']); 
     }

    public function getClasesVehiculo(){
        return response()->json([
            "data"=>ClaseVehiculo::orderBy('id','desc')->where([['estado',1],['per_id_padre',1]])->get()
            ]);
    }

    public function postClaseVehiculo(Request $request){
        try{
            $validator=Validator::make($request->all(),[
                'descripcion'=>'required|max:255',
                'glosa'=>'max:255'
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
                $claseVehiculo =new ClaseVehiculo();
                $claseVehiculo->per_id_padre = 1 ; 
                $claseVehiculo->descripcion = $request->input('descripcion') ; 
                $claseVehiculo->glosa = ($request->input('glosa') !=null)?$request->input('glosa'):'';      
                $claseVehiculo->save();
                return response()->json([
                    "res"=>"La clase del vehiculo con id {$claseVehiculo->id} ha sido guardado"
                ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
         
    } 

    public function getClaseVehiculoById($id){
        $claseVehiculo= ClaseVehiculo::find($id);
        return response()->json([
            "data"=>$claseVehiculo
        ]);
    }

    public function updateClaseVehiculo(Request $request){
         try{
            $validator=Validator::make($request->all(),[
                'id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'descripcion'=>'required|max:255',
                'glosa'=>'max:255'
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
                $clase_vehiculo_id=$request->input('id');
                $claseVehiculo= ClaseVehiculo::find($clase_vehiculo_id);
                $claseVehiculo->descripcion = $request->input('descripcion') ; 
                $claseVehiculo->glosa = ($request->input('glosa') !=null)?$request->input('glosa'):''; 
                $claseVehiculo->save();
                return response()->json([
                    "res"=>"La clase del vehiculo con id {$claseVehiculo->id} ha sido editado"
                ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
         
    }

    public function deleteClaseVehiculo(Request $request){
         $clase_vehiculo_id=$request->input('id');
         $claseVehiculo= ClaseVehiculo::find($clase_vehiculo_id);
         $claseVehiculo->estado=$request->input('estado');
         $claseVehiculo->save();
           return response()->json([
            "res"=>"La clase de vehiculo con id {$claseVehiculo->id} ha sido eliminado"
        ]);
    }
}
