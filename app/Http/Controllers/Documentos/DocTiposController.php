<?php

namespace App\Http\Controllers\Documentos;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Documentos\DocTipo;

class DocTiposController extends Controller
{
    public function __construct(){
         $this->middleware(['jwt.auth']); 
     }

    public function getDocTipos(){
        return response()->json([
            "data"=>DocTipo::orderBy('id','desc')->where([['estado',1],['per_id_padre',1]])->get()
            ]);
    }

    public function postDocTipo(Request $request){
        try{
            $validator=Validator::make($request->all(),[
                'descripcion'=>'required|max:255',
                'glosa'=>'max:255'
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
            $docTipo =new DocTipo();
            $docTipo->per_id_padre = 1 ;         
            $docTipo->descripcion = $request->input('descripcion') ; 
            $docTipo->glosa = ($request->input('glosa') !=null)?$request->input('glosa'):'';    
            $docTipo->save();
            return response()->json([
                "res"=>"El tipo de documento con id {$docTipo->id} ha sido guardado"
            ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
         
         
    } 

    public function getDocTipoById($id){
        $docTipo= DocTipo::find($id);
        return response()->json([
            "data"=>$docTipo
        ]);
    }

    public function updateDocTipo(Request $request){
        try{
            $validator=Validator::make($request->all(),[
                'id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'descripcion'=>'required|max:255',
                'glosa'=>'max:255'
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
            $docTipo_id=$request->input('id');
            $docTipo= DocTipo::find($docTipo_id);
            $docTipo->descripcion = $request->input('descripcion') ; 
            $docTipo->glosa = ($request->input('glosa') !=null)?$request->input('glosa'):'';   ;    
            $docTipo->save();
            return response()->json([
                "res"=>"El tipo de documento con id {$docTipo->id} ha sido editado"
            ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
         
         
    }

    public function deleteDocTipo(Request $request){
         $docTipo_id=$request->input('id');
         $docTipo= DocTipo::find($docTipo_id);
         $docTipo->estado=$request->input('estado');
         $docTipo->save();
           return response()->json([
            "res"=>"El tipo de documento con id {$docTipo->id} ha sido eliminado"
        ]);
    }
}
