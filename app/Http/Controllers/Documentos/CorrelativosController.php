<?php

namespace App\Http\Controllers\Documentos;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Documentos\Correlativo;


class CorrelativosController extends Controller
{
    public function getCorrelativos(){
        return response()->json([
            "data"=>Correlativo::with('puntoEmision','tipoComprobante')->orderBy('id','desc')->where([['estado',1],['per_id_padre',1]])->get()
            ]);
    }

    public function postCorrelativo(Request $request){
         try{
            $validator=Validator::make($request->all(),[
                'tipo_comprobante_id' => 'required|integer|regex:/^[1-9]+[0-9]*$/',
                'punto_emision_id' => 'required|integer|regex:/^[1-9]+[0-9]*$/',
                'serie'=>'required|max:5|regex:/^[a-zA-Z0-9]*$/',
                'numero_ini'=>'required|max:10|regex:/^[0-9]*$/',
                'numero_fin'=>'required|max:10|regex:/^[0-9]*$/',
                'numero_actual'=>'required|max:10|regex:/^[0-9]*$/'
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
                $correlativo =new Correlativo();
                $correlativo->per_id_padre = 1 ; 
                $correlativo->tipo_comprobante_id = $request->input('tipo_comprobante_id') ; 
                $correlativo->punto_emision_id = $request->input('punto_emision_id') ; 
                $correlativo->serie = $request->input('serie')  ;      
                $correlativo->numero_ini = $request->input('numero_ini')  ;      
                $correlativo->numero_fin = $request->input('numero_fin')  ;      
                $correlativo->numero_actual = $request->input('numero_actual')  ;      
                $correlativo->save();
                return response()->json([
                    "res"=>"El correlativo con id {$correlativo->id} ha sido guardado"
                ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
         
        
    } 

    public function getCorrelativoById($id){
        $correlativo= Correlativo::find($id);
        return response()->json([
            "data"=>$correlativo
        ]);
    }

    public function updateCorrelativo(Request $request){
         try{
            $validator=Validator::make($request->all(),[
                'id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'tipo_comprobante_id' => 'required|integer|regex:/^[1-9]+[0-9]*$/',
                'punto_emision_id' => 'required|integer|regex:/^[1-9]+[0-9]*$/',
                'serie'=>'required|max:5|regex:/^[a-zA-Z0-9_-]*$/',
                'numero_ini'=>'required|max:10|regex:/^[0-9]*$/',
                'numero_fin'=>'required|max:10|regex:/^[0-9]*$/',
                'numero_actual'=>'required|max:10|regex:/^[0-9]*$/'
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
                $correlativo_id=$request->input('id');
                $correlativo= Correlativo::find($correlativo_id);
                $correlativo->per_id_padre = 1 ; 
                $correlativo->tipo_comprobante_id = $request->input('tipo_comprobante_id') ; 
                $correlativo->punto_emision_id = $request->input('punto_emision_id') ; 
                $correlativo->serie = $request->input('serie')  ;      
                $correlativo->numero_ini = $request->input('numero_ini')  ;      
                $correlativo->numero_fin = $request->input('numero_fin')  ;      
                $correlativo->numero_actual = $request->input('numero_actual')  ;       
                $correlativo->save();
                return response()->json([
                    "res"=>"El correlativo con id {$correlativo->id} ha sido editado"
                ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
         
    }

    public function deleteCorrelativo(Request $request){
         $correlativo_id=$request->input('id');
         $correlativo= Correlativo::find($correlativo_id);
         $correlativo->estado=$request->input('estado');
         $correlativo->save();
           return response()->json([
            "res"=>"El correlativo con id {$correlativo->id} ha sido eliminado"
        ]);
    }
}
