<?php

namespace App\Http\Controllers\Documentos;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Models\Documentos\TipoComprobante;

class TiposComprobanteController extends Controller
{
     public function __construct(){
         $this->middleware(['jwt.auth']); 
     }

    public function getTiposComprobante(){
        return response()->json([
            "data"=>TipoComprobante::orderBy('id','desc')->where([['estado',1],['per_id_padre',1]])->get()
            ]);
    }

    public function postTipoComprobante(Request $request){
         try{
            $validator=Validator::make($request->all(),[
                'descripcion'=>'required|max:255',
                'numero'=>'required|max:20|regex:/^[0-9]*$/'
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
                $tipoComprobante =new TipoComprobante();
                $tipoComprobante->per_id_padre = 1 ; 
                $tipoComprobante->descripcion = $request->input('descripcion') ; 
                $tipoComprobante->numero = $request->input('numero') ;      
                $tipoComprobante->save();
                return response()->json([
                    "res"=>"El tipo comprobante con id {$tipoComprobante->id} ha sido guardado"
                ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
         
    } 

    public function getTipoComprobanteById($id){
        $tipoComprobante= TipoComprobante::find($id);
        return response()->json([
            "data"=>$tipoComprobante
        ]);
    }

    public function updateTipoComprobante(Request $request){
        try{
            $validator=Validator::make($request->all(),[
                'id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'descripcion'=>'required|max:255',
                'numero'=>'required|max:20|regex:/^[0-9]*$/'
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
                $tipoComprobante_id=$request->input('id');
                $tipoComprobante= TipoComprobante::find($tipoComprobante_id);        
                $tipoComprobante->descripcion = $request->input('descripcion') ; 
                $tipoComprobante->numero = $request->input('numero')  ;      
                $tipoComprobante->save();
                return response()->json([
                    "res"=>"El tipo de comprobante con id {$tipoComprobante->id} ha sido editado"
                ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
         
    }

    public function deleteTipoComprobante(Request $request){
         $tipoComprobante_id=$request->input('id');
         $tipoComprobante= TipoComprobante::find($tipoComprobante_id);
         $tipoComprobante->estado=$request->input('estado');
         $tipoComprobante->save();
           return response()->json([
            "res"=>"El tipo de comprobante con id {$tipoComprobante->id} ha sido eliminado"
        ]);
    }
}
