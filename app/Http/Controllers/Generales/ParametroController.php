<?php

namespace App\Http\Controllers\Generales;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Generales\Parametro;
use JWTAuth;

class ParametroController extends Controller
{
    public function __construct(JWTAuth $auth)
    {
        $this->middleware('jwt.auth');
    }

    public function getParametros(Request $request)
	{
        $per_id_padre = 1;
        $par_clase = $request->input('par_clase');

        $data = Parametro::where([['estado','=',1],['per_id_padre','=',$per_id_padre],['par_clase','=',$par_clase]])->get() ;

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function save(Request $request)
    {

        $per_id_padre = 1;

        $par_codigo = $request->input('par_codigo') ;
        $par_clase = $request->input('par_clase') ;
        $par_descripcion = $request->input('par_descripcion') ;
        $par_glosa = $request->input('par_glosa') ;

    	$rules = [
            	'par_descripcion'      => 'par_descripcion',
            ];

        try 
        {

	        $parametro= Parametro::where(['par_descripcion' => $par_descripcion])->first();

	        if (!$parametro)
	        {
	        	$parametro = new Parametro() ;
                $parametro->per_id_padre = $per_id_padre ;
                $parametro->par_codigo = $par_codigo ;
                $parametro->par_clase = $par_clase ;
                $parametro->par_descripcion = $par_descripcion ;
                $parametro->par_glosa = $par_glosa ;
	        	$parametro->save() ;
	        }
	        else
	        {
	        	$parametro->estado = 1 ;
	        	$parametro->save() ;
	        }

	        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => "OK",
	                    ]);

        } 
        catch (Exception $e)
        {
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }
    }

	public function getParametroById($id)
    {

        $parametro_id = $id ;

        $parametro = Parametro::where('id',$parametro_id)->first() ;

        return response()->json([
                	'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $parametro,
                ]);
    }

	public function update(Request $request)
    {

        $parametro_id = $request->input('id') ;
        $par_descripcion = $request->input('par_descripcion') ;
        $par_glosa = $request->input('par_glosa') ;

        $parametro =  Parametro::find($parametro_id) ;
		$parametro->par_descripcion = $par_descripcion ;
		$parametro->par_glosa = $par_glosa ;
        $parametro->save() ;

        $data =  Parametro::find($parametro_id) ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function updateEstado(Request $request)
    {

		$parametro_id = $request->input('id') ;
		$estado = $request->input('estado') ;

        $parametro =  Parametro::find($parametro_id) ;
        $parametro->estado = $estado ;
        $parametro->save() ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $estado,
                ]);
    }
}
