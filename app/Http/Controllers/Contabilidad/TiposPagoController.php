<?php

namespace App\Http\Controllers\Contabilidad;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Models\Contabilidad\TipoPago;

class TiposPagoController extends Controller
{
      public function __construct(){
         $this->middleware(['jwt.auth']); 
     }

    public function getTiposPago(){
        return response()->json([
            "data"=>TipoPago::where([['estado',1]])
            ->orderBy('id','desc')->get()
            ]);
     
    }

    public function postTipoPago(Request $request){
         try{
            $validator=Validator::make($request->all(),[
                'descripcion' => 'required|max:255',
                'glosa' => 'max:255',
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
                $tipoPago =new TipoPago();         
                $tipoPago->descripcion = $request->input('descripcion'); 
                $tipoPago->glosa = ($request->input('glosa') !=null)?$request->input('glosa'):''; 
                $tipoPago->save();
                return response()->json([
                    "res"=>"El tipo de pago con id {$tipoPago->id} ha sido guardado"
                ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
         
    } 

    public function getTipoPagoById($id){
        $tipoPago= TipoPago::find($id);
        return response()->json([
            "data"=>$tipoPago
        ]);
    }

    public function updateTipoPago(Request $request){
         try{
            $validator=Validator::make($request->all(),[
                'id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'descripcion' => 'required|max:255',
                'glosa' => 'max:255',
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
                    $tipoPago_id=$request->input('id');
                    $tipoPago= TipoPago::find($tipoPago_id);
                    $tipoPago->descripcion = $request->input('descripcion'); 
                    $tipoPago->glosa = ($request->input('glosa') !=null)?$request->input('glosa'):''; 
                    $tipoPago->save();
                    return response()->json([
                        "res"=>"El tipo de pago con id {$tipoPago->id} ha sido editado"
                    ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
        
    }

    public function deleteTipoPago(Request $request){
         $tipoPago_id=$request->input('id');
         $tipoPago= TipoPago::find($tipoPago_id);
         $tipoPago->estado=$request->input('estado');
         $tipoPago->save();
           return response()->json([
            "res"=>"El tipo de pago con id {$tipoPago->id} ha sido eliminado"
        ]);
    }
}
