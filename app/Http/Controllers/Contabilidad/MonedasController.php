<?php

namespace App\Http\Controllers\Contabilidad;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Models\Contabilidad\Moneda;

class MonedasController extends Controller
{
    public function __construct(){
         $this->middleware(['jwt.auth']); 
     }

    public function getMonedas(){
        return response()->json([
            "data"=>Moneda::orderBy('id','desc')->where([['estado',1],['per_id_padre',1]])->get()
            ]);
    }

    public function postMoneda(Request $request){
        try{
            $validator=Validator::make($request->all(),[
                'simbolo' => 'required|max:10',
                'descripcion' => 'required|max:255',
                'base'=>'required|integer'
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
              $moneda =new Moneda();
              $moneda->per_id_padre = 1 ; 
              $moneda->simbolo = $request->input('simbolo') ; 
              $moneda->descripcion = $request->input('descripcion') ; 
              $moneda->base = $request->input('base')  ;      
              $moneda->save();
              return response()->json([
            "res"=>"La moneda con id {$moneda->id} ha sido guardado"
              ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
         
    } 

    public function getMonedaById($id){
        $moneda= Moneda::find($id);
        return response()->json([
            "data"=>$moneda
        ]);
    }

    public function updateMoneda(Request $request){
        try{
            $validator=Validator::make($request->all(),[
                'id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'simbolo' => 'required|max:10',
                'descripcion' => 'required|max:255',
                'base'=>'required|integer'
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
                $moneda_id=$request->input('id');
                $moneda= Moneda::find($moneda_id);
                $moneda->simbolo = $request->input('simbolo') ; 
                $moneda->descripcion = $request->input('descripcion') ; 
                $moneda->base = $request->input('base')  ;      
                $moneda->save();
                return response()->json([
                    "res"=>"La moneda con id {$moneda->id} ha sido editado"
                ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
         
    }

    public function deleteMoneda(Request $request){
         $moneda_id=$request->input('id');
         $moneda= Moneda::find($moneda_id);
         $moneda->estado=$request->input('estado');
         $moneda->save();
           return response()->json([
            "res"=>"La moneda con id {$moneda->id} ha sido eliminado"
        ]);
    }
}
