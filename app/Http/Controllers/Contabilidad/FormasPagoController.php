<?php

namespace App\Http\Controllers\Contabilidad;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Models\Contabilidad\FormaPago;
class FormasPagoController extends Controller
{
   public function __construct(){
         $this->middleware(['jwt.auth']); 
     }

    public function getFormasPago(){
        return response()->json([
            "data"=>FormaPago::with('TipoPago')->where([['forma_pago.estado',1]])
            ->orderBy('forma_pago.id','desc')->get()
            ]);
     
    }

    public function postFormaPago(Request $request){
        try{
            $validator=Validator::make($request->all(),[
                'tipo_pago_id' => 'required|integer|regex:/^[1-9]+[0-9]*$/',
                'descripcion' => 'required|max:255',
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
             $formaPago =new FormaPago();         
             $formaPago->tipo_pago_id = $request->input('tipo_pago_id') ; 
             $formaPago->descripcion = $request->input('descripcion'); 
             $formaPago->save();
            return response()->json([
            "res"=>"La forma de pago con id {$formaPago->id} ha sido guardado"
            ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
         
         
    } 

    public function getFormaPagoById($id){
        $formaPago= FormaPago::find($id);
        return response()->json([
            "data"=>$formaPago
        ]);
    }

    public function updateFormaPago(Request $request){
        try{
            $validator=Validator::make($request->all(),[
                'id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'tipo_pago_id' => 'required|integer|regex:/^[1-9]+[0-9]*$/',
                'descripcion' => 'required|max:255',
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
            $formaPago_id=$request->input('id');
            $formaPago= FormaPago::find($formaPago_id);
            $formaPago->tipo_pago_id = $request->input('tipo_pago_id') ; 
            $formaPago->descripcion = $request->input('descripcion') ; 
            $formaPago->save();
            return response()->json([
            "res"=>"La forma de pago con id {$formaPago->id} ha sido editado"
        ]);
            return response()->json([
            "res"=>"La forma de pago con id {$formaPago->id} ha sido guardado"
            ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
         
         
    }

    public function deleteFormaPago(Request $request){
         $formaPago_id=$request->input('id');
         $formaPago= FormaPago::find($formaPago_id);
         $formaPago->estado=$request->input('estado');
         $formaPago->save();
           return response()->json([
            "res"=>"La forma de pago con id {$formaPago->id} ha sido eliminado"
        ]);
    }
}
