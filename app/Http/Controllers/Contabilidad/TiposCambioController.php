<?php

namespace App\Http\Controllers\Contabilidad;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Models\Contabilidad\TipoCambio;

class TiposCambioController extends Controller
{
    
     public function __construct(){
         $this->middleware(['jwt.auth']); 
     }

    public function getTiposCambio(){
        return response()->json([
            "data"=>TipoCambio::with('moneda')->where([['tipo_cambio.estado',1]])
            ->orderBy('tipo_cambio.id','desc')->get()
            ]);
     
    }

    public function postTipoCambio(Request $request){
        try{
            $validator=Validator::make($request->all(),[
                'moneda_id' => 'required|integer|regex:/^[1-9]+[0-9]*$/',
                'fecha' => 'required',
                'compra'=>'required|numeric',
                'venta'=>'required|numeric'
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
                $tipoCambio =new TipoCambio();         
                $tipoCambio->moneda_id = $request->input('moneda_id') ; 
                $tipoCambio->fecha = $request->input('fecha') ; 
                $tipoCambio->compra = $request->input('compra');  
                $tipoCambio->venta = $request->input('venta');  
                $tipoCambio->save();
                return response()->json([
                    "res"=>"El tipo de cambio con id {$sucursal->id} ha sido guardado"
                ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
        
    } 

    public function getTipoCambioById($id){
        $tipoCambio= TipoCambio::find($id);
        return response()->json([
            "data"=>$tipoCambio
        ]);
    }

    public function updateTipoCambio(Request $request){
        try{
            $validator=Validator::make($request->all(),[
                'id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'moneda_id' => 'required|integer|regex:/^[1-9]+[0-9]*$/',
                'fecha' => 'required',
                'compra'=>'required|numeric',
                'venta'=>'required|numeric'
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
                $tipoCambio_id=$request->input('id');
                $tipoCambio= TipoCambio::find($tipoCambio_id);
                $tipoCambio->moneda_id = $request->input('moneda_id') ; 
                $tipoCambio->fecha = $request->input('fecha') ; 
                $tipoCambio->compra = $request->input('compra');  
                $tipoCambio->venta = $request->input('venta');  
                $tipoCambio->save();
                return response()->json([
                    "res"=>"El tipo de cambio con id {$tipoCambio->id} ha sido editado"
                ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }   
         
    }

    public function deleteTipoCambio(Request $request){
         $tipoCambio_id=$request->input('id');
         $tipoCambio= TipoCambio::find($tipoCambio_id);
         $tipoCambio->estado=$request->input('estado');
         $tipoCambio->save();
           return response()->json([
            "res"=>"El tipo de cambio con id {$tipoCambio->id} ha sido eliminado"
        ]);
    }
}
