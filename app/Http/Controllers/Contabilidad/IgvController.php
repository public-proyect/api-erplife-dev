<?php

namespace App\Http\Controllers\Contabilidad;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Models\Contabilidad\Igv;

class IgvController extends Controller
{
    public function __construct(){
         $this->middleware(['jwt.auth']); 
     }

    public function getIgvs(){
        return response()->json([
            "data"=>Igv::orderBy('id','desc')->where([['estado',1]])->get()
            ]);
    }

    public function postIgv(Request $request){
        try{
            $validator=Validator::make($request->all(),[
                'valor' => 'required|numeric',
                'glosa' => 'max:255',
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
              $igv =new Igv();
              $igv->valor = $request->input('valor') ; 
              $igv->glosa =($request->input('glosa') !=null)?$request->input('glosa'):'';  
              $igv->save();
              return response()->json([
                "res"=>"El igv con id {$igv->id} ha sido guardado"
              ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
        
    } 

    public function getIgvById($id){
        $igv= Igv::find($id);
        return response()->json([
            "data"=>$igv
        ]);
    }

    public function updateIgv(Request $request){
         try{
            $validator=Validator::make($request->all(),[
                'id'=>'required|integer|regex:/^[1-9]+[0-9]*$/',
                'valor' => 'required|numeric',
                'glosa' => 'max:255',
            ]);
            if($validator->fails()){
                  $errors = (array)$validator->errors()->toArray();
                return response()->json([
                    'message'  => 'Operación Fallida',
                    'error' => true,
                    'errors' => $errors 
                ]);               
            }
              $igv_id=$request->input('id');
              $igv= Igv::find($igv_id);
              $igv->valor = $request->input('valor') ; 
              $igv->glosa = ($request->input('glosa') !=null)?$request->input('glosa'):''; 
              $igv->save();
              return response()->json([
            "res"=>"El igv con id {$igv->id} ha sido editado"
             ]);
        }catch(Exception $e){
            return response()->json([
                    'res'  => 'Operación Exception',
					'error' => true,
					'errors' => array('message'=> $e->getMessage()) ,
				    'data'   => "OK",
                ]);
        }     
        
    }

    public function deleteIgv(Request $request){
         $igv_id=$request->input('id');
         $igv= Igv::find($igv_id);
         $igv->estado=$request->input('estado');
         $igv->save();
           return response()->json([
            "res"=>"El igv con id {$igv->id} ha sido eliminado"
        ]);
    }
}
