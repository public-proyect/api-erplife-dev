<?php

namespace App\Http\Controllers\Empleados;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Empleados\SubArea;
use JWTAuth;

class SubAreaController extends Controller
{
    public function __construct(JWTAuth $auth)
    {
        $this->middleware('jwt.auth');
    }

    public function getSubAreas()
	{
        $data = SubArea::select('sub_area.*','area.descripcion AS area')
                ->join('area', 'area.id', '=', 'sub_area.area_id')
                ->where('sub_area.estado',1)->get() ;

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function getSubAreasByAreaId(Request $request)
	{ 
        $id_area = $request["id_area"];

        $data = SubArea::select('sub_area.*')
                ->join('area', 'area.id', '=', 'sub_area.area_id')
                ->where('sub_area.area_id', '=' , $id_area)
                ->where('sub_area.estado', '=' , 1)
                ->get() ;

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function save(Request $request)
    {

        $per_id_padre = 1;

        $area_id = $request->input('area_id');
        $descripcion = $request->input('descripcion');
        $glosa = $request->input('glosa');

        try 
        {

	        $sub_area = SubArea::where(['descripcion' => $descripcion])->first();

	        if (!$sub_area)
	        {
	        	$sub_area = new SubArea() ;
                $sub_area->area_id = $area_id ;
                $sub_area->descripcion = $descripcion ;
                $sub_area->glosa = $glosa ;
                $sub_area->per_id_padre = $per_id_padre ;
	        	$sub_area->save() ;
	        }
	        else
	        {
                $sub_area->area_id = $area_id ;
                $sub_area->descripcion = $descripcion ;
                $sub_area->glosa = $glosa ;
                $sub_area->per_id_padre = $per_id_padre ;
                $sub_area->estado = 1 ;
	        	$sub_area->save() ;
	        }

	        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => "OK",
	                    ]);

        } 
        catch (Exception $e)
        {
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }
    }

	public function getSubAreaById($id)
    {

        $sub_area_id = $id ;

        $data = SubArea::where('id',$sub_area_id)->first() ;

        return response()->json([
                	'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function update(Request $request)
    {

        $sub_area_id = $request->input('id') ;
        $area_id = $request->input('area_id');
        $descripcion = $request->input('descripcion');
        $glosa = $request->input('glosa');
        
        $sub_area =  SubArea::find($sub_area_id) ;
        $sub_area->area_id = $area_id ;
		$sub_area->descripcion = $descripcion ;
		$sub_area->glosa = $glosa ;
        $sub_area->save() ;

        $data =  SubArea::find($sub_area_id) ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function updateEstado(Request $request)
    {

		$sub_area_id = $request->input('id') ;
		$estado = $request->input('estado') ;

        $sub_area =  SubArea::find($sub_area_id) ;
        $sub_area->estado = $estado ;
        $sub_area->save() ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $estado,
                ]);
    }
}
