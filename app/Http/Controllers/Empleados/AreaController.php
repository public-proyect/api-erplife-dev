<?php

namespace App\Http\Controllers\Empleados;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Empleados\Area;
use JWTAuth;


class AreaController extends Controller
{
    public function __construct(JWTAuth $auth)
    {
        $this->middleware('jwt.auth');
    }

    public function getAreas()
	{
        $data = Area::where('estado',1) 
                    ->get() ;

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function save(Request $request)
    {

        $per_id_padre = 1;

        $descripcion      = $request->input('descripcion') ;
        $glosa = $request->input('glosa') ;

    	$rules = [
            	'descripcion'      => 'descripcion',
            ];

        try 
        {

	        $area = Area::where(['descripcion' => $descripcion])->first();

	        if (!$area)
	        {
	        	$area = new Area() ;
                $area->descripcion = $descripcion ;
                $area->glosa = $glosa ;
                $area->per_id_padre = $per_id_padre ;
	        	$area->save() ;
	        }
	        else
	        {
	        	$area->estado = 1 ;
	        	$area->save() ;
	        }

	        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => "OK",
	                    ]);

        } 
        catch (Exception $e)
        {
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }
    }

	public function getAreaById($id)
    {

        $area_id = $id ;

        $data = Area::where('id',$area_id)->first() ;

        return response()->json([
                	'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function update(Request $request)
    {

        $area_id = $request->input('id') ;
        $descripcion = $request->input('descripcion') ;
        $glosa = $request->input('glosa') ;

        $area =  Area::find($area_id) ;
		$area->descripcion = $descripcion ;
		$area->glosa = $glosa ;
        $area->save() ;

        $data =  Area::find($area_id) ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function updateEstado(Request $request)
    {

		$area_id = $request->input('id') ;
		$estado = $request->input('estado') ;

        $area =  Area::find($area_id) ;
        $area->estado = $estado ;
        $area->save() ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $estado,
                ]);
    }
}
