<?php

namespace App\Http\Controllers\Empleados;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Empleados\Empleado;
use App\Models\Empleados\CargoEmpleado;
use App\Models\Empleados\Cargo;
use App\Models\Empleados\SubArea;
use App\Models\Personas\Persona;
use App\Models\Personas\PerNatural;
use App\Models\Personas\PerImagen;
use App\Models\Personas\PerDireccion;
use App\Models\Personas\PerMail;
use App\Models\Personas\PerTelefono;
use App\Models\Personas\PerWeb;
use App\Models\Personas\PerDocIdentidad;
use JWTAuth;

class EmpleadoController extends Controller
{
    public function __construct(JWTAuth $auth)
    {
        $this->middleware('jwt.auth', ['except' => ['uploadImagen']]);
    }

    public function getEmpleados()
	{
        $data = Empleado::where('empleado.estado',1)
                ->join('cargo_empleado', function($join) {
					    $join->on('cargo_empleado.empleado_id','=', 'empleado.id')
						->where('cargo_empleado.estado','=',1);
				})
                ->join('cargo', function($join) {
						$join->on('cargo.id','=', 'cargo_empleado.cargo_id')
						->where('cargo.estado','=',1);
				})
                ->join('persona', function($join) {
						$join->on('persona.id','=', 'empleado.persona_id')
						->where('persona.estado','=',1);
				})
                ->select([
    					'empleado.id',
    					'empleado.codigo',
    					'persona.per_nombre as nombre',
    					'persona.per_apellidos as apellidos',
                        'cargo_empleado.fecha_ini as fecha_ini',
                        'cargo_empleado.fecha_fin as fecha_fin',
    					'cargo.descripcion as cargo',
    					 ])
    					->get();

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function uploadImagen(Request $request){

        $file=$request->file('imagen-empleado');
        foreach($file as $image){
            $name='empleado_'.time().rand().'.'.$image->getClientOriginalExtension();
            $image->storeAs('empleados',"{$name}",'asset');       
        }
        return \Response::Json(['status' => 'success',
                            'error'   => false,
                            'filename'    => $name],200);  
    } 

    public function save(Request $request)
    {
        $per_id_padre = 1;

        $direcciones = json_decode($request->input('direcciones'),true);
        $correos = json_decode($request->input('correos'),true);
        $telefonos = json_decode($request->input('telefonos'),true);
        $webs = json_decode($request->input('webs'),true);
        $documentos_identidad = json_decode($request->input('documentos_identidad'),true);
        
        $per_nombre = $request->input('per_nombre');
        $per_apellidos = $request->input('per_apellidos');
        $per_fecha_nac = $request->input('per_fecha_nac');
        $dni = $request->input('dni');
        $ruc = $request->input('ruc');
        $sexo = $request->input('sexo');
        $estado_civil = $request->input('estado_civil');
        $cargo_id = $request->input('cargo_id');
        $fecha_ini = $request->input('fecha_ini');
        $fecha_fin = $request->input('fecha_fin');
        $url = $request->input('url');
        $tipo = $request->input('tipo');

        try 
        {

	        $persona = Persona::where(['per_nombre' => $per_nombre])->first();

	        if (!$persona)
	        {
                \DB::beginTransaction();

	        	$persona = new Persona() ;
                $persona->per_nombre = $per_nombre ;
                $persona->per_apellidos = $per_apellidos ;
                $persona->per_fecha_nac = $per_fecha_nac ;
                $persona->per_id_padre = $per_id_padre ;
	        	$persona->save() ;

                $persona_id =  $persona->id ;

                $per_natural = new PerNatural() ;
                $per_natural->persona_id = $persona_id ;
                $per_natural->dni = $dni ;
                $per_natural->ruc = $ruc ;
                $per_natural->apellidos = $per_apellidos ;
                $per_natural->nombres = $per_nombre ;
                $per_natural->sexo = $sexo ;
                $per_natural->estado_civil = $estado_civil ;
	        	$per_natural->save() ;

                $per_imagen = new PerImagen() ;
                $per_imagen->persona_id = $persona_id ;
                $per_imagen->url = $url ;
                $per_imagen->tipo = $tipo ;
	        	$per_imagen->save() ;

                $empleado = new Empleado() ;
                $empleado->persona_id = $persona_id ;
                $empleado->per_id_padre = $per_id_padre ;
                $empleado->codigo = $persona_id+1;
	        	$empleado->save() ;

                $empleado_id =  $empleado->id ;

                $cargo_empleado = new CargoEmpleado() ;
                $cargo_empleado->cargo_id = $cargo_id ;
                $cargo_empleado->empleado_id = $empleado_id ;
                $cargo_empleado->fecha_ini = $fecha_ini ;
                $cargo_empleado->fecha_fin = $fecha_fin ;
	        	$cargo_empleado->save() ;

                $per_doc_identidad = new PerDocIdentidad() ;
                $per_doc_identidad->persona_id = $persona_id ;
                $per_doc_identidad->tipo_per_doc_identidad_id = 1 ;
                $per_doc_identidad->numero = $dni ;
                $per_doc_identidad->fecha_caducidad = null ;
                $per_doc_identidad->fecha_emision = null ;
                $per_doc_identidad->save() ;

                if($ruc){
                    $per_doc_identidad = new PerDocIdentidad() ;
                    $per_doc_identidad->persona_id = $persona_id ;
                    $per_doc_identidad->tipo_per_doc_identidad_id = 2 ;
                    $per_doc_identidad->numero = $ruc ;
                    $per_doc_identidad->fecha_caducidad = null ;
                    $per_doc_identidad->fecha_emision = null ;
                    $per_doc_identidad->save() ;      
                }

                $direcciones = $direcciones["direcciones"];
                for($i=0; $i< count($direcciones); $i++ ){
                    $per_direccion = new PerDireccion() ;
                    $per_direccion->persona_id = $persona_id ;
                    $per_direccion->tipo_direccion_id = $direcciones[$i]['tipo_direccion_id'] ;
                    $per_direccion->ubigeo_id = $direcciones[$i]['ubigeo']['id'] ;
                    $per_direccion->direccion = $direcciones[$i]['direccion'] ;
                    $per_direccion->referencia = $direcciones[$i]['referencia'] ;
                    $per_direccion->save() ; 
                }

                $correos = $correos["correos"];
                for($i=0; $i< count($correos); $i++ ){ 
                    $per_email = new PerMail() ;
                    $per_email->persona_id = $persona_id ;
                    $per_email->mail = $correos[$i]['mail'] ;
                    $per_email->save() ; 
                }

                $telefonos = $telefonos["telefonos"];
                for($i=0; $i< count($telefonos); $i++ ){
                    $per_telefono = new PerTelefono() ;
                    $per_telefono->persona_id = $persona_id ;
                    $per_telefono->tipo_telefono_id = $telefonos[$i]['tipo_telefono_id'] ;
                    $per_telefono->telefono = $telefonos[$i]['telefono'] ;
                    $per_telefono->item = 1 ;
                    $per_telefono->save() ;
                }

                $webs = $webs["webs"];
                for($i=0; $i< count($webs); $i++ ){
                    $per_web = new PerWeb() ;
                    $per_web->persona_id = $persona_id ;
                    $per_web->tipo_web_id = $webs[$i]['tipo_web_id'] ;
                    $per_web->url = $webs[$i]['url'] ;
                    $per_web->save() ;
                }

                $documentos_identidad = $documentos_identidad["documentos_identidad"];
                for($i=0; $i< count($documentos_identidad); $i++ ){
                    $per_doc_identidad = new PerDocIdentidad() ;
                    $per_doc_identidad->persona_id = $persona_id ;
                    $per_doc_identidad->tipo_per_doc_identidad_id = $documentos_identidad[$i]['tipo_per_doc_identidad_id'] ;
                    $per_doc_identidad->numero = $documentos_identidad[$i]['numero'] ;
                    $per_doc_identidad->fecha_caducidad = (isset($documentos_identidad[$i]['fecha_caducidad']['formatted'])) ? $documentos_identidad[$i]['fecha_caducidad']['formatted'] : null ;
                    $per_doc_identidad->fecha_emision = (isset($documentos_identidad[$i]['fecha_emision']['formatted'])) ? $documentos_identidad[$i]['fecha_emision']['formatted'] : null ;
                    $per_doc_identidad->save() ;
                }

                \DB::commit();
	        }
	        else
	        {
                return \Response::json([
							'message' => 'DNI, se encuentra registrada',
							'error'   => true,
							'errors'  => [] ,
							'data'    => [],
		                ]);
	        }

	        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => "OK",
	                    ]);

        } 
        catch (Exception $e)
        {
            \DB::rollback();

            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }
    }

	public function getEmpleadoById($id)
    {

        $empleado_id = $id ;

        $empleado = Empleado::where('id',$empleado_id)->first() ;
        $cargo_empleado = CargoEmpleado::where([['empleado_id',$empleado_id],['estado',1] ])->first() ;
        $cargo = Cargo::where([['id',$cargo_empleado->cargo_id],['estado',1]])->first();
        $sub_area = SubArea::where([['id',$cargo->sub_area_id],['estado',1]])->first();
        $persona = Persona::where([['id',$empleado->persona_id],['estado',1] ])->first() ;
        $per_natural = PerNatural::where([['persona_id',$empleado->persona_id],['estado',1] ])->first() ;
        $per_imagen = PerImagen::where([['persona_id',$empleado->persona_id],['estado',1] ])->first() ;
        $per_doc_identidad = PerDocIdentidad::where([['persona_id',$empleado->persona_id],['estado',1] ])->get() ;
        $per_mail = PerMail::where([['persona_id',$empleado->persona_id],['estado',1] ])->get() ;
        $per_direccion = PerDireccion::where([['persona_id',$empleado->persona_id],['estado',1] ])->get() ;
        $per_telefono = PerTelefono::where([['persona_id',$empleado->persona_id],['estado',1] ])->get() ;
        $per_web = PerWeb::where([['persona_id',$empleado->persona_id],['estado',1] ])->get() ;

        $data = ['empleado' => $empleado, 'sub_area' => $sub_area, 'cargo_empleado' => $cargo_empleado, 'persona' => $persona, 
                    'per_natural' => $per_natural, 'per_imagen' => $per_imagen, 'per_doc_identidad' => $per_doc_identidad, 
                    'per_mail' => $per_mail, 'per_direccion' => $per_direccion,'per_telefono' => $per_telefono, 
                    'per_web' => $per_web  ];

        return response()->json([
                	'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function updateInformacion(Request $request)
    {
        $input_persona = json_decode($request->input('persona'),true);
        $input_per_natural = json_decode($request->input('per_natural'),true);
        $input_per_imagen = json_decode($request->input('per_imagen'),true);
        $input_cargo_empleado = json_decode($request->input('cargo_empleado'),true);
        try 
        {
            \DB::beginTransaction();

            $persona =  Persona::find($input_persona['id']) ;
            $persona->per_nombre = $input_persona['per_nombre'] ;
            $persona->per_apellidos = $input_persona['per_apellidos'] ;
            $persona->per_fecha_nac = $input_persona['per_fecha_nac']; 
            $persona->save() ;

            $per_natural =  PerNatural::find($input_per_natural['id']) ;
            $per_natural->dni = $input_per_natural['dni'] ;
            $per_natural->ruc = $input_per_natural['ruc'] ;
            $per_natural->apellidos = $input_persona['per_apellidos'];
            $per_natural->nombres = $input_persona['per_nombre'];
            $per_natural->sexo = $input_per_natural['sexo'] ;
            $per_natural->estado_civil = $input_per_natural['estado_civil'] ;
            $per_natural->save() ;

            $per_imagen =  PerImagen::find($input_per_imagen['id']) ;
            $per_imagen->url = $input_per_imagen['url'] ;
            $per_imagen->save() ;

            $cargo_empleado =  CargoEmpleado::find($input_cargo_empleado['id']) ;
            $cargo_empleado->cargo_id = $input_cargo_empleado['cargo_id'] ;
            $cargo_empleado->fecha_ini = $input_cargo_empleado['fecha_ini'] ;
            $cargo_empleado->fecha_fin = $input_cargo_empleado['fecha_fin'] ;
            $cargo_empleado->save() ;

            \DB::commit();
        }  
        catch (Exception $e)
        {
            \DB::rollback();
            
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }  

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => 'OK',
        ]);
    }

    public function updateDirecciones(Request $request)
    {
        $direcciones = json_decode($request->input('direcciones'),true);
        $persona = json_decode($request->input('persona'),true);
        try 
        {
            \DB::beginTransaction();

            PerDireccion::where([['estado', '=', 1],['persona_id', '=', $persona['id']]])->update(['estado' => 0]);

            $direcciones = $direcciones["direcciones"];
            for($i=0; $i< count($direcciones); $i++ )
            {
                if($direcciones[$i]['id']!=null)
                {
                    $per_direccion = PerDireccion::find($direcciones[$i]['id']) ;
                    $per_direccion->tipo_direccion_id = $direcciones[$i]['tipo_direccion_id'] ;
                    $per_direccion->ubigeo_id = (isset($direcciones[$i]['ubigeo']['id'])) ? $direcciones[$i]['ubigeo']['id'] : $per_direccion->ubigeo_id;
                    $per_direccion->direccion = $direcciones[$i]['direccion'] ;
                    $per_direccion->referencia = $direcciones[$i]['referencia'] ;
                    $per_direccion->estado = 1;
                    $per_direccion->save() ;
                }
                else
                {
                    $per_direccion = new PerDireccion() ;
                    $per_direccion->persona_id = $persona['id'] ;
                    $per_direccion->tipo_direccion_id = $direcciones[$i]['tipo_direccion_id'] ;
                    $per_direccion->ubigeo_id = $direcciones[$i]['ubigeo']['id'] ;
                    $per_direccion->direccion = $direcciones[$i]['direccion'] ;
                    $per_direccion->referencia = $direcciones[$i]['referencia'] ;
                    $per_direccion->save() ; 
                } 
            }

            \DB::commit();
        }  
        catch (Exception $e)
        {
            \DB::rollback();
            
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }  

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => 'OK',
        ]);
    }

    public function updateCorreos(Request $request)
    {
        $correos = json_decode($request->input('correos'),true);
        $persona = json_decode($request->input('persona'),true);

        try 
        {
            \DB::beginTransaction();

            PerMail::where([['estado', '=', 1],['persona_id', '=', $persona['id']]])->update(['estado' => 0]);

            $correos = $correos["correos"];
            for($i=0; $i< count($correos); $i++ )
            {
                if($correos[$i]['id']!=null)
                {
                    $per_mail = PerMail::find($correos[$i]['id']) ;
                    $per_mail->mail = $correos[$i]['mail'] ;
                    $per_mail->estado = 1;
                    $per_mail->save() ; 
                }
                else
                {
                    $per_email = new PerMail() ;
                    $per_email->persona_id = $persona['id'] ;
                    $per_email->mail = $correos[$i]['mail'] ;
                    $per_email->save() ;
                }        
            }

            \DB::commit();
        }  
        catch (Exception $e)
        {
            \DB::rollback();
            
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }  

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => 'OK',
        ]);
    }

    public function updateTelefonos(Request $request)
    {
        $telefonos = json_decode($request->input('telefonos'),true);
        $persona = json_decode($request->input('persona'),true);

        try 
        {
            \DB::beginTransaction();

            PerTelefono::where([['estado', '=', 1],['persona_id', '=', $persona['id']]])->update(['estado' => 0]);

            $telefonos = $telefonos["telefonos"];
            for($i=0; $i< count($telefonos); $i++ )
            {
                if($telefonos[$i]['id']!=null)
                {
                    $per_telefono = PerTelefono::find($telefonos[$i]['id']) ;
                    $per_telefono->tipo_telefono_id = $telefonos[$i]['tipo_telefono_id'] ;
                    $per_telefono->telefono = $telefonos[$i]['telefono'] ;
                    $per_telefono->estado = 1;
                    $per_telefono->save() ; 
                }   
                else
                {
                    $per_telefono = new PerTelefono() ;
                    $per_telefono->persona_id = $persona['id'] ;
                    $per_telefono->tipo_telefono_id = $telefonos[$i]['tipo_telefono_id'] ;
                    $per_telefono->telefono = $telefonos[$i]['telefono'] ;
                    $per_telefono->item = 1 ;
                    $per_telefono->save() ;
                } 
            }

            \DB::commit();
        }  
        catch (Exception $e)
        {
            \DB::rollback();
            
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }  

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => 'OK',
        ]);
    }

    public function updateWebs(Request $request)
    {
        $webs = json_decode($request->input('webs'),true);
        $persona = json_decode($request->input('persona'),true);

        try 
        {
            \DB::beginTransaction();

            PerWeb::where([['estado', '=', 1],['persona_id', '=', $persona['id']]])->update(['estado' => 0]);

            $webs = $webs["webs"];
            for($i=0; $i< count($webs); $i++ )
            {
                if($webs[$i]['id']!=null)
                {
                    $per_web = PerWeb::find($webs[$i]['id']) ;
                    $per_web->tipo_web_id = $webs[$i]['tipo_web_id'] ;
                    $per_web->url = $webs[$i]['url'] ;
                    $per_web->estado = 1 ;
                    $per_web->save() ; 
                }
                else
                {
                    $per_web = new PerWeb() ;
                    $per_web->persona_id = $persona['id'] ;
                    $per_web->tipo_web_id = $webs[$i]['tipo_web_id'] ;
                    $per_web->url = $webs[$i]['url'] ;
                    $per_web->save() ;
                }
            }

            \DB::commit();
        }  
        catch (Exception $e)
        {
            \DB::rollback();
            
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }  

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => 'OK',
        ]);
    }

    public function updateDocumentosIdentidad(Request $request)
    {
        $documentos_identidad = json_decode($request->input('documentos_identidad'),true);
        $persona = json_decode($request->input('persona'),true);

        try 
        {
            \DB::beginTransaction();

            PerDocIdentidad::where([['estado', '=', 1],['persona_id', '=', $persona['id']]])->update(['estado' => 0]);

            $documentos_identidad = $documentos_identidad["documentos_identidad"];
            for($i=0; $i< count($documentos_identidad); $i++ )
            {
                if($documentos_identidad[$i]['id']!=null)
                {
                    $per_doc_identidad = PerDocIdentidad::find($documentos_identidad[$i]['id']) ;
                    $per_doc_identidad->tipo_per_doc_identidad_id = $documentos_identidad[$i]['tipo_per_doc_identidad_id'] ; 
                    $per_doc_identidad->numero = $documentos_identidad[$i]['numero'] ; 
                    $per_doc_identidad->fecha_caducidad = (isset($documentos_identidad[$i]['fecha_caducidad']['formatted'])) ? $documentos_identidad[$i]['fecha_caducidad']['formatted'] : $documentos_identidad[$i]['fecha_caducidad']['date']['year']."-".$documentos_identidad[$i]['fecha_caducidad']['date']['month']."-".$documentos_identidad[$i]['fecha_caducidad']['date']['day'] ;
                    $per_doc_identidad->fecha_emision = (isset($documentos_identidad[$i]['fecha_emision']['formatted'])) ? $documentos_identidad[$i]['fecha_emision']['formatted'] : $documentos_identidad[$i]['fecha_emision']['date']['year']."-".$documentos_identidad[$i]['fecha_emision']['date']['month']."-".$documentos_identidad[$i]['fecha_emision']['date']['day'] ;
                    $per_doc_identidad->estado = 1 ;
                    $per_doc_identidad->save() ; 
                }
                else
                {
                    $per_doc_identidad = new PerDocIdentidad() ;
                    $per_doc_identidad->persona_id = $persona['id'] ;
                    $per_doc_identidad->tipo_per_doc_identidad_id = $documentos_identidad[$i]['tipo_per_doc_identidad_id'] ;
                    $per_doc_identidad->numero = $documentos_identidad[$i]['numero'] ;
                    $per_doc_identidad->fecha_caducidad = (isset($documentos_identidad[$i]['fecha_caducidad']['formatted'])) ? $documentos_identidad[$i]['fecha_caducidad']['formatted'] : null ;
                    $per_doc_identidad->fecha_emision = (isset($documentos_identidad[$i]['fecha_emision']['formatted'])) ? $documentos_identidad[$i]['fecha_emision']['formatted'] : null ;
                    $per_doc_identidad->save() ;
                }
            }

            \DB::commit();
        }  
        catch (Exception $e)
        {
            \DB::rollback();
            
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }  

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => 'OK',
        ]);
    }

	public function updateEstado(Request $request)
    {

		$empleado_id = $request->input('id') ;
        $persona_id = $request->input('persona_id') ;
		$estado = $request->input('estado') ;

        $empleado =  Empleado::find($empleado_id) ;
        $empleado->estado = $estado ;
        $empleado->save() ;

        PerImagen::where([['estado', '=', 1],['persona_id', '=', $persona_id]])->update(['estado' => 0]);
        PerWeb::where([['estado', '=', 1],['persona_id', '=', $persona_id]])->update(['estado' => 0]);
        PerTelefono::where([['estado', '=', 1],['persona_id', '=', $persona_id]])->update(['estado' => 0]);
        PerDireccion::where([['estado', '=', 1],['persona_id', '=', $persona_id]])->update(['estado' => 0]);
        PerMail::where([['estado', '=', 1],['persona_id', '=', $persona_id]])->update(['estado' => 0]);
        PerNatural::where([['estado', '=', 1],['persona_id', '=', $persona_id]])->update(['estado' => 0]);
        PerDocIdentidad::where([['estado', '=', 1],['persona_id', '=', $persona_id]])->update(['estado' => 0]);
        CargoEmpleado::where([['estado', '=', 1],['empleado_id', '=', $empleado_id]])->update(['estado' => 0]);
        Persona::where([['estado', '=', 1],['id', '=', $persona_id]])->update(['estado' => 0]);

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $estado,
                ]);
    }
}
