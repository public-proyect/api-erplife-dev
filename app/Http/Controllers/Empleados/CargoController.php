<?php

namespace App\Http\Controllers\Empleados;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Empleados\Cargo;
use JWTAuth;

class CargoController extends Controller
{
    public function __construct(JWTAuth $auth)
    {
        $this->middleware('jwt.auth');
    }

    public function getCargos()
	{
        $data = Cargo::select('cargo.*','sub_area.descripcion AS sub_area')
                ->join('sub_area', 'sub_area.id', '=', 'cargo.sub_area_id')
                ->where('cargo.estado',1)->get() ;

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function getCargosByAreaId(Request $request)
	{ 
        $id_area = $request["id_area"];

        $data = Cargo::select('cargo.*','sub_area.descripcion AS sub_area')
                ->join('sub_area', 'sub_area.id', '=', 'cargo.sub_area_id')
                ->where('sub_area.area_id', '=' , $id_area)
                ->where('cargo.estado', '=' , 1)
                ->get() ;

        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => $data,
	                    ]);
    }

    public function save(Request $request)
    {

        $per_id_padre = 1;

        $sub_area_id = $request->input('sub_area_id');
        $descripcion = $request->input('descripcion');
        $glosa = $request->input('glosa');

        try 
        {

	        $cargo = Cargo::where(['descripcion' => $descripcion])->first();

	        if (!$cargo)
	        {
	        	$cargo = new Cargo() ;
                $cargo->sub_area_id = $sub_area_id ;
                $cargo->descripcion = $descripcion ;
                $cargo->glosa = $glosa ;
                $cargo->per_id_padre = $per_id_padre ;
	        	$cargo->save() ;
	        }
	        else
	        {
	        	$cargo->estado = 1 ;
	        	$cargo->save() ;
	        }

	        return \Response::json([
							'message'  => 'Operación Correcta',
							'error' => false,
							'errors' => '' ,
							'data'   => "OK",
	                    ]);

        } 
        catch (Exception $e)
        {
            return \Response::json([
	            				'message'  => 'Operación Exception',
								'error' => true,
								'errors' => array('message'=> $e->getMessage()) ,
								'data'   => "OK",
							], 500);
        }
    }

	public function getCargoById($id)
    {

        $cargo_id = $id ;

        $data = Cargo::where('cargo.id',$cargo_id)
                            ->join('sub_area', 'sub_area.id', '=', 'cargo.sub_area_id')
                            ->join('area', 'area.id', '=', 'sub_area.area_id')
                            ->select('cargo.*','area.id AS area_id')
                            ->first() ;

        return response()->json([
                	'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function update(Request $request)
    {

        $cargo_id = $request->input('id') ;
        $sub_area_id = $request->input('sub_area_id');
        $descripcion = $request->input('descripcion');
        $glosa = $request->input('glosa');
        
        $cargo =  Cargo::find($cargo_id) ;
        $cargo->sub_area_id = $sub_area_id ;
		$cargo->descripcion = $descripcion ;
		$cargo->glosa = $glosa ;
        $cargo->save() ;

        $data =  Cargo::find($cargo_id) ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $data,
                ]);
    }

	public function updateEstado(Request $request)
    {

		$cargo_id = $request->input('id') ;
		$estado = $request->input('estado') ;

        $cargo =  Cargo::find($cargo_id) ;
        $cargo->estado = $estado ;
        $cargo->save() ;

        return response()->json([
                    'message'  => 'Operación Correcta',
					'error' => false,
					'errors' => '' ,
                    'data'  => $estado,
                ]);
    }
}
