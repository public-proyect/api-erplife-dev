<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class AccesosUserPanelController extends Controller
{

	public function __construct(){
        $this->middleware('jwt.auth');
    }


    public function getControlByTipoControlId(Request $request)
    {
        $tipo_control_id = $request->input('tipo_control_id');
        $user_persona_id = \Crypt::decrypt($request->input('user_persona_id'));
        $per_id_padre    = \Crypt::decrypt($request->input('per_id_padre'));

        try {

           /* $sql = "SELECT
                        control.id,
                        control.control_padre_id,
                        control.jerarquia,
                        control.nombre,
                        control.valor,
                        control.descripcion,
                        control.tipo_control_id,
                        control.glosa
                    FROM control
                    INNER JOIN accesos ON accesos.control_id =  control.id
                    INNER JOIN users_persona ON users_persona.id = accesos.user_persona_id
                    WHERE  control.estado =  1
                    AND accesos.estado = 1
                    AND users_persona.estado =  1
                    AND users_persona.id = $user_persona_id
                    AND control.tipo_control_id =  $tipo_control_id
                    ORDER BY control.jerarquia  ASC;" ;*/

            $sql = "SELECT
                        control.id,
                        control.control_padre_id,
                        control.jerarquia,
                        control.nombre,
                        control.valor,
                        control.descripcion,
                        control.tipo_control_id,
                        control.glosa
                    FROM control
                    INNER JOIN accesos ON accesos.control_id =  control.id
                    INNER JOIN users_persona ON users_persona.id = accesos.user_persona_id
                    WHERE  control.estado =  1
                    AND accesos.estado = 1
                    AND users_persona.estado =  1
                    AND users_persona.id = $user_persona_id
                    ORDER BY control.jerarquia  ASC;" ;

            $data = \DB::select($sql) ;

            return \Response::json([
                            'message' => 'Operación Correcta',
                            'error'   => false,
                            'data'    => $data,
                        ]);

        } catch (Exception $e) {
            return \Response::json(['created' => false], 500);

        }
    }
}
