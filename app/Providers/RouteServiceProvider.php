<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */

    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        # Routes Personalizadas

        $this->mapAuthenticationRoutes();

        $this->mapConfiguracionRoutes() ;

        $this->mapComprasRoutes() ;

        $this->mapVentasRoutes() ;

        $this->mapLogisticaRoutes() ;

        $this->mapPlanillaRoutes() ;

        $this->mapContabilidadRoutes() ;


    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }

     /**
     * rutas for module authentication
    */

    protected function mapAuthenticationRoutes(){

        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/authentication.php');
        });
    }



    /**
     * routas for module Configuracion
    */
    protected function mapConfiguracionRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/configuracion.php');
        });
    }


    /**
     * routas for module Compras
    */
    protected function mapComprasRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/compras.php');
        });
    }

    /**
     * routas for module ventas
     */
    protected function mapVentasRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/ventas.php');
        });
    }

    /**
     * routas for module almacen
    */
    protected function mapLogisticaRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/logistica.php');
        });
    }


    /**
     * routas for module Planillas
    */
    protected function mapPlanillaRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/planillas.php');
        });
    }

    /**
     * routas for module ontabilidad
    */
    protected function mapContabilidadRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/contabilidad.php');
        });
    }

}
