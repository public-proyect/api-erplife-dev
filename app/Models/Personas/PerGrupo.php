<?php

namespace App\Models\Personas;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class PerGrupo extends Model
{
    protected $table = 'per_grupo';
    
    protected $fillable = [
                            'persona_id',
                            'per_id_padre',
                            'par_codigo',
                            'par_clase',
                            'codigo',
                            'glosa',
                            'parametro_id',
                            'estado'
						];
    protected $guarded = ['id'];
    protected $hidden = ['created_at', 'updated_at'];


	public function persona()
    {
        return $this->belongsTo('App\Models\Personas\Persona','persona_id','id') ;
    }

}
