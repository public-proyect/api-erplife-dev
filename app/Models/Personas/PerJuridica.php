<?php

namespace App\Models\Personas;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class PerJuridica extends Model
{
    protected $table = 'per_juridica';
    public $timestamps = false;

    protected $fillable = [
							'persona_id',
							'rubro_id',
							'ruc',
							'razon_social',
                            'nombre_comercial',
                            'glosa',
							'estado',
							];
    
    protected $guarded = ['id'];

    public function persona()
    {
        return $this->belongsTo('App\Models\Personas\Persona','persona_id','id') ;
    }

    public function rubro()
    {
        return $this->belongsTo('App\Models\Personas\Rubro','rubro_id','id') ;
    }
}
