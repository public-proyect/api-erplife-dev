<?php

namespace App\Models\Personas;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class PerDocIdentidad extends Model
{
    protected $table = 'per_doc_identidad';
    public $timestamps = false;

    protected $fillable = [
                        'persona_id',
                        'tipo_per_doc_identidad_id',
                        'item',
                        'numero',
                        'fecha_emision',
                        'fecha_caducidad',
                        'imagen',
                        'estado',
					];

	protected $guarded = ['id'];


    public function persona()
    {
        return $this->belongsTo('App\Models\Personas\Persona','persona_id','id') ;
    }

    public function tipoDocumento()
    {
        return $this->belongsTo('App\Models\Personas\TipoPerDocIdentidad','tipo_per_doc_identidad_id','id') ;
    }

}
