<?php

namespace App\Models\Personas;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class PerRelacion extends Model
{
    protected $table = 'per_relacion';

    protected $fillable = [
								'persona_id',
								'tipo_relacion_id',
								'per_id_padre',
								'estado',
							] ;
                            
    protected $guarded = ['id'];
    protected $hidden = ['created_at', 'updated_at'];

	public function persona()
    {
        return $this->belongsTo('App\Models\Personas\Persona','persona_id','id') ;
    }

    public function tipoRelacion()
    {
        return $this->belongsTo('App\Models\Personas\TipoRelacion','tipo_relacion_id','id') ;
    }
}
