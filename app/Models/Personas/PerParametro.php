<?php

namespace App\Models\Personas;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class PerParametro extends Model
{
    protected $table = 'per_parametro';
    public $timestamps = false;

	protected $fillable = [
						    'persona_id',
							'par_codigo',
							'par_clase',
                            'glosa',
                            'estado'
						];
    
    protected $guarded = ['id'];	
    
	public function persona()
    {
        return $this->belongsTo('App\Models\Personas\Persona','persona_id','id') ;
    }
}
