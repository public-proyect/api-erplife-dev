<?php

namespace App\Models\Personas;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class PerNatural extends Model
{
    protected $table = 'per_natural';
    public $timestamps = false;

	protected $fillable = [
								'persona_id' ,
								'dni' ,
								'ruc' ,
								'apellidos' ,
								'nombres' ,
								'estado_civil' ,
								'sexo' ,
								'estado'
							] ;
	
	protected $guarded = ['id'];

	public function persona()
    {
        return $this->belongsTo('App\Models\Personas\Persona','persona_id','id') ;
    }
}
