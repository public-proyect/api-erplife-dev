<?php

namespace App\Models\Personas;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class PerWeb extends Model
{
	protected $table = 'per_web';
    public $timestamps = false;

	protected $fillable = [
							'persona_id',
							'tipo_web_id',
							'url',
							'estado',
						];

	 protected $guarded = ['id'];

	public function persona()
    {
        return $this->belongsTo('App\Models\Personas\Persona','persona_id','id') ;
    }


}
