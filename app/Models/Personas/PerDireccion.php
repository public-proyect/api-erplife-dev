<?php

namespace App\Models\Personas;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class PerDireccion extends Model
{
    protected $table = 'per_direccion';
    public $timestamps = false;


    protected $fillable = [
							'persona_id',
							'tipo_direccion_id',
							'ubigeo_id',
                            'item',
							'direccion',
							'referencia',
							'estado',
						];
     protected $guarded = ['id'];

	public function persona()
    {
        return $this->belongsTo('App\Models\Personas\Persona','persona_id','id') ;
    }

    public function tipoDireccion()
    {
        return $this->belongsTo('App\Models\Personas\TipoDireccion','tipo_direccion_id','id') ;
    }

    public function ubigeo()
    {
        return $this->belongsTo('App\Models\Personas\Ubigeo','ubigeo_id','id') ;
    }
}
