<?php

namespace App\Models\Personas;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Rubro extends Model
{
    protected $table = 'rubro';
    public $timestamps = false;
    protected $fillable = [
							'descripcion',
							'glosa',
							'estado',
						] ;
	
	 protected $guarded = ['id'];

    public function perJuridica()
	{
		return $this->hasMany('App\Models\Personas\PerJuridica','rubro_id');
	}

}
