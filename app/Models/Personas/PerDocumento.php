<?php

namespace App\Models\Personas;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class PerDocumento extends Model
{
    protected $table = 'per_documento';
   
    protected $fillable = [
                            'persona_id',
                            'tipo_per_documento_id',
                            'numero',
                            'feha_emision',
                            'fecha_caducidad',
                            'clase_categoria',
                            'imagen',
                            'estado',
                        ];

	protected $guarded = ['id'];
    protected $hidden = ['created_at', 'updated_at'];


    public function persona()
    {
        return $this->belongsTo('App\Models\Personas\Persona','persona_id','id') ;
    }

    public function tipoPerDocumento()
    {
        return $this->belongsTo('App\Models\Personas\TipoPerDocumento','tipo_per_documento_id','id') ;
    }

}
