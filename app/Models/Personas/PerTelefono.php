<?php

namespace App\Models\Personas;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class PerTelefono extends Model
{
    protected $table = 'per_telefono';
    public $timestamps = false;

	protected $fillable = [
						    'persona_id',
							'tipo_telefono_id',
							'item',
                            'telefono',
                            'estado'
						];
    
    protected $guarded = ['id'];	
    
	public function persona()
    {
        return $this->belongsTo('App\Models\Personas\Persona','persona_id','id') ;
    }

    public function tipoTelefono()
    {
        return $this->belongsTo('App\Models\Personas\TipoTelefono','tipo_telefono_id','id') ;
    }
}
