<?php

namespace App\Models\Personas;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Persona extends Model
{
    protected $table = 'persona';

    protected $fillable = [
						'empresa_id',
						'per_nombre',
						'per_apellidos',
						'per_fecha_nac',
						'per_tipo',
						'estado',
					];
	protected $guarded = ['id'];
	// protected $appends = ['full_name'];
	protected $hidden = ['created_at', 'updated_at','per_id_padre'];

	# relaciones

		public function empresa()
		{
			return $this->belongsTo('\App\Models\Accesos\Empresa','empresa_id','id');
		}
		public function UsersPersonas()
		{
			return $this->hasMany('\App\Models\Personas\UsersPersona','persona_id');
		}

		public function perNatural()
		{
			return $this->hasMany('\App\Models\Personas\PerNatural');
		}

		public function perJuridica()
		{
			return $this->hasMany('\App\Models\Personas\PerJuridica','persona_id');
		}

		public function perTelefono()
		{
			return $this->hasMany('\App\Models\Personas\PerTelefono','persona_id')->orderBy('item');
		}

		public function perMail()
		{
			return $this->hasMany('\App\Models\Personas\PerMail','persona_id')->orderBy('item');
		}

		public function perDocumento()
		{
			return $this->hasMany('\App\Models\Personas\PerDocumento','persona_id');
		}

		public function perDireccion()
		{
			return $this->hasMany('\App\Models\Personas\PerDireccion','persona_id');
		}

		public function perImagen()
		{
			return $this->hasOne('\App\Models\Personas\PerImagen','persona_id');
		}

		public function perRelacion()
		{
			return $this->hasMany('\App\Models\Personas\PerRelacion','persona_id');
		}

		public function perWeb()
		{
			return $this->hasMany('\App\Models\Personas\PerWeb','persona_id');
		}

		public function perDocIdentidad()
		{
			return $this->hasMany('\App\Models\Personas\PerDocIdentidad','persona_id');
		}

/*		public function sucursales(){
			return $this->hasMany('\App\Models\Sucursales\Sucursal','per_id_padre');
		}
*/




	# attributes
		public function getFullNameAttribute()
		{
			return  $this->per_apellidos .' '. $this->per_nombre ;
		}



		# edad del persona
		public function getAgeAttribute()
		{
			return \Carbon\Carbon::parse($this->per_fecha_nac)->age;
		}

		/*public function getFullNameAttribute() {
		    return $this->attributes['name'] . ' - ' . $this->attributes['code'];
		}*/


		public function setPerFechaNacAttribute($value)
	    {
	    	if ($value != null)
	        $this->attributes['per_fecha_nac'] = \Carbon\Carbon::createFromFormat('Y-m-d',$value)->format('Y-m-d') ;
	    }

	    /*public function getPerFechaNacAttribute()
	    {
	        return \Carbon\Carbon::createFromFormat('Y-m-d',$this->attributes['per_fecha_nac'])->format('d/m/Y');
	    }*/

	# scopes
    	# personas naturales
	    public function scopeNatural($query)
	    {
	        return $query->where('per_tipo', '=', 1);
	    }

	    public function scopeJuridica($query)
	    {
	        return $query->where('per_tipo', '=', 2);
	    }

}
