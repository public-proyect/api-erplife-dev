<?php

namespace App\Models\Personas;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class TipoDireccion extends Model
{
    protected $table = 'tipo_direccion';
    public $timestamps = false;

    protected $fillable = [
							'descripcion',
							'glosa',
							'estado',
						] ;
	
	protected $guarded = ['id'];

    public function perDireccion()
	{
		return $this->hasMany('App\Models\Personas\PerDireccion','tipo_direccion_id');
	}
}
