<?php

namespace App\Models\Personas;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class TipoPerDocumento extends Model
{
    protected $table = 'tipo_per_documento';
    public $timestamps = false;

    protected $fillable = [
							'descripcion',
							'glosa',
							'estado',
						] ;
	
	protected $guarded = ['id'];

    public function perDocumento()
	{
		return $this->hasMany('App\Models\Personas\PerDocumento','tipo_per_documento_id','id');
	}
}
