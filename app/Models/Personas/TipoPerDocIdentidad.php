<?php

namespace App\Models\Personas;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class TipoPerDocIdentidad extends Model
{
    protected $table = 'tipo_per_doc_identidad';
    public $timestamps = false;

    protected $fillable = [
							'descripcion',
							'glosa',
							'estado',
						] ;
	
	protected $guarded = ['id'];

    public function perDocIdentidad()
	{
		return $this->hasMany('App\Models\Personas\PerDocIdentidad','tipo_per_doc_identidad_id','id');
	}

	
}
