<?php

namespace App\Models\Personas;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class PerMail extends Model
{
    protected $table = 'per_mail';
    public $timestamps = false;

    protected $fillable = [
							'persona_id',
							'item',
                            'mail',
                            'estado'
						];
    protected $guarded = ['id'];

	public function persona()
    {
        return $this->belongsTo('App\Models\Personas\Persona','persona_id','id') ;
    }

}
