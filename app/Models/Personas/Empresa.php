<?php

namespace App\Models\Personas;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Empresa extends Model
{
   protected $table = 'empresa';
    public $timestamps = false;
    protected $fillable = [
							'nombre',
							'nombre_comercial',
							'fecha_reg',
							'estado',
						];
    protected $guarded = ['id'];

    public function personas()
    {
        return $this->hasMany('App\Models\Personas\Persona','empresa_id');
    }
}
