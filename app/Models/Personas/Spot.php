<?php

namespace App\Models\Personas;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Spot extends Model
{
    protected $table = 'spot';
    public $timestamps = false;
    protected $fillable = [
							'descripcion',
                            'porcentaje',
							'glosa',
							'estado',
						] ;
	
	 protected $guarded = ['id'];

    public function perJuridica()
	{
		return $this->hasMany('App\Models\Personas\PerSpot','spot_id');
	}

}
