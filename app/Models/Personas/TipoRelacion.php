<?php

namespace App\Models\Personas;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class TipoRelacion extends Model
{
    protected $table = 'tipo_relacion';
    public $timestamps = false;

    protected $fillable = [
							'descripcion',
							'glosa',
							'estado',
						] ;

	protected $guarded = ['id'];

    public function perRelacion()
	{
		return $this->hasMany('App\Models\Personas\PerRelacion','tipo_relacion_id');
	}
}
