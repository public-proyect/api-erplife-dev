<?php

namespace App\Models\Personas;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class TipoTelefono extends Model
{
    protected $table = 'tipo_telefono';
    public $timestamps = false;

    protected $fillable = [
							'descripcion',
							'glosa',
							'estado',
						] ;

	protected $guarded = ['id'];

    public function perTelefono()
	{
		return $this->hasMany('App\Models\Personas\PerTelefono','tipo_telefono_id');
	}
}
