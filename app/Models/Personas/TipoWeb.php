<?php

namespace App\Models\Personas;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class TipoWeb extends Model
{
	protected $table = 'tipo_web';
    public $timestamps = false;
	
	protected $fillable = [
							'descripcion',
							'glosa',
							'estado',
						] ;

	protected $guarded = ['id'];

	public function perWeb()
	{
		return $this->hasMany('App\Models\Personas\PerWeb','tipo_web_id');
	}
}
