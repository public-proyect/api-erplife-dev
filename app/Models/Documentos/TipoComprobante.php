<?php

namespace App\Models\Documentos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class TipoComprobante extends Model
{
    protected $table = 'tipo_comprobante';
    public $timestamps = false;
	protected $fillable = [
							'per_id_padre',
							'descripcion',
							'numero',
							'estado',
					];
    
    protected $guarded = ['id'];

	public function correlativos()
    {
        return $this->hasMany('App\Models\Documentos\Correlativo');
    }

    public function docPagoDetalles()
    {
        return $this->hasMany('App\Models\Documentos\DocPagoDetalle');
    }
}
