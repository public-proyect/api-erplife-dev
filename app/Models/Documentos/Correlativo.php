<?php

namespace App\Models\Documentos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Correlativo extends Model
{
    protected $table = 'correlativo';

    public $timestamps = false;

    protected $fillable = [
                            'per_id_padre',
                            'tipo_comprobante_id',
                            'punto_emision_id',
                            'serie',
                            'numero_ini',
                            'numero_fin',
                            'numero_actual',
                            'estado'
                        ];
    
    protected $guarded = ['id'];

    public function tipoComprobante()
    {
        return $this->belongsTo('App\Models\Documentos\Tipocomprobante') ;
    }
    
    public function puntoEmision()
    {
        return $this->belongsTo('App\Models\Sucursales\PuntoEmision') ;
    }
}