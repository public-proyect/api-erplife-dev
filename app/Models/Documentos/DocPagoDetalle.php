<?php

namespace App\Models\Documentos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class DocPagoDetalle extends Model
{
	protected $table = 'doc_pago_datalle';
    
    public $timestamps = true;

    protected $fillable = [
                            'doc_pago_id',
                            'tipo_comprobante_id',
                            'item',
                            'numero_comp',
                            'importe',
                            'fecha_pago',
                            'tipo_cambio',
                            'doc_tipo_id',
                            'par_medio_pago_id',
                            'glosa',
                            'estado'
                        ];
    
    protected $guarded = ['id'];

    protected $hidden = ['created_at', 'updated_at'];

	public function docPago()
    {
        return $this->belongsTo('App\Models\Documentos\DocPago');
    }

    public function docTipo()
    {
        return $this->belongsTo('App\Models\Documentos\DocTipo');
    }

    public function tipoComprobantes()
    {
        return $this->hasMany('App\Models\Documentos\TipoComprobante');
    }
}
