<?php

namespace App\Models\Documentos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class DocTipo extends Model
{
	protected $table = 'doc_tipo';
    public $timestamps = false;
    protected $fillable = [
                            'per_id_padre',
                            'descripcion',
                            'glosa',
                            'estado'
                        ];
    
    protected $guarded = ['id'];

	public function documentos()
    {
        return $this->hasMany('App\Models\Documentos\Documento');
    }
}
