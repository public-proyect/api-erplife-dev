<?php

namespace App\Models\Documentos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class DocPersona extends Model
{
	protected $table = 'doc_persona';
    public $timestamps = false;
    protected $fillable = [
                            'documento_id',
                            'per_grupo_id',
                            'persona_id',
                            'par_codigo',
                            'glosa',
                            'estado'
                        ];
    
    protected $guarded = ['id'];

	public function documento()
    {
        return $this->belongsTo('App\Models\Documentos\Documento');
    }
}
