<?php

namespace App\Models\Documentos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class DocParametro extends Model
{
	protected $table = 'doc_parametro';

    public $timestamps = false;

    protected $fillable = [
                            'documento_id',
                            'par_clase',
                            'par_codigo',
                            'descripcion',
                            'glosa',
                            'estado'
                        ];
    
    protected $guarded = ['id'];

	public function documento()
    {
        return $this->belongsTo('App\Models\Documentos\Documento');
    }
}
