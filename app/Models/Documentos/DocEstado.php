<?php

namespace App\Models\Documentos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class DocEstado extends Model
{
    protected $table = 'doc_estado';

    public $timestamps = false;

    protected $fillable = [
                            'documento_id',
                            'par_codigo',
                            'par_clase',
                            'doc_est_fecha',
                            'glosa'    
                        ];
    
    protected $guarded = ['id'];

    public function documento()
    {
        return $this->belongsTo('App\Models\Documentos\Documento') ;
    }
}