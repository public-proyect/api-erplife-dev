<?php

namespace App\Models\Documentos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Documento extends Model
{
	protected $table = 'documento';
    
    public $timestamps = true;

    protected $fillable = [
                            'per_id_padre',
                            'doc_tipo_id',
                            'user_id',
                            'codigo',
                            'doc_fecha',
                            'doc_descripcion',
                            'doc_glosa',
                            'estado'
                        ];
    
    protected $guarded = ['id'];

    public function docPagos()
    {
        return $this->hasMany('App\Models\Documentos\DocPago');
    }

    public function docVigencias()
    {
        return $this->hasMany('App\Models\Documentos\DocVigencia');
    }

	public function docTipo()
    {
        return $this->belongsTo('App\Models\Documentos\DocTipo');
    }

    public function docPersonas()
    {
        return $this->hasMany('App\Models\Documentos\DocPersona');
    }

    public function docDetalles()
    {
        return $this->hasMany('App\Models\Documentos\DocDetalle');
    }

    public function kardexs()
    {
        return $this->hasMany('App\Models\Kardex\Kardex');
    }

    public function docEstados()
    {
        return $this->hasMany('App\Models\Documentos\DocEstado');
    }

    public function docParametros()
    {
        return $this->hasMany('App\Models\Documentos\DocParametro');
    }

    public function docRefs()
    {
        return $this->hasMany('App\Models\Documentos\DocRef');
    }
}
