<?php

namespace App\Models\Documentos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class DocDetalle extends Model
{
	protected $table = 'doc_detalle';
    
    public $timestamps = false;

    protected $fillable = [
                            'documento_id',
                            'producto_id',
                            'cantidad',
                            'precio_unitario',
                            'precio_total',
                            'glosa',
                            'estado'
                        ];
    
    protected $guarded = ['id'];


	public function documento()
    {
        return $this->belongsTo('App\Models\Documentos\Documento');
    }

    public function producto()
    {
        return $this->belongsTo('App\Models\Productos\Producto');
    }
}
