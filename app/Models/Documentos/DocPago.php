<?php

namespace App\Models\Documentos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class DocPago extends Model
{
	protected $table = 'doc_pago';
    public $timestamps = true;

    protected $fillable = [
                            'per_id_padre',
                            'documento_id',
                            'forma_pago_id',
                            'moneda_id',
                            'igv_id',
                            'items',
                            'importe',
                            'importe_igv',
                            'importe_total',
                            'saldo',
                            'fecha_vence',
                            'estado',
                            'glosa'
                        ];
    
    protected $guarded = ['id'];

    protected $hidden = ['created_at', 'updated_at'];

    public function docPagoDetalles()
    {
        return $this->hasMany('App\Models\Documentos\DocPagoDetalle');
    }

    public function igv()
    {
        return $this->belongsTo('App\Models\Documentos\Igv') ;
    }

    public function moneda()
    {
        return $this->belongsTo('App\Models\Documentos\Moneda') ;
    }

    public function documento()
    {
        return $this->belongsTo('App\Models\Documentos\Documento') ;
    }

    public function formaPago()
    {
        return $this->belongsTo('App\Models\Documentos\FormaPago') ;
    }
}
