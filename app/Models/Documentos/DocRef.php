<?php

namespace App\Models\Documentos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class DocRef extends Model
{

    protected $table = 'doc_ref';

    public $timestamps = false;

    protected $fillable = [
                            'documento_id',
                            'glosa',
                            'estado'    
                        ];
    
    protected $guarded = ['id'];

	public function documento()
    {
        return $this->belongsTo('App\Models\Documentos\Documento');
    }
}
