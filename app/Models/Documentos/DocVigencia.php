<?php

namespace App\Models\Documentos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class DocVigencia extends Model
{
	protected $table = 'doc_vigencia';
    public $timestamps = false;
    protected $fillable = [
                            'documento_id',
                            'fecha_emision',
                            'fecha_fin',
                            'glosa',
                            'estado'
                        ];
    
    protected $guarded = ['id'];

	public function documento()
    {
        return $this->belongsTo('App\Models\Documentos\Documento');
    }
}
