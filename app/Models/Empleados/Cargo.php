<?php

namespace App\Models\Empleados;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Cargo extends Model
{
    protected $table = 'cargo';
    public $timestamps = false;
	protected $fillable = [
						'area_id',
						'per_id_padre',
						'descripcion',
						'glosa',
						'estado',
					];

	protected $hidden = ['per_id_padre'];

	public function subArea()
	{
        return $this->belongsTo('App\Models\Empleados\SubArea') ;
	}
}
