<?php

namespace App\Models\Empleados;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class SubArea extends Model
{
	protected $table   = 'sub_area';
	public $timestamps = false;
	protected $fillable = [
						'area_id',
						'per_id_padre',
						'descripcion',
						'glosa',
						'estado',
					];

	public function cargo()
	{
		return $this->hasMany('\App\Models\Empleados\Cargo');
	}

	public function area()
	{
		return $this->belongsTo('\App\Models\Empleados\Area');
	}
}
