<?php

namespace App\Models\Empleados;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class CargoEmpleado extends Model
{
	protected $table = 'cargo_empleado';
	protected $fillable = [
							'cargo_id',
							'empleado_id',
							'fecha_ini',
							'fecha_fin',
							'estado',
						];

	protected $hidden = ['created_at', 'updated_at'];

	public function cargo()
	{
        return $this->belongsTo('App\Models\Empleados\Cargo') ;
	}

	public function empleado()
	{
        return $this->belongsTo('App\Models\Empleados\Empleado') ;
	}



}
