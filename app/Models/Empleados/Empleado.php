<?php

namespace App\Models\Empleados;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Empleado extends Model
{
	protected $table = 'empleado';

    protected $fillable =  [
						'persona_id',
                        'per_id_padre',
						'codigo',
						'estado',
     ] ;

    protected $hidden = ['created_at', 'updated_at'];


    public function personas()
    {
    	return $this->belongsTo('App\Models\Personas\Persona');
    }
    public function cargosEmpleados()
    {
        return $this->hasMany('App\Models\Empleados\CargoEmpleado');
    }
}
