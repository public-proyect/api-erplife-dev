<?php

namespace App\Models\Empleados;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Area extends Model
{
	protected $table = 'area';
    public $timestamps = false;
	protected $fillable = [
						'per_id_padre',
						'descripcion',
						'glosa',
						'estado',
					];

	protected $hidden = ['per_id_padre'];


	public function subAreas()
	{
		return $this->hasMany('\App\Models\Empleados\SubArea');
	}
}


