<?php

namespace App\Models\Socialite;


use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class OauthIdentities extends Model
{
     protected $table = 'oauth_identities';
}
