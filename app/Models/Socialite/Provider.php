<?php

namespace App\Models\Socialite;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Provider extends Model
{
     protected $table = 'provider';

}
