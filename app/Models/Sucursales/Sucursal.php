<?php

namespace App\Models\Sucursales;

use Illuminate\Database\Eloquent\Model;
use App\Models\Accesos\Transaccion ;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Sucursal extends Model
{
	protected $table = 'sucursal';
    // public $timestamps = false;

    protected $fillable = [
							'per_id_padre',
							'departamento_id',
							'item',
							'codigo',
							'nombre',
							'descripcion',
							'estado',
						];
	protected $hidden = ['created_at', 'updated_at'];

	public function tiendas()
    {
        return $this->HasMany('App\Models\Sucursales\Tienda') ;
    }

/*	public function persona(){
		return $this->belongsTo('App\Models\Personas\Persona','per_id_padre','id');
	}*/



}
