<?php

namespace App\Models\Sucursales;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class TipoAlmacen extends Model
{
	protected $table = 'tipo_almacen';
    public $timestamps = false;

    protected $fillable = [
							'per_id_padre',
							'descripcion',
							'glosa',
							'estado',
						];
	
	public function almacenes()
    {
        return $this->hasMany('App\Models\Sucursales\Almacen') ;
    }
}
