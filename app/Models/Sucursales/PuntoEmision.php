<?php

namespace App\Models\Sucursales;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class PuntoEmision extends Model
{
	protected $table = 'punto_emision';

    public $timestamps = true;

    protected $fillable = [
								'tienda_id',
								'maquina_id',
								'per_id_padre',
								'descripcion',
								'estado',
							];

	protected $hidden = ['created_at', 'updated_at'];

	public function tienda()
    {
    	return $this->belongsTo('App\Models\Sucursales\Tienda') ;
    }

    public function maquina()
    {
        return $this->belongsTo('App\Models\Sucursales\Maquina') ;
    }

    public function cerrelativos()
    {
    	return $this->hasMany('App\Models\Generales\Correlativo') ;
    }

}
