<?php

namespace App\Models\Sucursales;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class AlmacenEmpleado extends Model
{
	protected $table = 'almacen_empleado';

    public $timestamps = true;

    protected $fillable = [
							'empleado_id',
                            'almacen_id',
                            'fecha_ini',
                            'fecha_fin',
                            'codigo_empleado',
                            'nombres',
                            'estado'
						];

    protected $hidden = ['created_at', 'updated_at'];

	public function almacen()
    {
        return $this->hasMany('App\Models\Sucursales\Almacen') ;
    }

    public function empleado()
    {
        return $this->hasMany('App\Models\Empleados\Empleado') ;
    }
}
