<?php

namespace App\Models\Sucursales;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Tienda extends Model
{
	protected $table = 'tienda';

    protected $fillable = [
                            'sucursal_id',
    						'per_id_padre',
							'ubigeo_id',
							'codigo',
							'nombre',
							'descripcion',
							'direccion',
							'estado',
						];
	protected $hidden = ['created_at', 'updated_at'];

	public function sucursal()
    {
        return $this->belongsTo('App\Models\Sucursales\Sucursal') ;
    }

    public function ubigeo()
    {
        return $this->belongsTo('App\Models\Ubigeos\Ubigeo') ;
    }

    public function almacenes()
    {
    	return $this->hasMany('App\Models\Sucursales\Almacen') ;
    }

    public function puntosEmision()
    {
        return $this->hasMany('App\Models\Sucursales\PuntoEmision') ;
    }

    public function maquinas()
    {
        return $this->hasMany('App\Models\Sucursales\Maquina') ;
    }
}
