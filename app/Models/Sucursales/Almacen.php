<?php

namespace App\Models\Sucursales;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Almacen extends Model
{
	protected $table = 'almacen';
    
    public $timestamps = true;

    protected $fillable = [
							'per_id_padre',
                            'tienda_id',
                            'tipo_almacen_id',
                            'distrito_id',
							'codigo',
                            'descripcion',
                            'direccion',
                            'telefono',
							'glosa',
                            'estado'
						];
	protected $hidden = ['created_at', 'updated_at'];

	public function tipoAlmacen()
    {
        return $this->belongsTo('App\Models\Sucursales\TipoAlmacen') ;
    }

    public function almacenEmpleados()
    {
        return $this->hasMany('App\Models\Sucursales\AlmacenEmpleado') ;
    }

    public function tienda()
    {
        return $this->belongsTo('App\Models\Sucursales\Tienda') ;
    }
}
