<?php

namespace App\Models\Sucursales;

use Illuminate\Database\Eloquent\Model;

class Maquina extends Model
{
	protected $table = 'maquina';

	public $timestamps = true;
	
	protected $fillable = [
								'tienda_id',
								'codigo_maquina',
								'modelo_maquina',
								'serie_maquina',
								'descripcion',
								'estado',
							];

    protected $hidden = ['created_at', 'updated_at'];

	public function tienda()
    {
    	return $this->belongsTo('App\Models\Sucursales\Tienda') ;
    }

    public function maquinas()
    {
        return $this->belongsTo('App\Models\Sucursales\Maquina') ;
    }

}
