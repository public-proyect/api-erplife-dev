<?php

namespace App\Models\Ubigeos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Ubigeo extends Model
{
    protected $table = 'ubigeo';

    public $timestamps = false;

    protected $fillable = [
						'codigo',
						'ubigeo',
						'descripcion',
                        'ubigeo_id_padre',
                        'estado',
                        'pais_id',
                        'tipo_ubigeo_id'
					];

    public function ubigeos()
	{
		return $this->hasMany('App\Models\Ubigeos\Ubigeo');
	}

    public function pais()
	{
		return $this->belongsTo('App\Models\Ubigeos\Pais');
	}

    public function perDirecciones()
	{
		return $this->hasMany('App\Models\Persona\PerDireccion');
	}



}
