<?php

namespace App\Models\Ubigeos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Pais extends Model
{
    protected $table = 'pais';
    
    public $timestamps = false;
    
    protected $fillable = [
						'code',
						'descripcion',
						'estado',
					];
	
	protected $guarded = ['id'];

    public function ubigeos()
	{
		return $this->hasMany('App\Models\Ubigeos\Ubigeo');
	}
}
