<?php

namespace App\Models\Generales;

use Illuminate\Database\Eloquent\Model;

class ParPerPadre extends Model
{
	protected $table = 'par_perpadre';

    public $timestamps = false;

	protected $fillable = [
						 'parametro_id',
						  'par_clase',
						  'par_codigo',
						  'estado'
					];

	protected $guarded = ['id'];
	protected $hidden = ['per_id_padre'];
}
