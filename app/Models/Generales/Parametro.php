<?php

namespace App\Models\Generales;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Parametro extends Model
{
	protected $table = 'parametro';

    public $timestamps = false;

	protected $fillable = [
						'par_codigo',
						'par_clase',
						'par_jerarquia',
						'par_descripcion',
						'par_glosa',
					];

	protected $guarded = ['id'];
	protected $hidden = ['per_id_padre'];




}
