<?php

namespace App\Models\Contabilidad;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Moneda extends Model
{
    protected $table = 'moneda';

    public $timestamps = false;

    protected $fillable = [
        'per_id_padre',
        'simbolo',
        'descripcion',
        'base',
        'estado',
    ];

    protected $guarded = ['id'];

    public function tipoCambio()
	{
		return $this->hasOne('App\Models\Contabilidad\Tipocambio');
	}
}