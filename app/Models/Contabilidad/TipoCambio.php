<?php

namespace App\Models\Contabilidad;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class TipoCambio extends Model
{
    protected $table = 'tipo_cambio';
    public $timestamps = false;
	protected $fillable = [
						'moneda_id',
						'fecha',
						'compra',
						'venta',
						'estado',
					];

	public function moneda()
	{
		return $this->belongsTo('App\Models\Contabilidad\Moneda');
	}
}
