<?php

namespace App\Models\Contabilidad;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class FormaPago extends Model
{
    protected $table = 'forma_pago';

    public $timestamps = false;

    protected $fillable = [
                            'tipo_pago_id',
                            'descripcion',
                            'estado',
                        ];
    
    protected $guarded = ['id'];

    public function TipoPago()
    {
        return $this->belongsTo('App\Models\Contabilidad\TipoPago') ;
    }
}