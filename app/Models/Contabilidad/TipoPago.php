<?php

namespace App\Models\Contabilidad;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class TipoPago extends Model
{
    protected $table = 'tipo_pago';
    public $timestamps = false;
	protected $fillable = [
						'descripcion',
						'glosa',
						'estado',
					];

	protected $guarded = ['id'];

	public function formaPagos()
    {
        return $this->hasMany('App\Models\Contabilidad\FormaPago');
    }
}
