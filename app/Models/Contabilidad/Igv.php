<?php

namespace App\Models\Contabilidad;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Igv extends Model
{
    protected $table = 'igv';

    public $timestamps = true;

    protected $fillable = [
        'valor',
        'glosa',
        'estado'
    ];

    protected $guarded = ['id'];

    protected $hidden = ['created_at', 'updated_at'];

    public function docPagos()
    {
        return $this->hasMany('App\Models\Documentos\DocPago');
    }
}