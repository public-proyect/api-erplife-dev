<?php

namespace App\Models\Kardex;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class TipoOperacion extends Model
{
	protected $table = 'tipo_operacion';
    public $timestamps = false;

	protected $fillable = [
						'descripcion',
						'tipo',
						'simbolo',
						'estado',
					];

	protected $guarded = ['id'];

	public function kardex()
	{
		return $this->hasMany('\App\Models\Kardex\Kardex','tipo_operacion_id');
	}

}
