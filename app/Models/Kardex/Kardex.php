<?php

namespace App\Models\Kardex;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Kardex extends Model
{
	protected $table = 'kardex';
    public $timestamps = true;

	protected $fillable = [
						'documento_id',
						'tipo_operacion_id',
						'almacen_id_origen',
						'almacen_id_destino',
						'fecha',
						'user_id',
						'glosa',
						'estado',
					];

	protected $guarded = ['id'];

	protected $hidden = ['created_at', 'updated_at','per_id_padre'];

	public function detalleKardex()
	{
		return $this->hasMany('\App\Models\Kardex\DetalleKardex','kardex_id');
	}

	public function tipoOperacion()
	{
		return $this->belongsTo('\App\Models\Kardex\TipoOperacion','tipo_operacion_id','id');
	}

}
