<?php

namespace App\Models\Kardex;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class KardexDetalle extends Model
{
	protected $table = 'kardex_detalle';
    public $timestamps = true;

	protected $fillable = [
						'kardex_id',
						'producto_id',
						'user_id',
						'almacen_id_destino',
						'fecha_mov',
						'stock_actual',
						'stock_anterior',
						'cantidad',
						'precio_unitario',
						'precio_total',
						'glosa',
						'estado',
					];

	protected $guarded = ['id'];
	protected $hidden = ['created_at', 'updated_at','per_id_padre'];

	public function kardex(){
		return $this->belongsTo('\App\Models\Kardex\Kardex','kardex_id','id');
	}

	public function producto(){
		return $this->belongsTo('\App\Models\Productos\Producto','producto_id','id');
	}
}
