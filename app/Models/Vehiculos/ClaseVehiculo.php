<?php

namespace App\Models\Vehiculos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class ClaseVehiculo extends Model
{
	protected $table   = 'clase_vehiculo';
	public $timestamps = false;
	protected $fillable = [
						'per_id_padre',
						'descripcion',
						'glosa',
						'estado',
					];
	
	protected $guarded = ['id'];					

	public function tipoVehiculos()
	{
		return $this->hasMany('App\Models\Vehiculos\TipoVehiculo');
	}
}
