<?php

namespace App\Models\Vehiculos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class MarcaVehiculo extends Model
{

	protected $table   = 'marca_vehiculo';
	public $timestamps = false;
	protected $fillable = [
						'per_id_padre',
						'descripcion',
						'glosa',
						'estado',
					];

	protected $guarded = ['id'];

	public function vehiculos()
	{
		return $this->hasMany('App\Models\Vehiculos\Vehiculo');
	}
}
