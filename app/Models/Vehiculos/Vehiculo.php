<?php

namespace App\Models\Vehiculos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Vehiculo extends Model
{
	protected $table   = 'vehiculo';
	public $timestamps = false;
	protected $fillable = [
						'per_id_padre',
						'tipo_vehiculo_id',
						'marca_vehiculo_id',
						'placa',
						'tarjeta_propiedad',
						'anio_fabricacion',
						'largo',
						'ancho',
						'alto',
						'capacidad',
						'glosa',
						'estado',
					];

	protected $guarded = ['id'];

	public function tipoVehiculo()
	{
		return $this->belongsTo('App\Models\Vehiculos\TipoVehiculo');
	}
	public function marcaVehiculo()
	{
		return $this->belongsTo('App\Models\Vehiculos\MarcaVehiculo');
	}

}
