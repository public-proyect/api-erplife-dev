<?php

namespace App\Models\Vehiculos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class TipoVehiculo extends Model
{

	protected $table   = 'tipo_vehiculo';
	public $timestamps = false;
	protected $fillable = [
						'per_id_padre',
						'clase_vehiculo_id',
						'descripcion',
						'glosa',
						'estado',
					];

	protected $guarded = ['id'];				

	public function claseVehiculo()
	{
		return $this->belongsTo('App\Models\Vehiculos\ClaseVehiculo');
	}

	public function vehiculos()
	{
		return $this->hasMany('App\Models\Vehiculos\Vehiculos');
	}

}
