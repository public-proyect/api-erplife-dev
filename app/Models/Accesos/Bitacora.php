<?php

namespace App\Models\Accesos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Bitacora extends Model
{
    protected $table = 'bitacora';

    protected $fillable = [
						    'user_persona_id',
						    'action',
						    'table_id',
						    'table',
						    'computer_ip',
						    'new_value',
						    'old_value',
						];

	protected $guarded = ['id'];
	protected $hidden = ['created_at', 'updated_at'];

	// public function user(){
	// 	return $this->belongsTo('App\User','user_id','id');
	// }

	public function userPersona()
	{
		return $this->belongsTo('App\Models\Accesos\UserPersona','user_persona_id','id');
	}




}
