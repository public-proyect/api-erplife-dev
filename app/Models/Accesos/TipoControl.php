<?php

namespace App\Models\Accesos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class TipoControl extends Model
{
    protected $table = 'tipo_control';
    public $timestamps = false;
	
    protected $fillable = [
							'descripcion',
							'glosa',
							'estado',
						];
						
	 protected $guarded = ['id'];

	public function control()
	{
		return $this->hasMany('App\Models\Control','tipo_control_id');
	}
}
