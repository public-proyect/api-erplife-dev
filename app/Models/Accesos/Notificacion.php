<?php

namespace App\Models\Accesos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Notificacion extends Model
{
    protected $table = 'notificacion';

    protected $fillable =  [
							'user_id',
							'destino',
							'asunto',
							'mensaje',
							'referencia',
							'tipo',
							'fecha_envio',
							'estado',
     					] ;

	protected $guarded = ['id'];
	protected $hidden = ['created_at', 'updated_at'];


	public function user()
	{
		return $this->belongsTo('App\User','user_id','id');
	}



}
