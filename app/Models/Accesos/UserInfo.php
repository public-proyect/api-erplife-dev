<?php

namespace App\Models\Accesos;

use Illuminate\Database\Eloquent\Model;

class UserInfo extends Model
{
	protected $table = 'user_info';
	public $timestamps = false;



	protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }

}
