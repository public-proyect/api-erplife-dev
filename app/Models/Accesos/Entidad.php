<?php

namespace App\Models\Accesos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Entidad extends Model
{
	protected $table = 'acceso_entidad';
    public $timestamps = false;

	 protected $fillable =  [
						'descripcion',
						'estado'
     ] ;

	 protected $guarded = ['id'];

	public function accesosEntidad()
	{
		return $this->hasMany('App\Models\Accesos\AccesoEntidad','entidad_id');
	}


}
