<?php

namespace App\Models\Accesos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class RolControl extends Model
{
    protected $table = 'rol_control';
    public $timestamps = false;

    protected $fillable = [
								'rol_id',
								'control_id',
								'referencia',
								'estado',
							] ;
                            
    protected $guarded = ['id'];

	public function rol()
    {
        return $this->belongsTo('App\Models\Rol','rol_id','id') ;
    }

    public function control()
    {
        return $this->belongsTo('App\Models\Control','control_id','id') ;
    }

}
