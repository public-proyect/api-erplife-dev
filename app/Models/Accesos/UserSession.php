<?php

namespace App\Models\Accesos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class UserSession extends Model
{
	protected $table = 'user_session';
    public $timestamps = true;

    protected $fillable = [
							'user_id',
							'fecha_entrada',
							'fecha_salida',
							'token',
					];

	protected $guarded = ['id'];
	protected $hidden = ['token'];


	public function user()
	{
		return $this->belongsTo('App\User','user_id','id');
	}

	# attributes
		public function setFechaEntradaAttribute($value)
		{
			$this->attributes['fecha_entrada'] = !empty($value) ? $value : date('Y-m-d H:i:s');
		}
}
