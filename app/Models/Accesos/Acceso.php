<?php

namespace App\Models\Accesos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Acceso extends Model
{
    protected $table = 'accesos';
    public $timestamps = false;

	protected $fillable = [
						'user_persona_id',
						'control_id',
						'referencia',
						'estado',
					];

	protected $guarded = ['id'];

	public function control()
	{
		return $this->belongsTo('App\Models\Accesos\Control','control_id','id');
	}

	public function userPersona(){
		return $this->belongsTo('App\Models\Accesos\UserPersona','user_persona_id','id');
	}
}
