<?php

namespace App\Models\Accesos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class AccesoEntidad extends Model
{
	protected $table = 'acceso_entidad';

	protected $fillable = [
						'user_persona_id',
						'entidad_id',
						'objeto_entidad_id',
						'estado'
					];

	protected $guarded = ['id'];
	protected $hidden = ['created_at', 'updated_at'];

	public function userPersona()
	{
		return $this->belongsTo('App\Models\Accesos\UserPersona','user_persona_id','id');
	}

	public function entidad()
	{
		return $this->belongsTo('App\Models\Accesos\Entidad','entidad_id','id');
	}



}
