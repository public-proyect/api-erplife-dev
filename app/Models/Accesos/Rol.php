<?php

namespace App\Models\Accesos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Rol extends Model
{
    protected $table = 'rol';
	public $timestamps = false;
	
	protected $fillable = [
							'per_id_padre',
							'nombre',
							'descripcion',
							'estado',
						];

	protected $guarded = ['id'];
	protected $hidden = ['per_id_padre'];

	public function users()
	{
		return $this->hasMany('App\User','rol_id');
	}

	public function rolControl()
	{
		return $this->hasMany('App\Models\RolControl','rol_id');
	}
}
