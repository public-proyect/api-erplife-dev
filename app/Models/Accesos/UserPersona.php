<?php

namespace App\Models\Accesos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class UserPersona extends Model
{
    protected $table = 'users_persona';

    protected $fillable = [
                        'user_id',
                        'persona_id',
                        'rol_id',
                        'estado',
					];

	protected $guarded = ['id'];
    protected $hidden = ['created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }

    public function persona()
    {
        return $this->belongsTo('App\Models\Personas\Persona','persona_id','id');
    }

    public function rol()
    {
        return $this->belongsTo('App\Models\Accesos\rol','rol_id','id');
    }
}
