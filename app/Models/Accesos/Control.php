<?php

namespace App\Models\Accesos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Control extends Model
{
    protected $table = 'control';
	public $timestamps = false;

    protected $fillable =  [
						'control_padre_id',
						'tipo_control_id',
						'jerarquia',
						'nombre',
						'valor',
						'descripcion',
						'glosa',
						'estado',
     ] ;   
	 
	protected $guarded = ['id'];

    public function ChildControl()
	{
		return $this->hasMany('\App\Models\Control','control_padre_id', 'id' );
	}


	public function parentAccount()
	{
	    return $this->belongsTo('Account', 'id', 'control_padre_id');
	}

	public function allChildrenControl()
	{
	    return $this->ChildControl()->with('allChildrenControl');
	}

	public function tipoControl(){
		return $this->belongsTo('App\Models\Accesos\TipoControl');
	}

	public function rolControles(){
		return $this->hasMany('App\Models\Accesos\RolControl','control_id');
	}

	public function accesos(){
		return $this->hasMany('App\Models\Accesos\Acceso','control_id');
	}


}
