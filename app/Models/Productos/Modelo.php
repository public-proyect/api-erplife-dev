<?php

namespace App\Models\Productos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Modelo extends Model
{
	protected $table = 'modelo';
    public $timestamps = false;
	protected $fillable = [
						'per_id_padre',
						'marca_id',
						'descripcion',
						'glosa',
						'estado',
					];

	protected $guarded = ['id'];					

    public function marca()
	{
		return $this->belongsTo('App\Models\Productos\Marca');
	}

	public function productos()
	{
		return $this->hasMany('App\Models\Productos\Producto');
	}

}
