<?php

namespace App\Models\Productos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class ListaPrecio extends Model
{
	protected $table = 'lista_precio';
    public $timestamps = true;
	protected $fillable = [
							'per_id_padre',
							'producto_id',
							'precio',
							'porcentaje_ganancia',
							'precio_final',
							'glosa',
							'estado',
					];
	
	protected $guarded = ['id'];					

	protected $hidden = ['created_at', 'updated_at'];

    public function productos()
	{
		return $this->belongsTo('App\Models\Productos\Producto');
	}

}
