<?php

namespace App\Models\Productos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Producto extends Model
{
    protected $table = 'producto';
    public $timestamps = true;

    protected $fillable = [
        'per_id_padre',
        'codigo',
        'codigo_barra',
        'unidad_medida_id',
        'modelo_id',
        'sub_familia_id',
        'stock_min',
        'descripcion',
        'imagen',
        'percepcion',
        'glosa',
        'estado'
    ];

    protected $guarded = ['id'];					

	protected $hidden = ['created_at', 'updated_at'];

    public function listaPrecios()
    {
        return $this->hasMany('App\Models\Productos\ListaPrecio');
    }

    public function stocks()
    {
        return $this->hasMany('App\Models\Productos\Stock');
    }

    public function subFamilias()
    {
        return $this->belongsTo('App\Models\Productos\SubFamilia');
    }

    public function unidadMedidas()
    {
        return $this->belongsTo('App\Models\Productos\UnidadMedida');
    }

    public function modelos()
    {
        return $this->belongsTo('App\Models\Productos\Modelo');
    }

    public function docDetalles()
    {
        return $this->hasMany('App\Models\Productos\DocDetalle');
    }

    public function kardexDetalles()
    {
        return $this->hasMany('App\Models\Productos\KardexDetalle');
    }
}