<?php

namespace App\Models\Productos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Marca extends Model
{
	protected $table = 'marca';
    public $timestamps = false;
	protected $fillable = [
						'per_id_padre',
						'descripcion',
						'imagen',
						'estado',
					];
	
	protected $guarded = ['id'];			

    public function modelos()
	{
		return $this->hasMany('App\Models\Productos\Modelo');
	}

}
