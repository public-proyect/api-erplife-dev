<?php

namespace App\Models\Productos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Clase extends Model
{
	protected $table = 'clase';
    public $timestamps = false;
	protected $fillable = [
						'per_id_padre',
						'codigo',
						'descripcion',
						'glosa',
						'imagen',
						'estado',
					];
	
	protected $guarded = ['id'];					

    public function familias()
	{
		return $this->hasMany('App\Models\Productos\Familia');
	}

}