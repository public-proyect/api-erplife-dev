<?php

namespace App\Models\Productos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class UnidadMedida extends Model
{
    protected $table = 'unidad_medida';
    public $timestamps = false;
    protected $fillable = [
                            'per_id_padre',
                            'unidad_medida_id', 
                            'abreviatura',
                            'descripcion',
                            'equivalente',
                            'glosa',
                            'estado',
                        ];

    protected $guarded = ['id'];					

    public function unidadMedidas()
    {
        return $this->hasMany('App\Models\Productos\UnidadMedida');
    }
    public function productos()
    {
        return $this->hasMany('App\Models\Productos\Producto');
    }
}