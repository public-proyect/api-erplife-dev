<?php

namespace App\Models\Productos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Familia extends Model
{
	protected $table = 'familia';
    public $timestamps = false;
	protected $fillable = [
						'per_id_padre',
						'clase_id',
						'codigo',
						'descripcion',
						'glosa',
						'imagen',
						'estado',
					];

	protected $guarded = ['id'];					

	public function clase()
	{
		return $this->belongsTo('App\Models\Productos\Clase');
	}

    public function subFamilias()
	{
		return $this->hasMany('App\Models\Productos\SubFamilia');
	}
}
