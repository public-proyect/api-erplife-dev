<?php

namespace App\Models\Productos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class SubFamilia extends Model
{
	protected $table   = 'sub_familia';
	public $timestamps = false;
	protected $fillable = [
						'per_id_padre',
						'familia_id',
						'codigo',
						'descripcion',
						'glosa',
						'imagen',
						'estado',
					];
	
	protected $guarded = ['id'];					

    public function productos()
	{
		return $this->hasMany('App\Models\Productos\Producto');
	}

	public function familia()
	{
		return $this->belongsTo('App\Models\Productos\Familia');
	}
}
