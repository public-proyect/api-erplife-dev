<?php

namespace App\Models\Productos;

use Illuminate\Database\Eloquent\Model;

/**
 * developer: @armandoaepp
 * email: armandoaepp@gmail.com
*/
class Stock extends Model
{
	protected $table = 'stock';
    public $timestamps = true;
	protected $fillable = [
						'per_id_padre',
						'producto_id',
						'almacen_id',
						'stock_actual',
						'glosa',
						'estado'
					];

	protected $guarded = ['id'];					

	protected $hidden = ['created_at', 'updated_at'];

    public function producto()
	{
		return $this->belongsTo('App\Models\Productos\Producto');
	}

	public function almacen()
	{
		return $this->belongsTo('App\Models\Sucursales\Almacen');
	}
}
