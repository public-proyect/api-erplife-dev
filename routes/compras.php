<?php

$api=app('Dingo\Api\Routing\Router');

$api->version('v1',['middleware'=>'cors', 'namespace' => 'App\Http\Controllers'],function($api){

    $api->group(['prefix' => 'compras',],function($api){

        $api->group(['namespace' => 'Personas' ], function ($api) {

            $api->get('proveedores','PerGrupoController@getProveedores');
            $api->post('proveedores/upload-imagen-proveedores','PerGrupoController@uploadImagenProveedores');
            $api->post('proveedores/save','PerGrupoController@save');
            $api->get('proveedores/{id}','PerGrupoController@getProveedorById');
            $api->post('proveedores/updateInformacion','PerGrupoController@updateInformacion');
            $api->post('proveedores/updateDirecciones','PerGrupoController@updateDirecciones');
            $api->post('proveedores/updateCorreos','PerGrupoController@updateCorreos');
            $api->post('proveedores/updateTelefonos','PerGrupoController@updateTelefonos');
            $api->post('proveedores/updateWebs','PerGrupoController@updateWebs');
            $api->post('proveedores/updateDocumentosIdentidad','PerGrupoController@updateDocumentosIdentidad');
            $api->post('proveedores/delete','PerGrupoController@updateEstado');
        });

    });
});