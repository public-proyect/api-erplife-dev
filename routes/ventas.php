<?php

$api=app('Dingo\Api\Routing\Router');

$api->version('v1',['middleware'=>'cors', 'namespace' => 'App\Http\Controllers'],function($api){

    $api->group(['prefix' => 'ventas',],function($api){

        $api->group(['namespace' => 'Personas' ], function ($api) {

            $api->get('clientes','PerGrupoController@getClientes');
            $api->post('clientes/upload-imagen-clientes','PerGrupoController@uploadImagenClientes');
            $api->post('clientes/save','PerGrupoController@save');
            $api->get('clientes/{id}','PerGrupoController@getClienteById');
            $api->post('clientes/updateInformacion','PerGrupoController@updateInformacion');
            $api->post('clientes/updateDirecciones','PerGrupoController@updateDirecciones');
            $api->post('clientes/updateCorreos','PerGrupoController@updateCorreos');
            $api->post('clientes/updateTelefonos','PerGrupoController@updateTelefonos');
            $api->post('clientes/updateWebs','PerGrupoController@updateWebs');
            $api->post('clientes/updateDocumentosIdentidad','PerGrupoController@updateDocumentosIdentidad');
            $api->post('clientes/delete','PerGrupoController@updateEstado');
        });

        $api->group(['namespace' => 'Generales' ], function ($api) {
            $api->post('tipos_cliente','ParametroController@getParametros');
            $api->post('tipos_cliente/save','ParametroController@save');
            $api->get('tipos_cliente/{id}','ParametroController@getParametroById');
            $api->post('tipos_cliente/update','ParametroController@update');
            $api->post('tipos_cliente/delete','ParametroController@updateEstado');
        });
    });
});