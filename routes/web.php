<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'cors'], function(){
	// Route::get('login', 'ApiAuthController@authenticate');
	Route::post('login', 'ApiAuthController@authenticate');
    Route::get('logout', 'ApiAuthController@logout');
    Route::post('info-user', 'ApiAuthController@getDataUserInfo');

	// Route::post('user', 'ApiAuthController@getAuthenticatedUser');
	Route::post('demo', 'ApiAuthController@getDataUserPersona');

});


*/

// Route::get('panel', 'Accesos\AccesosUserController@getControlByTipoControlId');


Route::get('/', function () {

    $encrypt = \Crypt::encrypt(2);
    echo "$encrypt <br> " ;
    $decrypt = \Crypt::decrypt($encrypt);
    echo "$decrypt <br> " ;


});

Route::get('controles', 'Panel\AccesosUserController@getControlByTipoControlId');

$api=app('Dingo\Api\Routing\Router');

$api->version('v1',function($api){

    $api->group(['middleware'=>'cors'],function($api){
        $api->get('departamentos','App\Http\Controllers\Ubigeos\UbigeosController@getDepartamentos');
        $api->post('distritos-by-departamento','App\Http\Controllers\Ubigeos\UbigeosController@getDistritosByDepartameto');
        $api->post('distritos','App\Http\Controllers\Ubigeos\UbigeosController@getDistritos');
    });
});