<?php

$api=app('Dingo\Api\Routing\Router');

$api->version('v1',['middleware'=>'cors', 'namespace' => 'App\Http\Controllers'],function($api){

    $api->group(['prefix' => 'configuracion',],function($api){

        $api->group(['namespace' => 'Sucursales' ], function ($api) {

            # accesos for load menus
            $api->get('tipos-almacen','TiposAlmacenController@getTiposAlmacen');
            $api->post('tipos-almacen','TiposAlmacenController@postTipoAlmacen');
            $api->get('tipos-almacen/{page}','TiposAlmacenController@getTipoAlmacenById');
            $api->post('tipos-almacen/update','TiposAlmacenController@updateTipoAlmacen');
            $api->post('tipos-almacen/delete','TiposAlmacenController@deleteTipoAlmacen');

            $api->get('sucursales','SucursalesController@getSurcusales');
            $api->post('sucursales','SucursalesController@postSucursal');
            $api->get('sucursales/{page}','SucursalesController@getSucursalById');
            $api->post('sucursales/update','SucursalesController@updateSucursal');
            $api->post('sucursales/delete','SucursalesController@deleteSucursal');

            $api->get('tiendas','TiendasController@getTiendas');
            $api->post('tiendas','TiendasController@postTienda');
            $api->get('tiendas/{page}','TiendasController@getTiendaById');
            $api->post('tiendas/update','TiendasController@updateTienda');
            $api->post('tiendas/delete','TiendasController@deleteTienda');

            $api->get('almacenes','AlmacenesController@getAlmacenes');
            $api->post('almacenes','AlmacenesController@postAlmacen');
            $api->get('almacenes/{page}','AlmacenesController@getAlmacenById');
            $api->post('almacenes/update','AlmacenesController@updateAlmacen');
            $api->post('almacenes/delete','AlmacenesController@deleteAlmacen');

            $api->get('maquinas','MaquinasController@getMaquinas');
            $api->post('maquinas','MaquinasController@postMaquina');
            $api->get('maquinas/{id}','MaquinasController@getMaquinaById');
            $api->post('maquinas/update','MaquinasController@updateMaquina');
            $api->post('maquinas/delete','MaquinasController@deleteMaquina');

            $api->get('puntos-emision','PuntosEmisionController@getPuntosEmision');
            $api->post('puntos-emision','PuntosEmisionController@postPuntoEmision');
            $api->get('puntos-emision/{id}','PuntosEmisionController@getPuntoEmisionById');
            $api->post('puntos-emision/update','PuntosEmisionController@updatePuntoEmision');
            $api->post('puntos-emision/delete','PuntosEmisionController@deletePuntoEmision');
        });

        $api->group(['namespace' => 'Accesos' ], function ($api) {

            $api->get('roles','RolesController@getRoles');
            $api->get('roles/p/{page}','RolesController@getRoles2');
            $api->post('roles/save','RolesController@save');
            $api->get('roles/{id}','RolesController@getRoleById');
            $api->post('roles/update','RolesController@update');
            $api->post('roles/delete','RolesController@updateEstado');

            $api->get('tipos-control','TipoControlController@getTiposControl');
            $api->post('tipos-control/save','TipoControlController@save');
            $api->get('tipos-control/{id}','TipoControlController@getTipoControlById');
            $api->post('tipos-control/update','TipoControlController@update');
            $api->post('tipos-control/delete','TipoControlController@updateEstado');

            $api->get('controles','ControlController@getControles');
            $api->post('controles/save','ControlController@save');
            $api->get('controles/{id}','ControlController@getControlById');
            $api->post('controles/update','ControlController@update');
            $api->post('controles/delete','ControlController@updateEstado');

        });

        $api->group(['namespace' => 'Empleados' ], function ($api) {

            $api->get('areas','AreaController@getAreas');
            $api->post('areas/save','AreaController@save');
            $api->get('areas/{id}','AreaController@getAreaById');
            $api->post('areas/update','AreaController@update');
            $api->post('areas/delete','AreaController@updateEstado');

            $api->get('sub-areas','SubAreaController@getSubAreas');
            $api->post('sub-areas','SubAreaController@getSubAreasByAreaId');
            $api->post('sub-areas/save','SubAreaController@save');
            $api->get('sub-areas/{id}','SubAreaController@getSubAreaById');
            $api->post('sub-areas/update','SubAreaController@update');
            $api->post('sub-areas/delete','SubAreaController@updateEstado');

            $api->get('cargos','CargoController@getCargos');
            $api->post('cargos','CargoController@getCargosByAreaId');
            $api->post('cargos/save','CargoController@save');
            $api->get('cargos/{id}','CargoController@getCargoById');
            $api->post('cargos/update','CargoController@update');
            $api->post('cargos/delete','CargoController@updateEstado');

            $api->get('empleados','EmpleadoController@getEmpleados');
            $api->post('empleados/upload-imagen','EmpleadoController@uploadImagen');
            $api->post('empleados/save','EmpleadoController@save');
            $api->get('empleados/{id}','EmpleadoController@getEmpleadoById');
            $api->post('empleados/updateInformacion','EmpleadoController@updateInformacion');
            $api->post('empleados/updateDirecciones','EmpleadoController@updateDirecciones');
            $api->post('empleados/updateCorreos','EmpleadoController@updateCorreos');
            $api->post('empleados/updateTelefonos','EmpleadoController@updateTelefonos');
            $api->post('empleados/updateWebs','EmpleadoController@updateWebs');
            $api->post('empleados/updateDocumentosIdentidad','EmpleadoController@updateDocumentosIdentidad');
            $api->post('empleados/delete','EmpleadoController@updateEstado');
        });

        $api->group(['namespace' => 'Personas' ], function ($api) {

            $api->get('rubros','RubroController@getRubros');
            $api->post('rubros/save','RubroController@save');
            $api->get('rubros/{id}','RubroController@getRubroById');
            $api->post('rubros/update','RubroController@update');
            $api->post('rubros/delete','RubroController@updateEstado');

            $api->get('tipos-direccion','TipoDireccionController@getTiposDireccion');
            $api->post('tipos-direccion/save','TipoDireccionController@save');
            $api->get('tipos-direccion/{id}','TipoDireccionController@getTipoDireccionById');
            $api->post('tipos-direccion/update','TipoDireccionController@update');
            $api->post('tipos-direccion/delete','TipoDireccionController@updateEstado');

            $api->get('tipos-telefono','TipoTelefonoController@getTiposTelefono');
            $api->post('tipos-telefono/save','TipoTelefonoController@save');
            $api->get('tipos-telefono/{id}','TipoTelefonoController@getTipoTelefonoById');
            $api->post('tipos-telefono/update','TipoTelefonoController@update');
            $api->post('tipos-telefono/delete','TipoTelefonoController@updateEstado');

            $api->get('tipos-relacion','TipoRelacionController@getTiposRelacion');
            $api->post('tipos-relacion/save','TipoRelacionController@save');
            $api->get('tipos-relacion/{id}','TipoRelacionController@getTipoRelacionById');
            $api->post('tipos-relacion/update','TipoRelacionController@update');
            $api->post('tipos-relacion/delete','TipoRelacionController@updateEstado');

            $api->get('tipos-web','TipoWebController@getTiposWeb');
            $api->post('tipos-web/save','TipoWebController@save');
            $api->get('tipos-web/{id}','TipoWebController@getTipoWebById');
            $api->post('tipos-web/update','TipoWebController@update');
            $api->post('tipos-web/delete','TipoWebController@updateEstado');

            $api->get('spots','SpotController@getSpots');
            $api->post('spots/save','SpotController@save');
            $api->get('spots/{id}','SpotController@getSpotById');
            $api->post('spots/update','SpotController@update');
            $api->post('spots/delete','SpotController@updateEstado');

            $api->get('tipos-per-doc-identidad','TipoPerDocIdentidadController@getTiposPerDocIdentidad');
            $api->post('tipos-per-doc-identidad/save','TipoPerDocIdentidadController@save');
            $api->get('tipos-per-doc-identidad/{id}','TipoPerDocIdentidadController@getTipoPerDocIdentidadById');
            $api->post('tipos-per-doc-identidad/update','TipoPerDocIdentidadController@update');
            $api->post('tipos-per-doc-identidad/delete','TipoPerDocIdentidadController@updateEstado');

        });

        $api->group(['namespace' => 'Productos' ], function ($api) {

            $api->get('unidades-medida','UnidadMedidaController@getUnidadesMedida');
            $api->post('unidades-medida/save','UnidadMedidaController@save');
            $api->get('unidades-medida/{id}','UnidadMedidaController@getUnidadMedidaById');
            $api->post('unidades-medida/update','UnidadMedidaController@update');
            $api->post('unidades-medida/delete','UnidadMedidaController@updateEstado');

            $api->get('clases','ClaseController@getClases');
            $api->post('clases/upload-imagen','ClaseController@uploadImagen');
            $api->post('clases/save','ClaseController@save');
            $api->get('clases/{id}','ClaseController@getClaseById');
            $api->post('clases/update','ClaseController@update');
            $api->post('clases/delete','ClaseController@updateEstado');

            $api->get('marcas','MarcaController@getMarcas');
            $api->post('marcas/upload-imagen','MarcaController@uploadImagen');
            $api->post('marcas/save','MarcaController@save');
            $api->get('marcas/{id}','MarcaController@getMarcaById');
            $api->post('marcas/update','MarcaController@update');
            $api->post('marcas/delete','MarcaController@updateEstado');

            $api->get('modelos','ModeloController@getModelos');
            $api->post('modelos','ModeloController@getModelosByMarcaId');
            $api->post('modelos/save','ModeloController@save');
            $api->get('modelos/{id}','ModeloController@getModeloById');
            $api->post('modelos/update','ModeloController@update');
            $api->post('modelos/delete','ModeloController@updateEstado');

            $api->get('familias','FamiliaController@getFamilias');
            $api->post('familias','FamiliaController@getFamiliasByClaseId');
            $api->post('familias/upload-imagen','FamiliaController@uploadImagen');
            $api->post('familias/save','FamiliaController@save');
            $api->get('familias/{id}','FamiliaController@getFamiliaById');
            $api->post('familias/update','FamiliaController@update');
            $api->post('familias/delete','FamiliaController@updateEstado');

            $api->get('sub-familias','SubFamiliaController@getSubFamilias');
            $api->post('sub-familias','SubFamiliaController@getSubFamiliasByClaseId');
            $api->post('sub-familias/upload-imagen','SubFamiliaController@uploadImagen');
            $api->post('sub-familias/save','SubFamiliaController@save');
            $api->get('sub-familias/{id}','SubFamiliaController@getSubFamiliaById');
            $api->post('sub-familias/update','SubFamiliaController@update');
            $api->post('sub-familias/delete','SubFamiliaController@updateEstado');

            $api->get('productos','ProductoController@getProductos');
            $api->post('productos/upload-imagen','ProductoController@uploadImagen');
            $api->post('productos/save','ProductoController@save');
            $api->get('productos/{id}','ProductoController@getProductoById');
            $api->post('productos/update','ProductoController@update');
            $api->post('productos/delete','ProductoController@updateEstado');

        });

        $api->group(['namespace' => 'Kardex' ], function ($api) {

            $api->get('tipos-operacion','TiposOperacionController@getTiposOperacion');
            $api->post('tipos-operacion','TiposOperacionController@postTipoOperacion');
            $api->get('tipos-operacion/{page}','TiposOperacionController@getTipoOperacionById');
            $api->post('tipos-operacion/update','TiposOperacionController@updateTipoOperacion');
            $api->post('tipos-operacion/delete','TiposOperacionController@deleteTipoOperacion');

        });

        $api->group(['namespace' => 'Vehiculos' ], function ($api) {

           $api->get('clases-vehiculo','ClasesVehiculoController@getClasesVehiculo');
           $api->post('clases-vehiculo','ClasesVehiculoController@postClaseVehiculo');
           $api->get('clases-vehiculo/{page}','ClasesVehiculoController@getClaseVehiculoById');
           $api->post('clases-vehiculo/update','ClasesVehiculoController@updateClaseVehiculo');
           $api->post('clases-vehiculo/delete','ClasesVehiculoController@deleteClaseVehiculo');

           $api->get('marcas-vehiculo','MarcasVehiculoController@getMarcasVehiculo');
           $api->post('marcas-vehiculo','MarcasVehiculoController@postMarcaVehiculo');
           $api->get('marcas-vehiculo/{page}','MarcasVehiculoController@getMarcaVehiculoById');
           $api->post('marcas-vehiculo/update','MarcasVehiculoController@updateMarcaVehiculo');
           $api->post('marcas-vehiculo/delete','MarcasVehiculoController@deleteMarcaVehiculo');
           $api->get('tipos-vehiculo','TiposVehiculoController@getTiposVehiculo');

           $api->post('tipos-vehiculo','TiposVehiculoController@postTipoVehiculo');
           $api->get('tipos-vehiculo/{page}','TiposVehiculoController@getTipoVehiculoById');
           $api->get('tipos-vehiculo-by-id/{page}','TiposVehiculoController@getTiposVehiculoById');
           $api->post('tipos-vehiculo/update','TiposVehiculoController@updateTipoVehiculo');
           $api->post('tipos-vehiculo/delete','TiposVehiculoController@deleteTipoVehiculo');

           $api->get('vehiculos','VehiculosController@getVehiculos');
           $api->post('vehiculos','VehiculosController@postVehiculo');
           $api->get('vehiculos/{page}','VehiculosController@getVehiculoById');
           $api->post('vehiculos/update','VehiculosController@updateVehiculo');
           $api->post('vehiculos/delete','VehiculosController@deleteVehiculo');

        });

        $api->group(['namespace' => 'Documentos' ], function ($api) {

             $api->get('doc-tipos','DocTiposController@getDocTipos');
             $api->post('doc-tipos','DocTiposController@postDocTipo');
             $api->get('doc-tipos/{id}','DocTiposController@getDocTipoById');
             $api->post('doc-tipos/update','DocTiposController@updateDocTipo');
             $api->post('doc-tipos/delete','DocTiposController@deleteDocTipo');

             $api->get('tipos-comprobante','TiposComprobanteController@getTiposComprobante');
             $api->post('tipos-comprobante','TiposComprobanteController@postTipoComprobante');
             $api->get('tipos-comprobante/{id}','TiposComprobanteController@getTipoComprobanteById');
             $api->post('tipos-comprobante/update','TiposComprobanteController@updateTipoComprobante');
             $api->post('tipos-comprobante/delete','TiposComprobanteController@deleteTipoComprobante');

             $api->get('correlativos','CorrelativosController@getCorrelativos');
             $api->post('correlativos','CorrelativosController@postCorrelativo');
             $api->get('correlativos/{id}','CorrelativosController@getCorrelativoById');
             $api->post('correlativos/update','CorrelativosController@updateCorrelativo');
             $api->post('correlativos/delete','CorrelativosController@deleteCorrelativo');

        });

         $api->group(['namespace' => 'Contabilidad' ], function ($api) {
             
             $api->get('monedas','MonedasController@getMonedas');
             $api->post('monedas','MonedasController@postMoneda');
             $api->get('monedas/{id}','MonedasController@getMonedaById');
             $api->post('monedas/update','MonedasController@updateMoneda');
             $api->post('monedas/delete','MonedasController@deleteMoneda');

             $api->get('tipos-cambio','TiposCambioController@getTiposCambio');
             $api->post('tipos-cambio','TiposCambioController@postTipoCambio');
             $api->get('tipos-cambio/{id}','TiposCambioController@getTipoCambioById');
             $api->post('tipos-cambio/update','TiposCambioController@updateTipoCambio');
             $api->post('tipos-cambio/delete','TiposCambioController@deleteTipoCambio');

             $api->get('tipos-pago','TiposPagoController@getTiposPago');
             $api->post('tipos-pago','TiposPagoController@postTipoPago');
             $api->get('tipos-pago/{id}','TiposPagoController@getTipoPagoById');
             $api->post('tipos-pago/update','TiposPagoController@updateTipoPago');
             $api->post('tipos-pago/delete','TiposPagoController@deleteTipoPago');

             $api->get('formas-pago','FormasPagoController@getFormasPago');
             $api->post('formas-pago','FormasPagoController@postFormaPago');
             $api->get('formas-pago/{id}','FormasPagoController@getFormaPagoById');
             $api->post('formas-pago/update','FormasPagoController@updateFormaPago');
             $api->post('formas-pago/delete','FormasPagoController@deleteFormaPago');
             
             $api->get('igv','IgvController@getIgvs');
             $api->post('igv','IgvController@postIgv');
             $api->get('igv/{id}','IgvController@getIgvById');
             $api->post('igv/update','IgvController@updateIgv');
             $api->post('igv/delete','IgvController@deleteIgv');
             
        });

    });
});