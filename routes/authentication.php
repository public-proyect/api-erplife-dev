<?php


#Ruta para la autenticacion de usuario

$api=app('Dingo\Api\Routing\Router');

$api->version('v1', ['middleware'=>'cors', 'namespace' => 'App\Http\Controllers'], function ($api) {

	$api->post('authenticate','Accesos\AuthenticationController@authenticate');
	$api->get('EmpresasByUserEmpresaId','Accesos\AuthenticationController@getEmpresasByUserEmpresaId');
	$api->post('get-info-user','Accesos\AuthenticationController@getUserInfo');
	$api->get('logout','Accesos\AuthenticationController@logout');


    $api->group(['prefix' => 'panel', 'namespace' => 'Panel' ], function ($api) {
        # accesos for load menus
        $api->post('controles', 'AccesosUserPanelController@getControlByTipoControlId');
        $api->get('controles', 'AccesosUserPanelController@getControlByTipoControlId');
	});

});
